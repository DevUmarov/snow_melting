package com.example.knut.snowmelting.model.test;

import com.example.knut.snowmelting.model.custom_deserializre.Forecast6daysDeserializer;
import com.google.gson.annotations.JsonAdapter;

import java.util.Date;
import java.util.List;

/**
 * Created by Nizomjon on 04/03/2017.
 */
@JsonAdapter(Forecast6daysDeserializer.class)
public class Forecast6days {
    private Grid grid;
    private Date timeRelease;
    private List<Sky> skies;
    private List<Temperature> temperatures;

    public Forecast6days(Grid grid, Date timeRelease, List<Sky> skies, List<Temperature> temperatures) {
        this.grid = grid;
        this.timeRelease = timeRelease;
        this.skies = skies;
        this.temperatures = temperatures;
    }

    public Grid getGrid() {
        return grid;
    }

    public Date getTimeRelease() {
        return timeRelease;
    }

    public List<Sky> getSkies() {
        return skies;
    }

    public List<Temperature> getTemperatures() {
        return temperatures;
    }
}


