package com.example.knut.snowmelting.base.components;

import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.service.ControllerService;
import com.example.knut.snowmelting.views.admin.AdminPresenter;
import com.example.knut.snowmelting.views.admin.feedback.FeedbackPresenter;
import com.example.knut.snowmelting.views.admin.mainMenu.MainAdminPagePresenter;
import com.example.knut.snowmelting.views.dashboard.generalInfo.GeneralInfoPresenter;
import com.example.knut.snowmelting.views.dashboard.onOffHistory.OnOffHistoryPresenter;
import com.example.knut.snowmelting.views.main.MainPresenter;
import com.example.knut.snowmelting.views.sensorNode.SensorNodePresenter;
import com.example.knut.snowmelting.views.sensorNode.dataHistory.DataHistoryPresenter;
import com.example.knut.snowmelting.views.sensorNode.mainMenu.MainMenuPresenter;
import com.example.knut.snowmelting.views.xxxanalysis.AnalysisPresenter;
import com.example.knut.snowmelting.views.dashboard.DashboardPresenter;
import com.example.knut.snowmelting.views.dashboard.sensorNodes.SensorNodesPresenter;
import com.example.knut.snowmelting.views.locationList.LocationListPresenter;
import com.example.knut.snowmelting.views.login.LoginPresenter;
import com.example.knut.snowmelting.views.xxxmain.XXXMainPresenter;
import com.example.knut.snowmelting.views.register.RegisterPresenter;
import com.example.knut.snowmelting.views.usersList.UsersListPresenter;
import com.example.knut.snowmelting.views.weatherData.WeatherDataPresenter;
import com.example.knut.snowmelting.views.weatherData.forecast.WeatherForecastPresenter;
import com.example.knut.snowmelting.views.weatherData.history.WeatherHistoryPresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Singleton
@Component(modules = {NetModule.class, UtilsModule.class, DatabaseModule.class})
public interface PresenterComponent {
    void inject(LoginPresenter presenter);

    void inject(RegisterPresenter presenter);

    void inject(XXXMainPresenter presenter);

    void inject(WeatherDataPresenter presenter);

    void inject(DashboardPresenter presenter);

    void inject(AnalysisPresenter presenter);

    void inject(WeatherHistoryPresenter presenter);

    void inject(WeatherForecastPresenter presenter);

    void inject(SensorNodesPresenter presenter);

    void inject(AdminPresenter presenter);

    void inject(UsersListPresenter usersListPresenter);

    void inject(MainAdminPagePresenter mainAdminPagePresenter);

    void inject(FeedbackPresenter feedbackPresenter);

    void inject(LocationListPresenter locationListPresenter);

    void inject(SensorNodePresenter sensorNodePresenter);

    void inject(DataHistoryPresenter dataHistoryPresenter);

    void inject(OnOffHistoryPresenter onOffHistoryPresenter);

    void inject(MainMenuPresenter mainMenuPresenter);

    void inject(GeneralInfoPresenter generalInfoPresenter);

    void inject(MainPresenter mainPresenter);

    void inject(ControllerService controllerService);
}
