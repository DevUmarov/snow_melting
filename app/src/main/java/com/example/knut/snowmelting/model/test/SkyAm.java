package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 06/03/2017.
 */

public class SkyAm {
    private String code;
    private String name;

    public SkyAm(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
