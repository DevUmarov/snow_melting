package com.example.knut.snowmelting.views.weatherData.forecast;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.model.test.Response;
import com.example.knut.snowmelting.views.weatherData.history.WeatherHistoryFragment;

/**
 * Created by umarov on 5/1/17.
 */

public class WeatherForecastFragment extends BaseFragment implements WeatherForecastView {

    public static WeatherForecastFragment newInstance() {
        WeatherForecastFragment myFragment = new WeatherForecastFragment();
        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onError(String error) {

    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main_admin_dashboard;//TODO: delete
    }

    @Override
    public void fillGraph(Response response) {

    }

    @Override
    public void fillForecast10days(Response response) {

    }
}
