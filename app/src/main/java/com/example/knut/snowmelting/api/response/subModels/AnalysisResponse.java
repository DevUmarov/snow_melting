package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 8/2/17.
 */

public class AnalysisResponse implements Parcelable {

    @SerializedName("count_sf")
    @Expose
    private float countSf;

    @SerializedName("dp")
    @Expose
    private float dp;

    @SerializedName("humidity")
    @Expose
    private float humidity;

    @SerializedName("max_sf")
    @Expose
    private float maxSf;

    @SerializedName("rain")
    @Expose
    private float rain;

    @SerializedName("sf")
    @Expose
    private float sf;

    @SerializedName("temp")
    @Expose
    private float temp;

    @SerializedName("ws")
    @Expose
    private float ws;


    public float getCountSf() {
        return countSf;
    }

    public void setCountSf(float countSf) {
        this.countSf = countSf;
    }

    public float getDp() {
        return dp;
    }

    public void setDp(float dp) {
        this.dp = dp;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getMaxSf() {
        return maxSf;
    }

    public void setMaxSf(float maxSf) {
        this.maxSf = maxSf;
    }

    public float getRain() {
        return rain;
    }

    public void setRain(float rain) {
        this.rain = rain;
    }

    public float getSf() {
        return sf;
    }

    public void setSf(float sf) {
        this.sf = sf;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getWs() {
        return ws;
    }

    public void setWs(float ws) {
        this.ws = ws;
    }

    protected AnalysisResponse(Parcel in) {
        countSf = in.readFloat();
        dp = in.readFloat();
        humidity = in.readFloat();
        maxSf = in.readFloat();
        rain = in.readFloat();
        sf = in.readFloat();
        temp = in.readFloat();
        ws = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(countSf);
        dest.writeFloat(dp);
        dest.writeFloat(humidity);
        dest.writeFloat(maxSf);
        dest.writeFloat(rain);
        dest.writeFloat(sf);
        dest.writeFloat(temp);
        dest.writeFloat(ws);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AnalysisResponse> CREATOR = new Parcelable.Creator<AnalysisResponse>() {
        @Override
        public AnalysisResponse createFromParcel(Parcel in) {
            return new AnalysisResponse(in);
        }

        @Override
        public AnalysisResponse[] newArray(int size) {
            return new AnalysisResponse[size];
        }
    };
}