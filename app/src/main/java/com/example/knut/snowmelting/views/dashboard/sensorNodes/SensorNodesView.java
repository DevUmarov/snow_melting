package com.example.knut.snowmelting.views.dashboard.sensorNodes;

import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by umarov on 5/10/17.
 */

public interface SensorNodesView extends MvpView {
//    void showSensorNodes(SingleUserResponse data);
    void onSensorNodeClick(int position);
}
