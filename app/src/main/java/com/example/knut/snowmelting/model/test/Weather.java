package com.example.knut.snowmelting.model.test;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Weather {

    private List<Forecast> forecast3hours = null;

    private List<Forecast3hours> forecast3days = null;

    @SerializedName("forecast6days")
    @Expose
    private List<Forecast6days> forecast6daysList;

    public List<Forecast> getForecast3hours() {
        return forecast3hours;
    }

    public List<Forecast3hours> getForecast3days() {
        return forecast3days;
    }

    public List<Forecast6days> getForecast6days() {
        return forecast6daysList;
    }

    @Override
    public String toString() {
        return new StringBuilder("Weather{")
                .append("forecast3hours=").append(forecast3hours)
                .append('}').toString();
    }
}
