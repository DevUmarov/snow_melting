package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Grid {

    final String city = null;
    final String county = null;
    final String village = null;
    final double latitude = Double.valueOf(0); // disable inlining the primitive double 0
    final double longitude = Double.valueOf(0); // disable inlining the primitive double 0

    @Override
    public String toString() {
        return new StringBuilder("Grid{")
                .append("city='").append(city).append('\'')
                .append(", county='").append(county).append('\'')
                .append(", village='").append(village).append('\'')
                .append(", latitude=").append(latitude)
                .append(", longitude=").append(longitude)
                .append('}').toString();
    }
}
