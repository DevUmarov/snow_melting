package com.example.knut.snowmelting.model.test;

import com.example.knut.snowmelting.model.custom_deserializre.YnToBooleanJsonDeserializer;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Common {


    @SerializedName("alertYn")
    @JsonAdapter(YnToBooleanJsonDeserializer.class)
    final boolean alert = Boolean.valueOf(false); // disable inlining the primitive boolean false

    @SerializedName("stormYn")
    @JsonAdapter(YnToBooleanJsonDeserializer.class)
    final boolean storm = Boolean.valueOf(false); // disable inlining the primitive boolean false

}
