package com.example.knut.snowmelting.base.module;

import android.content.Context;

import com.example.knut.snowmelting.service.ControllerService;
import com.example.knut.snowmelting.views.admin.AdminPresenter;
import com.example.knut.snowmelting.views.admin.feedback.FeedbackPresenter;
import com.example.knut.snowmelting.views.admin.mainMenu.MainAdminPagePresenter;
import com.example.knut.snowmelting.views.dashboard.generalInfo.GeneralInfoPresenter;
import com.example.knut.snowmelting.views.dashboard.onOffHistory.OnOffHistoryPresenter;
import com.example.knut.snowmelting.views.dashboard.sensorNodes.SensorNodesPresenter;
import com.example.knut.snowmelting.views.main.MainPresenter;
import com.example.knut.snowmelting.views.sensorNode.SensorNodePresenter;
import com.example.knut.snowmelting.views.sensorNode.dataHistory.DataHistoryPresenter;
import com.example.knut.snowmelting.views.sensorNode.mainMenu.MainMenuPresenter;
import com.example.knut.snowmelting.views.xxxanalysis.AnalysisPresenter;
import com.example.knut.snowmelting.views.dashboard.DashboardPresenter;
import com.example.knut.snowmelting.views.locationList.LocationListPresenter;
import com.example.knut.snowmelting.views.login.LoginPresenter;
import com.example.knut.snowmelting.views.xxxmain.XXXMainPresenter;
import com.example.knut.snowmelting.views.register.RegisterPresenter;
import com.example.knut.snowmelting.views.usersList.UsersListPresenter;
import com.example.knut.snowmelting.views.weatherData.WeatherDataPresenter;
import com.example.knut.snowmelting.views.weatherData.forecast.WeatherForecastPresenter;
import com.example.knut.snowmelting.views.weatherData.history.WeatherHistoryPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Module
public class PresenterModule {
    private Context context;

    public PresenterModule(Context context) {
        this.context = context;
    }

    @Provides
    LoginPresenter loginPresenter() {
        return new LoginPresenter(context);
    }

    @Provides
    RegisterPresenter registerPresenter() {
        return new RegisterPresenter(context);
    }

    @Provides
    XXXMainPresenter xxxMainPresenter() {
        return new XXXMainPresenter(context);
    }

    @Provides
    AnalysisPresenter analysisPresenter() {
        return new AnalysisPresenter(context);
    }

    @Provides
    DashboardPresenter dashboardPresenter() {
        return new DashboardPresenter(context);
    }

    @Provides
    SensorNodePresenter detailsPresenter() {
        return new SensorNodePresenter(context);
    }

    @Provides
    AdminPresenter adminPresenter() {
        return new AdminPresenter(context);
    }

    @Provides
    WeatherDataPresenter weatherDataPresenter() {
        return new WeatherDataPresenter(context);
    }

    @Provides
    WeatherHistoryPresenter weatherHistoryPresenter() {
        return new WeatherHistoryPresenter(context);
    }

    @Provides
    WeatherForecastPresenter weatherForecastPresenter() {
        return new WeatherForecastPresenter(context);
    }

    @Provides
    SensorNodesPresenter advancedInfoPresenter() {
        return new SensorNodesPresenter(context);
    }

    @Provides
    UsersListPresenter usersListPresenter() {
        return new UsersListPresenter(context);
    }

    @Provides
    MainAdminPagePresenter mainAdminPagePresenter() {
        return new MainAdminPagePresenter(context);
    }

    @Provides
    FeedbackPresenter feedbackPresenter() {
        return new FeedbackPresenter(context);
    }

    @Provides
    LocationListPresenter locationListPresenter() {
        return new LocationListPresenter(context);
    }

    @Provides
    DataHistoryPresenter dataHistoryPresenter(){ return new DataHistoryPresenter(context);}

    @Provides
    MainMenuPresenter mainMenuPresenter(){return  new MainMenuPresenter(context);}

    @Provides
    OnOffHistoryPresenter onOffHistoryPresenter(){return new OnOffHistoryPresenter(context);}

    @Provides
    GeneralInfoPresenter generalInfoPresenter(){return new GeneralInfoPresenter(context);}

    @Provides
    MainPresenter mainPresenter(){return new MainPresenter(context);}

}
