package com.example.knut.snowmelting.model;

import com.example.knut.snowmelting.model.test.Precipitation;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class WeatherConditionSk {
    @SerializedName("grid")
    private
    StationSk stationSk;

    @SerializedName("station")
    private StationSk station;

    @SerializedName("wind")
    private
    WindSk windSk;

    @SerializedName("sky")
    private
    SkySk skySk;

    @SerializedName("temperature")
    private
    TemperatureSk temperatureSk;

    @SerializedName("fcst3hour")
    private Fcst3hour fcst3hour;

    @SerializedName("precipitation")
    private Precipitation precipitation;



    private String humidity;
    private String timeObservation;

//    String location;


    public Precipitation getPrecipitation() {
        return precipitation;
    }

    public StationSk getStation() {
        return station;
    }

    public Fcst3hour getFcst3hour() {
        return fcst3hour;
    }

    public void setFcst3hour(Fcst3hour fcst3hour) {
        this.fcst3hour = fcst3hour;
    }

    private String timeRelease;

    public String getTimeRelease() {
        return timeRelease;
    }

    public StationSk getStationSk() {
        return stationSk;
    }

    public WindSk getWindSk() {
        return windSk;
    }

    public SkySk getSkySk() {
        return skySk;
    }

    public TemperatureSk getTemperatureSk() {
        return temperatureSk;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getTimeObservation() {
        return timeObservation;
    }

}
