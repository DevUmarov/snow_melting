package com.example.knut.snowmelting.views.dashboard.generalInfo;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.body.TurnOnResponseBody;
import com.example.knut.snowmelting.api.response.subModels.LastSeenValues;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.WeatherDay;
import com.example.knut.snowmelting.utils.AppTimeUtils;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.utils.WeatherUtils;
import com.example.knut.snowmelting.widgets.ChannelsView;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.iconics.context.IconicsContextWrapper;
import com.mikepenz.weather_icons_typeface_library.WeatherIcons;

import org.w3c.dom.Text;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by umarov on 5/10/17.
 */

public class GeneralInfoFragment extends BaseFragment implements GeneralInfoView {

    //    private boolean hasFinishedBefore;
    private static final String LAST_SEEN_EXTRA = "com.example.knut.snowmelting.views.dashboard.generalInfo.LAST_SEEN_EXTRA";
    private static final String IS_ADMIN_EXTRA = "com.example.knut.snowmelting.views.dashboard.generalInfo.IS_ADMIN_EXTRA";
    private static final String SENSOR_NODE_ID_EXTRA = "com.example.knut.snowmelting.views.dashboard.generalInfo.SENSOR_NODE_ID_EXTRA";

    @Inject
    GeneralInfoPresenter generalInfoPresenter;

    @Inject
    Settings settings;

    @BindView(R.id.temperature_value)
    TextView temperatureValue;

    @BindView(R.id.humidity_value)
    TextView humidityValue;

    @BindView(R.id.wind_speed_value)
    TextView windSpeedValue;

    @BindView(R.id.web_view)
    WebView webview;

    @BindView(R.id.toggle)
    RadioGroup radioGroup;

    @BindView(R.id.status_container)
    LinearLayout statusContainer;

    @BindView(R.id.status_text)
    TextView statusTextView;

    @BindView(R.id.off)
    RadioButton offRadioButton;

    @BindView(R.id.on)
    RadioButton onRadioButton;

    @BindView(R.id.text_timer)
    Chronometer chronometer;

    @BindView(R.id.channels_view)
    ChannelsView mChannelsView;

    @BindView(R.id.mac_address)
    TextView macAddress;

    @BindView(R.id.address_text)
    TextView addressText;

    private LastSeenValues mLastSeenValues;
    private int mSensorNodeId;
    private int statusTextColor;
    private String mMacAddressString;
    private String statusText;
    private String mCameraUrl;
    private boolean isOn;
    private String mAddress;
    private boolean mIsAdmin;
    private ActivityInteraction mActivityInteraction;
    private boolean toggleButtonState = true;
    private ArrayList<Integer> mMicLists;

//    public static GeneralInfoFragment newInstance(boolean isAdmin, int sensorNodeId, LastSeenValues lastSeenValues) {
//        GeneralInfoFragment myFragment = new GeneralInfoFragment();
//        Bundle bundle = new Bundle();
//        bundle.putParcelable(LAST_SEEN_EXTRA, lastSeenValues);
//        bundle.putBoolean(IS_ADMIN_EXTRA, isAdmin);
//        bundle.putInt(SENSOR_NODE_ID_EXTRA, sensorNodeId);
//
//        myFragment.setArguments(bundle);
//        return myFragment;
//    }

    public static GeneralInfoFragment newInstance(boolean isAdmin) {
        GeneralInfoFragment myFragment = new GeneralInfoFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_ADMIN_EXTRA, isAdmin);

        myFragment.setArguments(bundle);
        return myFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof Activity) {
            Activity activity = (Activity) context;

            if (activity instanceof ActivityInteraction) {
                mActivityInteraction = (ActivityInteraction) activity;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (activity instanceof ActivityInteraction) {
            mActivityInteraction = (ActivityInteraction) activity;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        if (getArguments() != null) {
//            if (getArguments().containsKey(LAST_SEEN_EXTRA)) {
//                mLastSeenValues = getArguments().getParcelable(LAST_SEEN_EXTRA);
//            }
//
//            if (getArguments().containsKey(SENSOR_NODE_ID_EXTRA)) {
//                mSensorNodeId = getArguments().getInt(SENSOR_NODE_ID_EXTRA);
//            }
//
            mIsAdmin = getArguments().getBoolean(IS_ADMIN_EXTRA, false);
        }

        generalInfoPresenter.attachView(this);

        if (mIsAdmin) {
            radioGroup.setVisibility(View.GONE);
            statusContainer.setVisibility(View.GONE);
            mChannelsView.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(statusText)) {
            setStatusText(statusText);
            setStatusTextColor(statusTextColor);
        }

        if (!TextUtils.isEmpty(mMacAddressString)) {
            setMacAddress(mMacAddressString);
        }

        toggleRadioGroup(toggleButtonState);

        if (mLastSeenValues != null) {
            configureUI(mIsAdmin, mCameraUrl, mAddress, mSensorNodeId, mLastSeenValues);
        }

        if (mLastSeenValues != null) {
            configureUI(mIsAdmin, mCameraUrl, mAddress, mSensorNodeId, mLastSeenValues);
        }

        super.onViewCreated(view, savedInstanceState);
    }

    public void setData(boolean isAdmin, String cameraUrl, int sensorNodeId, String address, LastSeenValues lastSeenValues) {
        mLastSeenValues = new LastSeenValues(lastSeenValues);
        mSensorNodeId = sensorNodeId;
        mIsAdmin = isAdmin;
        mAddress = address;
        mCameraUrl = cameraUrl;

        if (isAdded() && isVisible()) {
            configureUI(mIsAdmin, mCameraUrl, mAddress, mSensorNodeId, mLastSeenValues);
        }

        generalInfoPresenter.checkMicStatus(mSensorNodeId);
    }

    public void configureUI(boolean isAdmin, String cameraUrl, String address, int sensorNodeId, LastSeenValues lastSeenValues) {

        if (isAdmin) {
            radioGroup.setVisibility(View.GONE);
        }

        addressText.setText(address);
        chronometer.setFormat("Running - %s");

        if (generalInfoPresenter.isTurnedOn(sensorNodeId)) {
            radioGroup.check(R.id.on);
            chronometer.setVisibility(View.VISIBLE);
            chronometer.setBase(SystemClock.elapsedRealtime() - (System.currentTimeMillis() - generalInfoPresenter.getTurnedTime(sensorNodeId)));
            chronometer.start();
            isOn = true;
        } else {
            radioGroup.check(R.id.off);
            chronometer.setVisibility(View.GONE);
            chronometer.setBase(SystemClock.elapsedRealtime());
//            initTurnedOffMics();
            isOn = false;
        }

        mMicLists = generalInfoPresenter.getMicList(sensorNodeId);
        initMics(isOn, mMicLists);

        offRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOn) {
                    toggleRadioGroup(false);
                    generalInfoPresenter.turnOff(sensorNodeId);
                }
            }
        });

        onRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isOn) {
                    toggleRadioGroup(false);
                    generalInfoPresenter.turnOn(sensorNodeId);
                }
            }
        });

//        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
//                if(shouldRun) {
//                    RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
//                    boolean isChecked = checkedRadioButton.isChecked();
//
//                    switch (checkedId) {
//                        case R.id.on: {
//                            if (isChecked) {
//                                toggleRadioGroup(false);
//                                generalInfoPresenter.turnOn(sensorNodeId);
//                            }
//                            break;
//                        }
//                        case R.id.off: {
//                            if (isChecked) {
//
//                            }
//                            break;
//                        }
//                    }
//                }
//                shouldRun = true;
//            }
//        });
//
//        shouldRun = true;

        windSpeedValue.setText(getString(R.string.wind_speed_value, lastSeenValues.getWindSpeed()));
        humidityValue.setText(getString(R.string.humidity_value, lastSeenValues.getHumidity()));
        temperatureValue.setText(getString(R.string.temperature_value, lastSeenValues.getTemperature()));

        WebSettings settings = webview.getSettings();
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);

//      webview.loadUrl("http://210.119.146.199:8081/");

        if (!TextUtils.isEmpty(cameraUrl)) {
            webview.loadUrl(cameraUrl);
        }
    }

    public void setStatusText(String text) {
        statusText = text;

        if (isAdded()) {
            statusTextView.setText(text);
        }
    }

    public void setStatusTextColor(int statusTextColor) {
        this.statusTextColor = statusTextColor;

        if (isAdded()) {
            statusTextView.setTextColor(statusTextColor);
        }
    }

    public void setMacAddress(String macAddressString) {
        mMacAddressString = macAddressString;

        if (isAdded()) {
            macAddress.setVisibility(View.VISIBLE);
            macAddress.setText(getString(R.string.mac_address, macAddressString));
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_general_info;
    }

    @Override
    public void onDestroy() {
        generalInfoPresenter.detachView();
        super.onDestroy();
    }

    public void toggleRadioGroup(boolean status) {
        toggleButtonState = status;

        if (isAdded()) {
            for (int i = 0; i < radioGroup.getChildCount(); i++) {
                radioGroup.getChildAt(i).setEnabled(status);
            }

            if (status) {
                radioGroup.setAlpha(1.0f);
            } else {
                radioGroup.setAlpha(.5f);
            }
        }
    }

    private void initMics(boolean isOn, ArrayList<Integer> integers) {
        try {
            mChannelsView.setMainChannelColors(isOn ? Color.GREEN : Color.RED,
                    integers.get(0) == 1 ? Color.GREEN : Color.RED,
                    integers.get(1) == 1 ? Color.GREEN : Color.RED,
                    integers.get(2) == 1 ? Color.GREEN : Color.RED);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

//    private void initRedMics() {
//        mChannelsView.setMainChannelColors(Color.RED, Color.RED, Color.RED, Color.RED);
//    }

    /*** MVP Implementation ***/

    @Override
    public void turnOnFail() {
        toggleRadioGroup(true);
        radioGroup.check(R.id.off);
        isOn = false;
        Toast.makeText(getActivity(), R.string.turn_on_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void turnOnSuccess(TurnOnResponseBody turnOnResponseBody) {
        toggleRadioGroup(true);
        isOn = true;
//        initTurnedOnMics(turnOnResponseBody.getResult().getMcStatusListOn());
        chronometer.setVisibility(View.VISIBLE);
        chronometer.start();
        Toast.makeText(getActivity(), R.string.turn_on_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void turnOffFail() {
        toggleRadioGroup(true);
        radioGroup.check(R.id.on);
        isOn = true;
        Toast.makeText(getActivity(), R.string.turn_off_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void turnOffSuccess() {
        toggleRadioGroup(true);
        isOn = false;
//        initTurnedOffMics();
        chronometer.setVisibility(View.GONE);
        chronometer.stop();
        Toast.makeText(getActivity(), R.string.turn_off_success, Toast.LENGTH_SHORT).show();

        if (mActivityInteraction != null) {
            mActivityInteraction.onTurnedOff();
        }
    }

    @Override
    public void notTurnedOn() {
        toggleRadioGroup(true);
        Toast.makeText(getActivity(), R.string.not_turned_on, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHourlyWeatherDataArrived(WeatherDay weatherDay) {
//        this.weatherDay = weatherDay;
//        temperatureValue.setText(getString(R.string.temperature_value, weatherDay.getWeather().getHourly().get(0).getTemperature().getTc()));
//        humidityValue.setText(getString(R.string.humidity_value, weatherDay.getWeather().getHourly().get(0).getHumidity()));
//        windSpeedValue.setText(getString(R.string.wind_speed_value, weatherDay.getWeather().getHourly().get(0).getWind().getWspd()));
//        windDirection.setText(getString(R.string.wind_direction_value, weatherDay.getWeather().getHourly().get(0).getWind().getWdir(),
//                WeatherUtils.getDirection(Double.valueOf(weatherDay.getWeather().getHourly().get(0).getWind().getWdir()))));
//        skyName.setText(weatherDay.getWeather().getHourly().get(0).getSky().getName());
//        lastUpdatedTime.setText(getString(R.string.last_update, AppTimeUtils.getDate(settings.getWeatherLastUpdatedTime())));
    }

    @Override
    public void onMicStatusArrived(ArrayList<Integer> integers) {
        mMicLists = integers;

        if (isAdded() && isVisible()) {
            initMics(isOn, mMicLists);
        }
    }

    public interface ActivityInteraction {
        void onTurnedOff();
    }


}
