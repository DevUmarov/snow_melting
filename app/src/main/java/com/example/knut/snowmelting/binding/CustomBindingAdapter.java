package com.example.knut.snowmelting.binding;

import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by umarov on 4/28/17.
 */

public class CustomBindingAdapter extends BindingAdapters {

    @Override
    public void setTypeface(TextView v, String style) {
        switch (style) {
            case "bold":
                v.setTypeface(null, Typeface.BOLD);
                break;
            default:
                v.setTypeface(null, Typeface.NORMAL);
                break;
        }
    }
}
