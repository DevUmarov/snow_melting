package com.example.knut.snowmelting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 16/02/2017.
 */

class ResultSk {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("requestUrl")
    @Expose
    private String requestUrl;

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public String getRequestUrl() {
        return requestUrl;
    }
}
