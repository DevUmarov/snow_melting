package com.example.knut.snowmelting.base;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by umarov on 6/22/17.
 */

public class RxBus {

    private static RxBus instance;
    private final Subject bus = PublishSubject.create();

    public static RxBus getInstance() {
        if (instance == null){
            instance = new RxBus();
        }
        return instance;
    }

    public void post(Object event){
        bus.onNext(event);
    }

    public <T> Observable<T> toObserverable(Class<T> eventType){
        return bus.ofType(eventType);
    }

    public boolean hasObservers() {
        return bus.hasObservers();
    }
}