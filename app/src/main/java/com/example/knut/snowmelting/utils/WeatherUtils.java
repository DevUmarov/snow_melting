package com.example.knut.snowmelting.utils;

/**
 * Created by umarov on 7/4/17.
 */

public final class WeatherUtils {

    private WeatherUtils() {
    }

    public static String getDirection(double deg) {
        String directions[] = {"N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"};
        return directions[(int) Math.round(((deg % 360) / 45))];
    }
}
