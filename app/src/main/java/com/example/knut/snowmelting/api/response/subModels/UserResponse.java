package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 5/17/17.
 */

public class UserResponse implements Parcelable {

    @SerializedName("email")
    @Expose
    String email;

    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("isAdmin")
    @Expose
    boolean isAdmin;

    @SerializedName("username")
    @Expose
    String userName;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserResponse(int id, String email, String userName, boolean isAdmin) {
        this.email = email;
        this.id = id;
        this.isAdmin = isAdmin;
        this.userName = userName;
    }

    protected UserResponse(Parcel in) {
        email = in.readString();
        id = in.readInt();
        isAdmin = in.readByte() != 0x00;
        userName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeInt(id);
        dest.writeByte((byte) (isAdmin ? 0x01 : 0x00));
        dest.writeString(userName);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserResponse> CREATOR = new Parcelable.Creator<UserResponse>() {
        @Override
        public UserResponse createFromParcel(Parcel in) {
            return new UserResponse(in);
        }

        @Override
        public UserResponse[] newArray(int size) {
            return new UserResponse[size];
        }
    };
}