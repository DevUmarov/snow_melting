package com.example.knut.snowmelting.views.weatherData;

import android.content.Context;

import com.example.knut.snowmelting.api.SkService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by umarov on 4/21/17.
 */

public class WeatherDataPresenter extends BasePresenter<WeatherDataView> {

    Context mContext;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;

    public WeatherDataPresenter(Context context) {
        mContext = context;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);
    }


}
