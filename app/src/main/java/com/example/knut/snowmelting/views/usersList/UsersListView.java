package com.example.knut.snowmelting.views.usersList;

import android.view.View;

import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.mvp.MvpView;

import java.util.List;

/**
 * Created by islom on 17. 5. 24.
 */

public interface UsersListView extends MvpView {
    void onUserDataArrived(List<UserResponse> data);
    void onItemClick(View view, UserResponse userResponse);
    void onEditButtonClick(UserResponse userResponse);
    void onDeleteButtonClick(UserResponse userResponse);
    void onViewButtonClick(UserResponse userResponse);

}
