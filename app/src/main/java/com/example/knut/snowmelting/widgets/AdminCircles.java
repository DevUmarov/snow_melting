package com.example.knut.snowmelting.widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.utils.AppUtils;

/**
 * Created by umarov on 5/22/17.
 */

public class AdminCircles extends ViewGroup {


    public AdminCircles(Context context) {
        super(context);
    }

    public AdminCircles(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AdminCircles(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

//    public void addNewView(View child, int radius) {
//        addView(child);
//        curRadius = radius;
//    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int initialLeft = this.getPaddingLeft();
        int initialTop = this.getPaddingTop();
        int curLeft = initialLeft;
        int curTop = initialTop;

        final int childCount = getChildCount();
        final int availableWidth = getMeasuredWidth() - getPaddingRight() - getPaddingLeft();
//        final int availableHeight = getMeasuredHeight() - getPaddingBottom() - getPaddingTop();

        final int childWidth = (availableWidth - AppUtils.dpToPx(10)) / 2;
        final int childHeight = childWidth;

        for (int i = 0; i < childCount; i++) {

            View curChild = getChildAt(i);

            curChild.measure(MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.EXACTLY),
                    MeasureSpec.makeMeasureSpec(childHeight, MeasureSpec.EXACTLY));

            curChild.layout(curLeft, curTop, curLeft + childWidth, curTop + childHeight);

            if (i % 2 == 0) {
                curLeft += childWidth + AppUtils.dpToPx(10);
            } else {
                curLeft = initialLeft;
                curTop += childHeight + AppUtils.dpToPx(15);
            }
        }
    }
}
