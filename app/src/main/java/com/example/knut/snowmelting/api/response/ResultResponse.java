package com.example.knut.snowmelting.api.response;

import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 8/15/17.
 */

public class ResultResponse {

    @SerializedName("result")
    @Expose
    String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
