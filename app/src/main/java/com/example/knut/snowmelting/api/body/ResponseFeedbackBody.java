package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 8/15/17.
 */

public class ResponseFeedbackBody {
    @SerializedName("feedback_id")
    @Expose
    int feedbackId;

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("response")
    @Expose
    String response;

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
