package com.example.knut.snowmelting.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by islom on 17. 6. 8.
 */
@Entity(tableName = "sensor_node", foreignKeys = {
        @ForeignKey(
                entity = LocationEntity.class,
                parentColumns = "id",
                childColumns = "location_id",
                onDelete = CASCADE,
                onUpdate = CASCADE
        )
}, indices = {
        @Index("location_id")
})
public class SensorNodeEntity {

    public SensorNodeEntity(int sensorNodeId, int locationId,
                            double latitude, double longitude) {
        this.sensorNodeId = sensorNodeId;
        this.locationId = locationId;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @PrimaryKey
    @ColumnInfo(name = "sensor_node_id")
    public int sensorNodeId;

    @ColumnInfo(name = "location_id")
    public int locationId;

    public double latitude;

    public double longitude;

    public boolean status;
}
