package com.example.knut.snowmelting.base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.MvpView;
import com.example.knut.snowmelting.utils.AppUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements MvpView {

    @Inject
    public AppUtils appUtils;

    @Nullable
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    private ProgressDialog progressDialog;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);

        if (toolbar!=null){
            setSupportActionBar(toolbar);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayout());

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
    }

    protected boolean interceptBackAction() {
        return false;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void setHomeAsUp() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void onBackPressed() {
        if (!interceptBackAction()) {
            hideKeyboard();
            super.onBackPressed();
        }
    }
    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public abstract
    @LayoutRes
    int getLayout();

    @Override
    public void onDestroy() {
        hideProgress();
        super.onDestroy();
    }

    public void showNoConnection(){
        Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
    }

    public void createProgressDialog(){
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(getString(R.string.loading));
        }
    }

    public void showProgress() {
        createProgressDialog();

        if(!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void hideProgress() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onError(String error) {
        hideProgress();
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    public abstract void handleBus(Object event);
}
