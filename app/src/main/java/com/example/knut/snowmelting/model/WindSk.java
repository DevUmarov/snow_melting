package com.example.knut.snowmelting.model;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class WindSk {

    private String wdir;
    private String wspd;

    public String getWdir() {
        return wdir;
    }

    public String getWspd() {
        return wspd;
    }
}
