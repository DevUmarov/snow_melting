package com.example.knut.snowmelting.api.response;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by umarov on 8/15/17.
 */

public class FilterByDateResponse implements Parcelable {

    @SerializedName("humidity_list")
    @Expose
    ArrayList<Float> humidityList;

    @SerializedName("humidity_list_date")
    @Expose
    ArrayList<String> humidityListDate;

    @SerializedName("temperature_list")
    @Expose
    ArrayList<Float> temperatureList;

    @SerializedName("temperature_list_date")
    @Expose
    ArrayList<String> temperatureListDate;

    @SerializedName("wind_speed_list")
    @Expose
    ArrayList<Float> windSpeedList;

    @SerializedName("wind_speed_list_date")
    @Expose
    ArrayList<String> windSpeedListDate;

    public ArrayList<Float> getHumidityList() {
        return humidityList;
    }

    public void setHumidityList(ArrayList<Float> humidityList) {
        this.humidityList = humidityList;
    }

    public ArrayList<String> getHumidityListDate() {
        return humidityListDate;
    }

    public void setHumidityListDate(ArrayList<String> humidityListDate) {
        this.humidityListDate = humidityListDate;
    }

    public ArrayList<Float> getTemperatureList() {
        return temperatureList;
    }

    public void setTemperatureList(ArrayList<Float> temperatureList) {
        this.temperatureList = temperatureList;
    }

    public ArrayList<String> getTemperatureListDate() {
        return temperatureListDate;
    }

    public void setTemperatureListDate(ArrayList<String> temperatureListDate) {
        this.temperatureListDate = temperatureListDate;
    }

    public ArrayList<Float> getWindSpeedList() {
        return windSpeedList;
    }

    public void setWindSpeedList(ArrayList<Float> windSpeedList) {
        this.windSpeedList = windSpeedList;
    }

    public ArrayList<String> getWindSpeedListDate() {
        return windSpeedListDate;
    }

    public void setWindSpeedListDate(ArrayList<String> windSpeedListDate) {
        this.windSpeedListDate = windSpeedListDate;
    }

    protected FilterByDateResponse(Parcel in) {
        if (in.readByte() == 0x01) {
            humidityList = new ArrayList<Float>();
            in.readList(humidityList, Float.class.getClassLoader());
        } else {
            humidityList = null;
        }
        if (in.readByte() == 0x01) {
            humidityListDate = new ArrayList<String>();
            in.readList(humidityListDate, String.class.getClassLoader());
        } else {
            humidityListDate = null;
        }
        if (in.readByte() == 0x01) {
            temperatureList = new ArrayList<Float>();
            in.readList(temperatureList, Float.class.getClassLoader());
        } else {
            temperatureList = null;
        }
        if (in.readByte() == 0x01) {
            temperatureListDate = new ArrayList<String>();
            in.readList(temperatureListDate, String.class.getClassLoader());
        } else {
            temperatureListDate = null;
        }
        if (in.readByte() == 0x01) {
            windSpeedList = new ArrayList<Float>();
            in.readList(windSpeedList, Float.class.getClassLoader());
        } else {
            windSpeedList = null;
        }
        if (in.readByte() == 0x01) {
            windSpeedListDate = new ArrayList<String>();
            in.readList(windSpeedListDate, String.class.getClassLoader());
        } else {
            windSpeedListDate = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (humidityList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(humidityList);
        }
        if (humidityListDate == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(humidityListDate);
        }
        if (temperatureList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(temperatureList);
        }
        if (temperatureListDate == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(temperatureListDate);
        }
        if (windSpeedList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(windSpeedList);
        }
        if (windSpeedListDate == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(windSpeedListDate);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<FilterByDateResponse> CREATOR = new Parcelable.Creator<FilterByDateResponse>() {
        @Override
        public FilterByDateResponse createFromParcel(Parcel in) {
            return new FilterByDateResponse(in);
        }

        @Override
        public FilterByDateResponse[] newArray(int size) {
            return new FilterByDateResponse[size];
        }
    };
}
