package com.example.knut.snowmelting.api.response;

import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class LoginResponse {

    @SerializedName("result")
    @Expose
    UserResponse userResponse;

    public UserResponse getUserResponse() {
        return userResponse;
    }

    public void setUserResponse(UserResponse userResponse) {
        this.userResponse = userResponse;
    }
}
