package com.example.knut.snowmelting.views.admin;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.RxBus;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.views.admin.feedback.FeedbackFragment;
import com.example.knut.snowmelting.views.admin.mainMenu.MainAdminPageFragment;
import com.example.knut.snowmelting.views.login.LoginActivity;
import com.example.knut.snowmelting.views.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by umarov on 5/22/17.
 */

public class AdminActivity extends BaseActivity implements AdminView, NavigationView.OnNavigationItemSelectedListener {

    static final String EXTRA_ADMIN_ID = "com.example.knut.snowmelting.extras.ADMIN_ID";

    @Inject
    Settings settings;

    @Inject
    AdminPresenter adminPresenter;

    @BindView(R.id.admin_container)
    LinearLayout adminContainer;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.toolbar_main)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.content_frame)
    FrameLayout mainContainer;

    @Inject
    NetworkUtils networkUtils;

    private Disposable internetStatusObserver;

    private int checkedPosition = 1;
    private UserResponse data;
    private ActionBarDrawerToggle mDrawerToggle;
    private MainAdminPageFragment mainAdminPageFragment;
    private FeedbackFragment mFeedbackFragment;

    public static Intent getStartIntent(Context context, UserResponse data) {
        Intent intent = new Intent(context, AdminActivity.class);
        intent.putExtra(EXTRA_ADMIN_ID, data);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        data = getIntent().getParcelableExtra(EXTRA_ADMIN_ID);
//        monitorInternetConnection();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adminPresenter.attachView(this);

        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            mainAdminPageFragment = MainAdminPageFragment.newInstance(data);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, mainAdminPageFragment)
                    .commit();
        }
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().performIdentifierAction(R.id.nav_main_page, 0);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...


        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main_admin_dashboard;
    }

    @Override
    public void handleBus(Object event) {

    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (checkedPosition != 1) {
                navigationView.getMenu().performIdentifierAction(R.id.nav_main_page, 0);
                navigationView.getMenu().getItem(0).setChecked(true);
            } else {
                super.onBackPressed();
            }
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;
        adminContainer.setBackgroundColor(getResources().getColor(R.color.default_background));

        switch (item.getItemId()) {
            case R.id.nav_main_page: {
                checkedPosition = 1;
//                adminContainer.setBackgroundResource(R.drawable.admin_background);
//                toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
                fragment = mainAdminPageFragment;
                break;
            }
//            case R.id.nav_admin_profile: {
//                checkedPosition = 2;
//                return true;
//            }
//            case R.id.nav_create_user: {
//                checkedPosition = 3;
//                return true;
//            }
            case R.id.nav_feedback: {
                checkedPosition = 2;
                toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

                if (mFeedbackFragment == null) {
                    mFeedbackFragment = FeedbackFragment.newInstance(data);
                }

                fragment = mFeedbackFragment;
                break;
            }
            case R.id.nav_log_out: {
                checkedPosition = 3;
                settings.logOutUser();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        item.setCheckable(true);

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onNoConnection() {
        Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adminPresenter.onDestroy();
    }

    public void monitorInternetConnection() {
        if (internetStatusObserver == null || internetStatusObserver.isDisposed()) {
            internetStatusObserver = networkUtils
                    .stream()
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(internetConnectionStatus -> {

                        if (RxBus.getInstance().hasObservers() && internetConnectionStatus) {
                            RxBus.getInstance().post(true);
                        }

                        if (!internetConnectionStatus) {
                            Toast.makeText(AdminActivity.this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void stopWaitForInternetComeBack() {
        if (internetStatusObserver != null || !internetStatusObserver.isDisposed()) {
            internetStatusObserver.dispose();
        }
    }
}
