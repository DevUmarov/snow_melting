package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 8/2/17.
 */

public class LastSeenValues implements Parcelable, Cloneable {
    @SerializedName("id")
    @Expose
    private float id;

    @SerializedName("humidity")
    @Expose
    private float humidity;

    @SerializedName("temperature")
    @Expose
    private float temperature;

    @SerializedName("wind_speed")
    @Expose
    private float windSpeed;

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        this.windSpeed = windSpeed;
    }

    protected LastSeenValues(Parcel in) {
        id = in.readFloat();
        humidity = in.readFloat();
        temperature = in.readFloat();
        windSpeed = in.readFloat();
    }

    public LastSeenValues(LastSeenValues lastSeenValues){
        this.setHumidity(lastSeenValues.getHumidity());
        this.setId(lastSeenValues.getId());
        this.setTemperature(lastSeenValues.getTemperature());
        this.setWindSpeed(lastSeenValues.getWindSpeed());
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(id);
        dest.writeFloat(humidity);
        dest.writeFloat(temperature);
        dest.writeFloat(windSpeed);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LastSeenValues> CREATOR = new Parcelable.Creator<LastSeenValues>() {
        @Override
        public LastSeenValues createFromParcel(Parcel in) {
            return new LastSeenValues(in);
        }

        @Override
        public LastSeenValues[] newArray(int size) {
            return new LastSeenValues[size];
        }
    };
}