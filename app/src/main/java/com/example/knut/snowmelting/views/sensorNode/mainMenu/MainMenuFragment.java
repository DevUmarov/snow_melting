package com.example.knut.snowmelting.views.sensorNode.mainMenu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.ViewUtils;
import android.view.View;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.LastSeenValues;
import com.example.knut.snowmelting.api.response.subModels.SensorNodeResponse;
import com.example.knut.snowmelting.api.response.subModels.SensorValuesResponse;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.AppUtils;
import com.example.knut.snowmelting.views.dashboard.sensorNodes.SensorNodesFragment;
import com.example.knut.snowmelting.widgets.SensorDataView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 6. 12.
 */

public class MainMenuFragment extends BaseFragment implements MainMenuView {

    static final String EXTRA_LAST_SEEN = "com.example.knut.snowmelting.extras.LAST_SEEN";

    @Inject
    MainMenuPresenter mainMenuPresenter;
//    ArrayList<SensorValuesResponse> sensorLastSeenValuesList;

    LastSeenValues lastSeenValues;

    @BindView(R.id.sensor_views_container)
    GridLayout gridLayout;

    public static MainMenuFragment newInstance(LastSeenValues lastSeenValues) {

        MainMenuFragment fragment = new MainMenuFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_LAST_SEEN, lastSeenValues);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main_menu;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);

        mainMenuPresenter.attachView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        if (getArguments() != null) {
            lastSeenValues = getArguments().getParcelable(EXTRA_LAST_SEEN);

            if (lastSeenValues != null) {
                initData();
            }
        }

        super.onViewCreated(view, savedInstanceState);
    }

    private void initData() {

        for (int i = 0; i < 3; i++) {
            SensorDataView sensorDataView = new SensorDataView(getActivity());
            GridLayout.LayoutParams layoutParams = new GridLayout.LayoutParams();
            layoutParams.width = 0;
            layoutParams.rowSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);
            layoutParams.columnSpec = GridLayout.spec(GridLayout.UNDEFINED, 1f);

            if ((i + 1) % 2 == 0) {
                layoutParams.setMargins(AppUtils.dpToPx(2), 0, 0, 0);
            } else {
                layoutParams.setMargins(0, 0, AppUtils.dpToPx(2), 0);
            }

            sensorDataView.setLayoutParams(layoutParams);

            gridLayout.addView(sensorDataView);

            switch (i) {
                case 0: {
                    int maxValue = 60;

                    if (maxValue < lastSeenValues.getHumidity()) {
                        maxValue = (int) lastSeenValues.getHumidity() + 10;
                    }

                    sensorDataView.setData(lastSeenValues.getHumidity(), lastSeenValues.getHumidity(), getString(R.string.humidity), 0, maxValue);
                    break;
                }
                case 1: {
                    int maxValue = 50;
                    int minValue = -50;

                    if (maxValue < lastSeenValues.getTemperature()) {
                        maxValue = (int) lastSeenValues.getTemperature() + 10;
                        minValue = (-1) * maxValue;
                    } else if (minValue > lastSeenValues.getTemperature()) {
                        minValue = (int) lastSeenValues.getTemperature() - 10;
                        maxValue = (-1) * minValue;
                    }

                    sensorDataView.setData(lastSeenValues.getTemperature(), lastSeenValues.getTemperature() + maxValue, getString(R.string.temperature), minValue, maxValue);
                    break;
                }
                case 2: {
                    int maxValue = 25;

                    if (maxValue < lastSeenValues.getWindSpeed()) {
                        maxValue = (int) lastSeenValues.getWindSpeed() + 10;
                    }

                    sensorDataView.setData(lastSeenValues.getWindSpeed(), lastSeenValues.getWindSpeed(), getString(R.string.wind_speed), 0, maxValue);
                    break;
                }
            }
        }
    }
}
