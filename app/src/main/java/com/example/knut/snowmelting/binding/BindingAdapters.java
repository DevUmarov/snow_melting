package com.example.knut.snowmelting.binding;

import android.databinding.BindingAdapter;
import android.widget.TextView;

/**
 * Created by umarov on 4/28/17.
 */

public abstract class BindingAdapters {

    @BindingAdapter("android:typeface")
    public abstract void setTypeface(TextView v, String style);
}
