package com.example.knut.snowmelting.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by umarov on 7/4/17.
 */

public final class AppTimeUtils {

    private AppTimeUtils(){}

    public static String getDate(long timeStamp){

        if(timeStamp == 0){
            return "";
        }

        try{
            DateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }


    public static String getDateForDataHistory(long timeStamp){

        if(timeStamp == 0){
            return "";
        }

        try{
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

}
