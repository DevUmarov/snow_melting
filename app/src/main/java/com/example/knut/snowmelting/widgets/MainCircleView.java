package com.example.knut.snowmelting.widgets;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.utils.AppUtils;
import com.google.android.gms.maps.model.Circle;

/**
 * Created by umarov on 5/22/17.
 */

public class MainCircleView extends LinearLayout {

    private TextView circleTitle;
    private CircleView circleView;
    private GestureDetector gestureDetector;
    private View.OnClickListener listener;

    public MainCircleView(Context context, View.OnClickListener onClickListener) {
        super(context);
        init();
        listener = onClickListener;
        gestureDetector  = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                listener.onClick(MainCircleView.this);
                return true;
            }
        });
    }

    public MainCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MainCircleView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        gestureDetector.onTouchEvent(ev);
        return super.onInterceptTouchEvent(ev);
    }

    public void setData(int color, int count, String title){
        circleView.setData(color, String.valueOf(count));
        circleTitle.setText(title);
    }

    public void setData(int color, String text){
        circleView.setData(color, text);
        circleTitle.setVisibility(View.GONE);
    }

    private void init() {
        inflate(getContext(), R.layout.view_circle_main, this);
        setClickable(true);
        setFocusable(true);
        circleView = (CircleView) findViewById(R.id.circle_view);
        circleTitle = (TextView) findViewById(R.id.circle_title);
    }
}
