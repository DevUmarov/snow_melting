package com.example.knut.snowmelting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 24/02/2017.
 */

public class Fcst3hour {

    @SerializedName("wind")
    @Expose
    private WindSk wind;
//    @SerializedName("precipitation")
//    @Expose
//    private PercipitationSk precipitation;
    @SerializedName("temperature")
    @Expose
    private TemperatureSk temperature;
//    @SerializedName("humidity")
//    @Expose
//    private HumiditySk humidity;
    @SerializedName("sky")
    @Expose
    private SkySk sky;


    public WindSk getWind() {
        return wind;
    }

    public void setWind(WindSk wind) {
        this.wind = wind;
    }

//    public PercipitationSk getPrecipitation() {
//        return precipitation;
//    }
//
//    public void setPrecipitation(PercipitationSk precipitation) {
//        this.precipitation = precipitation;
//    }

    public TemperatureSk getTemperature() {
        return temperature;
    }

    public void setTemperature(TemperatureSk temperature) {
        this.temperature = temperature;
    }

//    public HumiditySk getHumidity() {
//        return humidity;
//    }
//
//    public void setHumidity(HumiditySk humidity) {
//        this.humidity = humidity;
//    }

    public SkySk getSky() {
        return sky;
    }

    public void setSky(SkySk sky) {
        this.sky = sky;
    }
}
