package com.example.knut.snowmelting.views.usersList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerUserListAdapter;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.views.dashboard.DashboardActivity;
import com.example.knut.snowmelting.views.locationList.LocationListActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 5. 24.
 */

public class UsersListActivity extends BaseActivity implements UsersListView {

    @BindView(R.id.recycler_view_user_list)
    RecyclerView recyclerView;

    @Inject
    UsersListPresenter usersListPresenter;

    private RecyclerUserListAdapter recyclerUserListAdapter;
    private ProgressDialog progressDialog;
    private List<UserResponse> listOfUsers;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, UsersListActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        initializeToolbar();

        initializeRecyclerView();

        usersListPresenter.attachView(this);
//        showProgress(this);
        usersListPresenter.getAllUsers();
    }

    private void initializeToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Users' list");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
        }
    }

    private void initializeRecyclerView() {
        recyclerUserListAdapter = new RecyclerUserListAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerUserListAdapter);
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_users_list;
    }

    @Override
    public void handleBus(Object event) {

    }

    /**
     * MVP methods implementation
     **/

    @Override
    public void onUserDataArrived(List<UserResponse> data) {
//        hideProgress();
        listOfUsers = data;
        recyclerUserListAdapter.setData(data);
        recyclerUserListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onEditButtonClick(UserResponse userResponse) {
        Toast.makeText(this, "Edit", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteButtonClick(UserResponse userResponse) {
        Toast.makeText(this, "Delete", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onViewButtonClick(UserResponse userResponse) {
        Toast.makeText(this, "View", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(View view, UserResponse userResponse) {
//        startActivity(LocationListActivity.getStartIntent(this, userResponse));
    }

    /*** MVP Implementation ***/


}
