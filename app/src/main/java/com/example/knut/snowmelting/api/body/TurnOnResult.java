package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by umarov on 9/27/17.
 */

public class TurnOnResult {

    @SerializedName("data_from_io_on")
    @Expose
    ArrayList<String> dataFromIOn;

    @SerializedName("mc_status_list_on")
    @Expose
    ArrayList<Integer> mcStatusListOn;

    @SerializedName("type")
    @Expose
    String type;

    public ArrayList<String> getDataFromIOn() {
        return dataFromIOn;
    }

    public void setDataFromIOn(ArrayList<String> dataFromIOn) {
        this.dataFromIOn = dataFromIOn;
    }

    public ArrayList<Integer> getMcStatusListOn() {
        return mcStatusListOn;
    }

    public void setMcStatusListOn(ArrayList<Integer> mcStatusListOn) {
        this.mcStatusListOn = mcStatusListOn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
