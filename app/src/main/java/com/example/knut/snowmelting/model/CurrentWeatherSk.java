package com.example.knut.snowmelting.model;

import com.example.knut.snowmelting.model.test.Forecast;
import com.example.knut.snowmelting.model.test.Forecast6days;

/**
 * Created by Nizomjon on 24/02/2017.
 */

public class CurrentWeatherSk {

    private SkWeather skWeather;
    private Fcst3hour fcst3hour;
    private Forecast forecast;
    private Forecast6days forecast6days;

    public CurrentWeatherSk(SkWeather skWeather, Forecast6days forecast6days) {
        this.skWeather = skWeather;
        this.forecast6days = forecast6days;
    }

    public CurrentWeatherSk(SkWeather skWeather, Fcst3hour fcst3hour) {
        this.skWeather = skWeather;
        this.fcst3hour = fcst3hour;
    }

    public CurrentWeatherSk(SkWeather skWeather, Forecast forecast) {
        this.skWeather = skWeather;
        this.forecast = forecast;
    }

    public Forecast getForecast() {
        return forecast;
    }

    public SkWeather getSkWeather() {
        return skWeather;
    }

    public void setSkWeather(SkWeather skWeather) {
        this.skWeather = skWeather;
    }

    public Fcst3hour getFcst3hour() {
        return fcst3hour;
    }

    public void setFcst3hour(Fcst3hour fcst3hour) {
        this.fcst3hour = fcst3hour;
    }

    public Forecast6days getForecast6days() {
        return forecast6days;
    }
}
