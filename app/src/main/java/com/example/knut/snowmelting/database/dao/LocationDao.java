package com.example.knut.snowmelting.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.knut.snowmelting.database.entities.LocationEntity;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by islom on 17. 6. 8.
 */

@Dao
public interface LocationDao {

    @Insert(onConflict = REPLACE)
    void insertLocation(LocationEntity locationEntity);

    @Query("SELECT * FROM location")
    List<LocationEntity> getAllLocations();

    @Query("SELECT * FROM location WHERE area_code = :areaCode")
    List<LocationEntity> getLocationsByAreaCode(int areaCode);

    @Query("SELECT * FROM location WHERE user_id = :userId")
    List<LocationEntity> getLocationsByUserId(int userId);
}
