package com.example.knut.snowmelting.views.xxxanalysis;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerAnalysisAdapter;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by umarov on 4/20/17.
 */

public class AnalysisActivity extends BaseActivity implements AnalysisView, AdapterView.OnItemSelectedListener {

    @Inject
    AnalysisPresenter analysisPresenter;

    @BindView(R.id.recycler_analysis)
    RecyclerView recyclerView;

    @BindView(R.id.spinner_weather)
    Spinner spinnerWeather;

    @BindView(R.id.spinner_years)
    Spinner spinnerYears;

    static final String EXTRA_LOGIN = "com.example.knut.snowmelting.extras.MAP_ID";
    private UserResponse data;

    public static Intent getStartIntent(Context context, UserResponse data) {
        Intent intent = new Intent(context, AnalysisActivity.class);
        intent.putExtra(EXTRA_LOGIN, data);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        if (getIntent() != null) {
            data = getIntent().getParcelableExtra(EXTRA_LOGIN);
        }


        initSpinners();
        initializeToolbar();
        initializeRecyclerView();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_xxxanalysis;
    }

    @Override
    public void handleBus(Object event) {

    }

    private void initSpinners() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.weather));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerWeather.setAdapter(spinnerArrayAdapter);

        spinnerArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.years));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerYears.setAdapter(spinnerArrayAdapter);

        spinnerWeather.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        spinnerYears.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);

        spinnerWeather.setOnItemSelectedListener(this);
        spinnerYears.setOnItemSelectedListener(this);
    }

    private void initializeToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Analysis " + data.getUserName());
        }
    }

    private void initializeRecyclerView() {
        RecyclerAnalysisAdapter recyclerWeatherDataAdapter = new RecyclerAnalysisAdapter(this, recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerWeatherDataAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ((TextView) parent.getChildAt(0)).setTextSize(14);
        ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
