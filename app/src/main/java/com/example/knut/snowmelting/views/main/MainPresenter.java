package com.example.knut.snowmelting.views.main;

import android.content.Context;

import com.example.knut.snowmelting.api.SkService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.WeatherDay;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.Settings;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by umarov on 6/22/17.
 */

public class MainPresenter extends BasePresenter<MainView> {
    Context mContext;

    public MainPresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }
}
