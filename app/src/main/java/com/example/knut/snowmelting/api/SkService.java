package com.example.knut.snowmelting.api;

import com.example.knut.snowmelting.model.SkWeather;
import com.example.knut.snowmelting.model.WeatherData;
import com.example.knut.snowmelting.model.WeatherDay;
import com.example.knut.snowmelting.model.test.Response;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Nizomjon on 21/02/2017.
 */

public interface SkService {
//    @GET("/weather/current/minutely?&appKey=" + "4ce0462a-3884-30ab-ab13-93efb1bc171f" + "&version=1")
//    Observable<SkWeather> getCurrentWeatherCondition(@Query("lon") double longitude, @Query("lat") double latitude); // SKY_O01
//
//    @GET("/weather/forecast/3hours?version=1&appKey=" + "4ce0462a-3884-30ab-ab13-93efb1bc171f")
//    Observable<Response> sync(@Query("lat") double latitude, @Query("lon") double longitude); // SKY_V01
//
//    @GET("/weather/forecast/3days?version=1&appKey=" + "4ce0462a-3884-30ab-ab13-93efb1bc171f")
//    Observable<Response> getForeCast3days(@Query("lat") double latitude, @Query("lon") double longitude); // SKY_S01
//
//    @GET("/weather/forecast/6days?version=1&appKey=" + "4ce0462a-3884-30ab-ab13-93efb1bc171f")
//    Observable<Response> getForecast6days(@Query("lat") double latitude, @Query("lon") double longitude); // SKY_W01

    @GET("/weather/current/hourly")
    Observable<WeatherDay> getHourlyWeather(@Query("appKey") String APP_KEY,
                                         @Query("version") int ver,
                                         @Query("lat") double latitude,
                                         @Query("lon") double longitude);
}
