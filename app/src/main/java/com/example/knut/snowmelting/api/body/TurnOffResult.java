package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by umarov on 9/27/17.
 */

public class TurnOffResult {

    @SerializedName("data_from_io_off")
    @Expose
    ArrayList<String> dataFromIOff;

    @SerializedName("mc_status_list_off")
    @Expose
    ArrayList<Integer> mcStatusListOff;

    @SerializedName("type")
    @Expose
    String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<String> getDataFromIOff() {
        return dataFromIOff;
    }

    public void setDataFromIOff(ArrayList<String> dataFromIOff) {
        this.dataFromIOff = dataFromIOff;
    }

    public ArrayList<Integer> getMcStatusListOff() {
        return mcStatusListOff;
    }

    public void setMcStatusListOff(ArrayList<Integer> mcStatusListOff) {
        this.mcStatusListOff = mcStatusListOff;
    }
}
