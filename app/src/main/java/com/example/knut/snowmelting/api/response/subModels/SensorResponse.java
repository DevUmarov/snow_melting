package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 6. 8.
 */

public class SensorResponse implements Parcelable {

    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("sensor_node_id")
    @Expose
    int sensorNodeId;

    @SerializedName("sensor_type")
    @Expose
    String sensorType;

    @SerializedName("sensor_values")
    @Expose
    List<SensorValuesResponse> sensorValuesList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSensorNodeId() {
        return sensorNodeId;
    }

    public void setSensorNodeId(int sensorNodeId) {
        this.sensorNodeId = sensorNodeId;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public List<SensorValuesResponse> getSensorValuesList() {
        return sensorValuesList;
    }

    public void setSensorValuesList(List<SensorValuesResponse> sensorValuesList) {
        this.sensorValuesList = sensorValuesList;
    }

    protected SensorResponse(Parcel in) {
        id = in.readInt();
        sensorNodeId = in.readInt();
        sensorType = in.readString();
        if (in.readByte() == 0x01) {
            sensorValuesList = new ArrayList<SensorValuesResponse>();
            in.readList(sensorValuesList, SensorValuesResponse.class.getClassLoader());
        } else {
            sensorValuesList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(sensorNodeId);
        dest.writeString(sensorType);
        if (sensorValuesList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sensorValuesList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SensorResponse> CREATOR = new Parcelable.Creator<SensorResponse>() {
        @Override
        public SensorResponse createFromParcel(Parcel in) {
            return new SensorResponse(in);
        }

        @Override
        public SensorResponse[] newArray(int size) {
            return new SensorResponse[size];
        }
    };
}