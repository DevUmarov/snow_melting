package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringRes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 6. 8.
 */
public class SensorNodeResponse implements Parcelable {

    @SerializedName("node_unique_id")
    @Expose
    private String nodeUniqueId;

    @SerializedName("id")
    @Expose
    private int sensorNodeId;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("history")
    @Expose
    private List<OnOffHistoryResponse> onOffHistoryResponsesList;

    @SerializedName("last_seen_values")
    @Expose
    private LastSeenValues lastSeenValues;

    @SerializedName("analysis")
    @Expose
    private AnalysisResponse analysisResponse;

    public String getNodeUniqueId() {
        return nodeUniqueId;
    }

    public void setNodeUniqueId(String nodeUniqueId) {
        this.nodeUniqueId = nodeUniqueId;
    }

    public int getSensorNodeId() {
        return sensorNodeId;
    }

    public void setSensorNodeId(int sensorNodeId) {
        this.sensorNodeId = sensorNodeId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public List<OnOffHistoryResponse> getOnOffHistoryResponsesList() {
        return onOffHistoryResponsesList;
    }

    public void setOnOffHistoryResponsesList(List<OnOffHistoryResponse> onOffHistoryResponsesList) {
        this.onOffHistoryResponsesList = onOffHistoryResponsesList;
    }

    public LastSeenValues getLastSeenValues() {
        return lastSeenValues;
    }

    public void setLastSeenValues(LastSeenValues lastSeenValues) {
        this.lastSeenValues = lastSeenValues;
    }

    public AnalysisResponse getAnalysisResponse() {
        return analysisResponse;
    }

    public void setAnalysisResponse(AnalysisResponse analysisResponse) {
        this.analysisResponse = analysisResponse;
    }

    protected SensorNodeResponse(Parcel in) {
        nodeUniqueId = in.readString();
        sensorNodeId = in.readInt();
        latitude = in.readDouble();
        longitude = in.readDouble();
        if (in.readByte() == 0x01) {
            onOffHistoryResponsesList = new ArrayList<OnOffHistoryResponse>();
            in.readList(onOffHistoryResponsesList, OnOffHistoryResponse.class.getClassLoader());
        } else {
            onOffHistoryResponsesList = null;
        }
        lastSeenValues = (LastSeenValues) in.readValue(LastSeenValues.class.getClassLoader());
        analysisResponse = (AnalysisResponse) in.readValue(AnalysisResponse.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nodeUniqueId);
        dest.writeInt(sensorNodeId);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        if (onOffHistoryResponsesList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(onOffHistoryResponsesList);
        }
        dest.writeValue(lastSeenValues);
        dest.writeValue(analysisResponse);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SensorNodeResponse> CREATOR = new Parcelable.Creator<SensorNodeResponse>() {
        @Override
        public SensorNodeResponse createFromParcel(Parcel in) {
            return new SensorNodeResponse(in);
        }

        @Override
        public SensorNodeResponse[] newArray(int size) {
            return new SensorNodeResponse[size];
        }
    };
}