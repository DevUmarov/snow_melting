package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.knut.snowmelting.api.response.subModels.SensorNodeResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 5. 30.
 */

public class LocationResponse implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("camera_url")
    @Expose
    private String cameraUrl;

    @SerializedName("postal_code")
    @Expose
    private String postalCode;

    @SerializedName("road_address")
    @Expose
    private String roadAddress;

    @SerializedName("user_id")
    @Expose
    private int userId;

    @SerializedName("sensor_nodes")
    @Expose
    private List<SensorNodeResponse> sensorNodesList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCameraUrl() {
        return cameraUrl;
    }

    public void setCameraUrl(String cameraUrl) {
        this.cameraUrl = cameraUrl;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getRoadAddress() {
        return roadAddress;
    }

    public void setRoadAddress(String roadAddress) {
        this.roadAddress = roadAddress;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public List<SensorNodeResponse> getSensorNodesList() {
        return sensorNodesList;
    }

    public void setSensorNodesList(List<SensorNodeResponse> sensorNodesList) {
        this.sensorNodesList = sensorNodesList;
    }

    protected LocationResponse(Parcel in) {
        id = in.readInt();
        address = in.readString();
        cameraUrl = in.readString();
        postalCode = in.readString();
        roadAddress = in.readString();
        userId = in.readInt();
        if (in.readByte() == 0x01) {
            sensorNodesList = new ArrayList<SensorNodeResponse>();
            in.readList(sensorNodesList, SensorNodeResponse.class.getClassLoader());
        } else {
            sensorNodesList = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(address);
        dest.writeString(cameraUrl);
        dest.writeString(postalCode);
        dest.writeString(roadAddress);
        dest.writeInt(userId);
        if (sensorNodesList == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sensorNodesList);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<LocationResponse> CREATOR = new Parcelable.Creator<LocationResponse>() {
        @Override
        public LocationResponse createFromParcel(Parcel in) {
            return new LocationResponse(in);
        }

        @Override
        public LocationResponse[] newArray(int size) {
            return new LocationResponse[size];
        }
    };
}