package com.example.knut.snowmelting.mvp;


import com.example.knut.snowmelting.base.RxBus;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Umarov Islom on 02/06/17.
 */

public class BasePresenter<T extends MvpView> implements Presenter<T> {

    private T mvpView;

    private CompositeDisposable compositeDisposable;
//    private Disposable internetStatusObserver;

    @Override
    public void attachView(T mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void detachView() {
        this.mvpView = null;
    }

    @Override
    public void onDestroy() {
        unSubscribeAll();
    }

    public boolean isViewAttached() {
        return mvpView != null;
    }

    public T getMvpView() {
        return mvpView;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) throw new MvpViewNotAttachedException();
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before" +
                    " requesting data to the Presenter");
        }
    }

    private CompositeDisposable getCompositeDisposable() {
        if (compositeDisposable == null || compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        return compositeDisposable;
    }

    protected void unSubscribeAll() {
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }
    }

    protected <T> void registerAsync(Observable<T> observable , DisposableObserver<T> observer){
        getCompositeDisposable().add( observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer));
    }

    protected <T> void registerSync(Observable<T> observable , DisposableObserver<T> observer){
        getCompositeDisposable().add( observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer));
    }

    protected <T> void subscribe(Observable<T> observable , DisposableObserver<T> observer){

        getCompositeDisposable().add( observable
                .subscribeWith(observer));
    }
}
