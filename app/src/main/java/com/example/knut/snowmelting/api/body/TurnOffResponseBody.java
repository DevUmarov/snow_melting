package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 9/25/17.
 */

public class TurnOffResponseBody {
    @SerializedName("result")
    @Expose
    private TurnOffResult result;

    public TurnOffResult getResult() {
        return result;
    }

    public void setResult(TurnOffResult result) {
        this.result = result;
    }
}
