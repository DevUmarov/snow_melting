package com.example.knut.snowmelting.model;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class SkySk {

    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

}
