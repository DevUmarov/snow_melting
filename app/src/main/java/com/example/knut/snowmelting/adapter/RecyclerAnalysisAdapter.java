package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.commons.ExpandableItemStates;
import com.example.knut.snowmelting.databinding.ItemAnalysisBinding;
import com.example.knut.snowmelting.model.WeatherData;

import java.util.ArrayList;

/**
 * Created by umarov on 4/21/17.
 */

public class RecyclerAnalysisAdapter extends RecyclerView.Adapter<RecyclerAnalysisAdapter.RecyclerViewHolder> {

    Context mContext;
    RecyclerView recyclerView;
    ExpandableItemStates itemStates;
    ArrayList<WeatherData> data;

    public RecyclerAnalysisAdapter(Context context, RecyclerView recyclerView) {
        mContext = context;
        itemStates = new ExpandableItemStates(this);
        this.recyclerView = recyclerView;
        initData();
    }

    private void initData() {
        data = new ArrayList<>();

        for (int i = 2017; i >= 1990; i--) {
            WeatherData obj;

            if (i % 2 == 0) {
                obj = new WeatherData(i, 88, 12, 234, -17, "Freezing");
            } else {
                obj = new WeatherData(i, 68, 21, 134, 10, "Mostly Cloudy");
            }

            data.add(obj);
        }
    }

    @Override
    public RecyclerAnalysisAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemAnalysisBinding itemBinding = ItemAnalysisBinding.inflate(layoutInflater, parent, false);
        return new RecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerAnalysisAdapter.RecyclerViewHolder holder, int position) {
        holder.bind(data.get(position), itemStates.isExpanded(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemAnalysisBinding binding;

        private RecyclerViewHolder(ItemAnalysisBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setOnClick(this);
        }

        public void bind(WeatherData item, boolean isExpanded) {
            binding.setAnalysisData(item);
            binding.setIsExpanded(isExpanded);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            itemStates.toggle(getAdapterPosition());
        }
    }
}
