package com.example.knut.snowmelting.views.locationList;

import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.mvp.MvpView;

import java.util.List;

/**
 * Created by islom on 17. 5. 30.
 */

public interface LocationListView extends MvpView {

    void onLocationsDataArrived(List<LocationResponse> data);

    void onItemClicked(int clickedPst, LocationResponse location);
}
