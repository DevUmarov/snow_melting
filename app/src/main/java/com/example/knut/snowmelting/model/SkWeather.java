package com.example.knut.snowmelting.model;

import com.example.knut.snowmelting.model.test.Precipitation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class SkWeather {
//
    @SerializedName("weather")
    @Expose
    private WeatherSk weather;
    @SerializedName("common")
    @Expose
    private CommonSk common;
    @SerializedName("result")
    @Expose
    private ResultSk result;


    private List<Precipitation> precipitationList;

    public List<Precipitation> getPrecipitationList() {
        return precipitationList;
    }

    public WeatherSk getWeather() {
        return weather;
    }

    public void setWeather(WeatherSk weather) {
        this.weather = weather;
    }

    public CommonSk getCommon() {
        return common;
    }

    public void setCommon(CommonSk common) {
        this.common = common;
    }

    public ResultSk getResult() {
        return result;
    }

    public void setResult(ResultSk result) {
        this.result = result;
    }
}

