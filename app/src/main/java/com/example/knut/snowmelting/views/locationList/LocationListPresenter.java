package com.example.knut.snowmelting.views.locationList;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.api.response.subModels.SensorNodeResponse;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.database.AppDatabase;
import com.example.knut.snowmelting.database.entities.LocationEntity;
import com.example.knut.snowmelting.database.entities.SensorNodeEntity;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by islom on 17. 5. 30.
 */

public class LocationListPresenter extends BasePresenter<LocationListView> {

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

//    @Inject
//    NetworkUtils networkUtils;

    @Inject
    Settings settings;

    @Inject
    AppDatabase appDatabase;

    private Context context;
    private int areaCode;
    private String areaName;
    private int userId;
    //    private boolean isDataLoadedAtLeastOne;
    private Disposable internetStatusObserver;
    private List<LocationResponse> locationResponses;

    public LocationListPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);

        userId = 0;

        this.context = context;
    }

//    private Observable<List<LocationResponse>> getLocationsDBAreaCode(int areaCode) {
//        Observable<List<LocationEntity>> observable;
//
//        if (areaCode == 0) {
//            observable = Observable.defer(new Callable<ObservableSource<? extends List<LocationEntity>>>() {
//                @Override
//                public ObservableSource<? extends List<LocationEntity>> call() throws Exception {
//                    return Observable.just(appDatabase.locationDao().getAllLocations());
//                }
//            });
//        } else {
//            observable = Observable.defer(new Callable<ObservableSource<? extends List<LocationEntity>>>() {
//                @Override
//                public ObservableSource<? extends List<LocationEntity>> call() throws Exception {
//                    return Observable.just(appDatabase.locationDao().getLocationsByAreaCode(areaCode));
//                }
//            });
//        }
//
//        return observable.map(locations -> fromDbToResponse(locations));
//    }

//    private Observable<List<LocationResponse>> getLocationsDBUserId(final int userId) {
//        Observable<List<LocationEntity>> observable;
//
//        observable = Observable.defer(new Callable<ObservableSource<? extends List<LocationEntity>>>() {
//            @Override
//            public ObservableSource<? extends List<LocationEntity>> call() throws Exception {
//                return Observable.just(appDatabase.locationDao().getLocationsByUserId(userId));
//            }
//        });
//
//        return observable.map(locations -> {
//            return fromDbToResponse(locations);
//        });
//    }

//    private List<LocationResponse> fromDbToResponse(List<LocationEntity> locations) {
//
//        List<LocationResponse> listResponses = new ArrayList<>();
//
//        for (LocationEntity locationEntity : locations) {
//
//            LocationResponse response = new LocationResponse();
//            response.setId(locationEntity.id);
//            response.setAddress(locationEntity.address);
////            response.setAreaCode(locationEntity.areaCode);
//
//            List<SensorNodeEntity> nodes = appDatabase
//                    .sensorNodeDao()
//                    .getSensorNodesByLocationId(locationEntity.id);
//
//            response.setSensorNodesList(new ArrayList<>());
//
//            for (SensorNodeEntity node : nodes) {
//                response.getSensorNodesList().add(new SensorNodeResponse(
//                        node.sensorNodeId,
//                        node.latitude,
//                        node.longitude
//                ));
//            }
//
//            listResponses.add(response);
//        }
//
//        return listResponses;
//    }

//    private void fromResponseToDb(List<LocationResponse> locationResponses) {
//
//        for (LocationResponse listResponse : locationResponses) {
//
//            appDatabase.locationDao().insertLocation(
//                    new LocationEntity(listResponse.getId(),
//                            listResponse.getUserId(),
//                            listResponse.getAddress()));
//
//            for (SensorNodeResponse node : listResponse.getSensorNodesList()) {
//
//                appDatabase.sensorNodeDao().insertSensorNode(
//                        new SensorNodeEntity(node.getSensorNodeId(),
//                                listResponse.getId(),
//                                node.getLatitude(),
//                                node.getLongitude()));
//            }
//        }
//    }


    public void getLocationsByArea(String areaName) {

        unSubscribeAll();

        this.areaName = areaName;

        switch (this.areaName) {
            case "chungju": {
                areaCode = 1001;
                break;
            }
            case "seoul": {
                areaCode = 1002;
                break;
            }
            default: {
                areaCode = 0;
            }
        }

//        Observable<List<LocationResponse>> responseDB =
//                getLocationsDBAreaCode(areaCode)
//                        .filter(response -> response.size() > 0)
//                        .doOnNext(data -> {
//                            isDataLoadedAtLeastOne = true;
//                            locationResponses = data;
//                        })
//                        .subscribeOn(Schedulers.computation());

//        Observable<List<LocationResponse>> responseApi =
//                restService.getLocationsByArea(areaName)
//                        .map(locationList -> {
//                            Observable.create(disposable -> {
////                                fromResponseToDb(locationList);
//                                disposable.onComplete();
//                            }).subscribeOn(Schedulers.computation()).subscribe();
//
//                            return locationList;
//                        })
//                        .subscribeOn(Schedulers.io());

//        Observable.concat(responseDB, responseApi)

        registerAsync(restService.getLocationsByArea(areaName), new DisposableObserver<List<LocationResponse>>() {
            @Override
            public void onNext(@NonNull List<LocationResponse> data) {
//                isDataLoadedAtLeastOne = true;
                locationResponses = data;
                getMvpView().onLocationsDataArrived(data);
            }

            @Override
            public void onError(@NonNull Throwable e) {
//                if (!isDataLoadedAtLeastOne) {
//                    waitInternetConnection(() -> {
//                        tryToLoadAgain();
//                    });
                getMvpView().onError(e.getMessage());
//                } else {
//                    getMvpView().onLocationsDataArrived(locationResponses);
//                }
            }

            @Override
            public void onComplete() {
            }
        });
    }

    public void getLocationsByUserId(int userId) {
        this.userId = userId;

        unSubscribeAll();

//        Observable<List<LocationResponse>> responseDB =
//                getLocationsDBUserId(userId)
//                        .filter(response -> response.size() > 0)
//                        .doOnNext(data -> {
//                            isDataLoadedAtLeastOne = true;
//                            locationResponses = data;
//                        })
//                        .subscribeOn(Schedulers.computation());

        Observable<List<LocationResponse>> responseApi =
                restService.getSingelUserData(userId)
                        .map(singleUserResponse -> {

                            Observable.create(disposable -> {

//                                fromResponseToDb(singleUserResponse.getLocationList());
                                disposable.onComplete();

                            }).subscribeOn(Schedulers.computation())
                                    .subscribe();

                            return singleUserResponse.getLocationList();
                        })
                        .subscribeOn(Schedulers.io());
//        Observable.concat(responseDB, responseApi)

        registerSync(responseApi, new DisposableObserver<List<LocationResponse>>() {
            @Override
            public void onNext(@NonNull List<LocationResponse> data) {
//                isDataLoadedAtLeastOne = true;
                locationResponses = data;
                getMvpView().onLocationsDataArrived(data);
            }

            @Override
            public void onError(@NonNull Throwable e) {
//                if (!isDataLoadedAtLeastOne) {
//                    waitInternetConnection(() -> {
//                        tryToLoadAgain();
//                    });
                getMvpView().onError(e.getMessage());
//                } else {
//                    getMvpView().onLocationsDataArrived(locationResponses);
//                }

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void tryToLoadAgain() {
        if (areaName != null && !areaName.isEmpty()) {
            getLocationsByArea(areaName);
        } else {
            getLocationsByUserId(userId);
        }
    }

}
