package com.example.knut.snowmelting.views.sensorNode;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.body.FilterByDateBody;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.annotations.NonNull;

/**
 * Created by islom on 17. 6. 1.
 */

public class SensorNodePresenter extends BasePresenter<SensorNodeView> {

    Context mContext;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    public SensorNodePresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }

//    public void getFullSensorData(String sensorNodeId){
//        registerAsync(restService.getFullSensorNodeData(sensorNodeId), new ApiDisposableCallback<SensorNodeResponse>() {
//            @Override
//            public void onAPIError(@NonNull Exception e) {
//                getMvpView().onError(e.getMessage());
//            }
//
//            @Override
//            public void onNext(@NonNull SensorNodeResponse sensorNodeResponse) {
//                getMvpView().onFullSensorNodeDataArrived(sensorNodeResponse);
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });
//    }

}
