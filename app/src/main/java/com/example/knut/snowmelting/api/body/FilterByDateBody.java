package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 8/15/17.
 */

public class FilterByDateBody {

    @SerializedName("start_time")
    @Expose
    String startTime;

    @SerializedName("end_time")
    @Expose
    String endTime;

    @SerializedName("sensor_node_id")
    @Expose
    int sensorNodeId;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getSensorNodeId() {
        return sensorNodeId;
    }

    public void setSensorNodeId(int sensorNodeId) {
        this.sensorNodeId = sensorNodeId;
    }
}
