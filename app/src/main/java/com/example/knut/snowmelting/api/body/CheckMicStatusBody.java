package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.zip.CheckedInputStream;

/**
 * Created by Islom on 01/12/2017.
 */

public class CheckMicStatusBody {

    @SerializedName("result")
    @Expose
    CheckMicStatusResult checkMicStatusResult;

    public CheckMicStatusResult getConnectionResult() {
        return checkMicStatusResult;
    }

    public void setCheckMicStatusResult(CheckMicStatusResult checkMicStatusResult) {
        this.checkMicStatusResult = checkMicStatusResult;
    }
}
