package com.example.knut.snowmelting.views.sensorNode.dataHistory;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.body.FilterByDateBody;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.annotations.NonNull;

/**
 * Created by islom on 17. 6. 12.
 */

public class DataHistoryPresenter extends BasePresenter<DataHistoryView> {


    Context mContext;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    public DataHistoryPresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }

    public void getDataChange(int sendorNodeId, String startTime, String endTime) {

        unSubscribeAll();

        FilterByDateBody filterByDateBody = new FilterByDateBody();
        filterByDateBody.setSensorNodeId(sendorNodeId);
        filterByDateBody.setEndTime(endTime);
        filterByDateBody.setStartTime(startTime);

        registerAsync(restService.getDataChange(filterByDateBody), new ApiDisposableCallback<FilterByDateResponse>() {
            @Override
            public void onAPIError(@NonNull Exception e) {
                if (isViewAttached()) {
                    getMvpView().onError(e.getMessage());
                }
            }

            @Override
            public void onNext(@NonNull FilterByDateResponse filterByDateResponse) {
                if (isViewAttached()) {
                    getMvpView().onDataChangArrived(filterByDateResponse);
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

}
