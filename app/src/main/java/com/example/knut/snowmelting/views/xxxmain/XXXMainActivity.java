package com.example.knut.snowmelting.views.xxxmain;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.CustomMarker;
import com.example.knut.snowmelting.utils.AppUtils;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.views.login.LoginActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.android.gms.location.LocationServices;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public class XXXMainActivity extends BaseActivity implements XXXMainView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    //
//    @BindView(R.id.tab_layout)
//    TabLayout tabLayout;
//    @BindView(R.id.view_pager)
//    public CustomViewPager viewPager;

    @Inject
    XXXMainPresenter XXXMainPresenter;

    @Inject
    Settings settings;

    static final String EXTRA_ADMIN_ID = "com.example.knut.snowmelting.extras.ADMIN_ID";

    @BindView(R.id.city)
    TextView textCity;

    @BindView(R.id.temperature)
    TextView textTemperature;

    @BindView(R.id.bottom_sheet)
    FrameLayout bottomSheet;

    @BindView(R.id.select_city)
    FloatingActionButton btnSelectCity;

    // Google Map
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap googleMap;
    private HashMap markersHashMap;
    private Iterator<Map.Entry> iter;
    private CameraUpdate cu;
    private int cityId = 0;
    private SupportMapFragment mapFragment;
    private BottomSheetBehavior bottomSheetBehavior;
    private UserResponse data;
    private List<UserResponse> listOfUsers;

//    private CustomMarker customMarker;

//  int height, actionBarHeight;

//    private RxBus _rxBus;

    public static Intent getStartIntent(Context context, UserResponse data) {
        Intent intent = new Intent(context, XXXMainActivity.class);
        intent.putExtra(EXTRA_ADMIN_ID, data);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        data = getIntent().getParcelableExtra(EXTRA_ADMIN_ID);

        initializeToolbar();
        showCityChoiceDialog();
        XXXMainPresenter.attachView(this);

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

//                if (BottomSheetBehavior.STATE_DRAGGING == newState) {
//                    btnSelectCity.animate().scaleX(0).scaleY(0).setDuration(300).start();
//                } else if (BottomSheetBehavior.STATE_COLLAPSED == newState) {
//                    btnSelectCity.animate().scaleX(1).scaleY(1).setDuration(300).start();
//                }


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                btnSelectCity.animate().scaleX(1 - slideOffset).scaleY(1 - slideOffset).setDuration(0).start();
            }
        });

//        setTitle("Enjoy WeatherPi");
//        tabLayout.addOnTabSelectedListener(this);
//        TypedValue tv = new TypedValue();
//        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
//            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
//        }
//        ViewTreeObserver viewTreeObserver = tabLayout.getViewTreeObserver();
//        if (viewTreeObserver.isAlive()) {
//            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                @Override
//                public void onGlobalLayout() {
//                    tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                    height = tabLayout.getHeight();
//                    setupViewPager(viewPager, height + actionBarHeight);
//                    tabLayout.setupWithViewPager(viewPager);
//                }
//            });
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
//            case R.id.menu_settings: {
//                break;
//            }
//            case R.id.menu_weather: {
//                startActivity(new Intent(this, WeatherDataActivity.class));
//                break;
//            }
            case R.id.menu_log_out:{
                settings.logOutUser();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

//    public void enable(boolean enable) {
//        viewPager.setPagingEnabled(enable);
//    }
//
//    private void setupViewPager(CustomViewPager viewPager, int height) {
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPager.setPagingEnabled(true);
//        viewPagerAdapter.addFragment(WeatherFragment.newInstance(height), "Today");
////        viewPagerAdapter.addFragment(WeatherFragment.newInstance(height), "6 days");
//        viewPagerAdapter.addFragment(new PiFragment(), "Real Time");
//        viewPager.setOffscreenPageLimit(2);
//        viewPager.setAdapter(viewPagerAdapter);
//    }

    private void initializeToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(data.getUserName());
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @OnClick(R.id.select_city)
    void selectCity() {
        showCityChoiceDialog();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main_new;
    }

    @Override
    public void handleBus(Object event) {
//        if (event instanceof MainEvent){
//            viewPager.setPagingEnabled(((MainEvent) event).isPagerStatus());
//        }
    }

//    @Override
//    public void onTabSelected(TabLayout.Tab tab) {
////        viewPager.setCurrentItem(tab.getPosition());
//    }
//
//    @Override
//    public void onTabUnselected(TabLayout.Tab tab) {
//
//    }
//
//    @Override
//    public void onTabReselected(TabLayout.Tab tab) {
//
//    }


    public void showCityChoiceDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        CharSequence[] array = {"All", "Seoul", "Chungju"};

        builder.setTitle("Select City")
                .setSingleChoiceItems(array, cityId, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        cityId = which;
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

//                        XXXMainPresenter.getAllUsers();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        Dialog dialog = builder.create();
        dialog.show();
    }

    // Google Maps
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        buildGoogleApiClient();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        initializeUiSettings();
        initializeMapLocationSettings();
        initializeMapTraffic();
        initializeMapType();
        initializeMapViewSettings();
        initData();
    }

    private void initData() {
        removeAllMarkers();

        if (cityId == 0) {
            bottomSheetBehavior.setHideable(true);
            bottomSheetBehavior.setPeekHeight(0);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            setRandomMarkers();
        } else {
            bottomSheetBehavior.setHideable(false);
            bottomSheetBehavior.setPeekHeight(AppUtils.dpToPx(70));

            setRandomMarkers(cityId);
            setWeatherData();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    void setWeatherData() {

        if (cityId == 1) {
            textCity.setText("Seoul");
            textTemperature.setText("15 C. Mostly Cloudy");
        } else {
            textTemperature.setText("21 C. Sunny");
            textCity.setText("Chungju");
        }
    }

    void setRandomMarkers(int cityId) {

        for (int i = 0; i < 10; i++) {
            CustomMarker customMarker = generateMarker(i, cityId);
            float markerColor;

            if (i % 2 == 0) {
                markerColor = BitmapDescriptorFactory.HUE_RED;
            } else {
                markerColor = BitmapDescriptorFactory.HUE_BLUE;
            }

            addMarker(customMarker, markerColor, "Marker " + i);
        }

        zoomAnimateLevelToFitMarkers(120);
    }

    void setRandomMarkers() {

        for (int j = 1; j <= 2; j++) {
            for (int i = 0; i < 10; i++) {
                CustomMarker customMarker = generateMarker(i, j);
                float markerColor;

                if (i % 2 == 0) {
                    markerColor = BitmapDescriptorFactory.HUE_RED;
                } else {
                    markerColor = BitmapDescriptorFactory.HUE_BLUE;
                }

                addMarker(customMarker, markerColor, "Marker " + i);
            }
        }

        zoomAnimateLevelToFitMarkers(120);
    }

    CustomMarker generateMarker(int markerId, int cityId) {

        CustomMarker customMarker;

        if (cityId == 1) {
            customMarker = new CustomMarker(markerId, generateRandomFloat(37.466, 37.629), generateRandomFloat(126.892, 127.096));
        } else {
            customMarker = new CustomMarker(markerId, generateRandomFloat(36.965, 36.993), generateRandomFloat(127.917, 127.946));
        }

        return customMarker;
    }

    double generateRandomFloat(double min, double max) {
        return min + (Math.random() * (max - min));
    }

    public void initializeUiSettings() {
//        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
//            @Override
//            public void onMapLoaded() {
//                zoomAnimateLevelToFitMarkers(120);
//            }
//        });

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
//                startActivity(DashboardActivity.getStartIntent(XXXMainActivity.this, listOfUsers.get(1)));
                return false;
            }
        });
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    public void initializeMapLocationSettings() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                googleMap.setMyLocationEnabled(true);
            }
        } else {
            googleMap.setMyLocationEnabled(true);
        }
    }

    public void initializeMapTraffic() {
        googleMap.setTrafficEnabled(true);
    }

    public void initializeMapType() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public void initializeMapViewSettings() {
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(false);
    }

    public void setUpMarkersHashMap() {
        if (markersHashMap == null) {
            markersHashMap = new HashMap();
        }
    }

    public void addMarkerToHashMap(CustomMarker customMarker, Marker marker) {
        setUpMarkersHashMap();
        markersHashMap.put(customMarker, marker);
    }

    public Marker findMarker(CustomMarker customMarker) {
        iter = markersHashMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry mEntry = (Map.Entry) iter.next();
            CustomMarker key = (CustomMarker) mEntry.getKey();
            if (customMarker.getCustomMarkerId() == (key.getCustomMarkerId())) {
                Marker value = (Marker) mEntry.getValue();
                return value;
            }
        }
        return null;
    }

    public void addMarker(CustomMarker customMarker, float markerColor, String markerTitle) {

        MarkerOptions markerOption = new MarkerOptions()
                .position(new LatLng(customMarker.getCustomMarkerLatitude(), customMarker.getCustomMarkerLongitude()))
                .icon(BitmapDescriptorFactory.defaultMarker(markerColor))
                .title(markerTitle);

        Marker newMark = googleMap.addMarker(markerOption);

        addMarkerToHashMap(customMarker, newMark);
    }

    public void removeMarker(CustomMarker customMarker) {
        if (markersHashMap != null) {
            if (findMarker(customMarker) != null) {
                findMarker(customMarker).remove();
                markersHashMap.remove(customMarker);
            }
        }
    }

    public void removeAllMarkers() {
        if (markersHashMap != null) {
            iter = markersHashMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry mEntry = (Map.Entry) iter.next();
                Marker marker = (Marker) mEntry.getValue();
                marker.remove();
            }

            markersHashMap.clear();
        }
    }

    public void zoomAnimateLevelToFitMarkers(int padding) {
        LatLngBounds.Builder b = new LatLngBounds.Builder();
        iter = markersHashMap.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry mEntry = (Map.Entry) iter.next();
            CustomMarker key = (CustomMarker) mEntry.getKey();
            LatLng ll = new LatLng(key.getCustomMarkerLatitude(), key.getCustomMarkerLongitude());
            b.include(ll);
        }

        LatLngBounds bounds = b.build();

        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        googleMap.animateCamera(cu);
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserDataArrived(List<UserResponse> data) {
        listOfUsers = data;

        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(XXXMainActivity.this);
        } else {
            initData();
        }
    }
}
