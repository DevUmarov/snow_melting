package com.example.knut.snowmelting.database.entities;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by islom on 17. 6. 7.
 */

@Entity(tableName = "user")
public class UserEntity {

    public UserEntity(int id, String email, String name, boolean isAdmin) {
        this.id = id;
        this.email = email;
        this.isAdmin = isAdmin;
        this.name = name;
    }

    @PrimaryKey
    public int id;

    public String email;

    public boolean isAdmin;

    public String name;
}
