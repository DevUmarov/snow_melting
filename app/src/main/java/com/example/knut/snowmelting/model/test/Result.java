package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Result {
    final int code = Integer.valueOf(0); // disable inlining the primitive int 0
    final String requestUrl = null;
    final String message = null;

    @Override
    public String toString() {
        return new StringBuilder("Result{")
                .append("code=").append(code)
                .append(", requestUrl='").append(requestUrl).append('\'')
                .append(", message='").append(message).append('\'')
                .append('}').toString();
    }
}
