package com.example.knut.snowmelting.views.sensorNode.dataHistory;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.api.response.subModels.SensorResponse;
import com.example.knut.snowmelting.api.response.subModels.SensorValuesResponse;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.AppTimeUtils;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.logging.Filter;
import java.util.stream.Collectors;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 6. 12.
 */

public class DataHistoryFragment extends BaseFragment implements DataHistoryView {

    static final String EXTRA_SENSOR_NODE_ID = "com.example.knut.snowmelting.extras.EXTRA_SENSOR_NODE_ID";

    private boolean isLoaded = false;
    private int sensorNodeId;
//    Calendar start;
//    Calendar end;

    LineChart lineChart;

    @BindView(R.id.container_charts)
    LinearLayout chartsContainer;

//    @BindView(R.id.spinner)
//    Spinner mSpinner;

    @BindView(R.id.start_date)
    EditText mStartDate;

    @BindView(R.id.end_date)
    EditText mEndDate;

    @Inject
    DataHistoryPresenter dataHistoryPresenter;

    @Inject
    NetworkUtils mNetworkUtils;

    //ArrayList<SensorResponse> sensorResponses;
    FilterByDateResponse filterByDateResponse;

    String myFormat = "yyyy/MM/dd";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.KOREA);

    @Override
    protected int getLayout() {
        return R.layout.fragment_data_history;
    }

    public static DataHistoryFragment newInstance(int sensorNodeId) {

        DataHistoryFragment fragment = new DataHistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_SENSOR_NODE_ID, sensorNodeId);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onStop() {
        isLoaded = true;
        super.onStop();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);

        dataHistoryPresenter.attachView(this);

        if (getArguments() != null) {
            sensorNodeId = getArguments().getInt(EXTRA_SENSOR_NODE_ID);
        }
    }

    Calendar endDate;
    Calendar startDate;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        if (savedInstanceState == null) {
//            for (int i = 0; i < sensorResponses.size(); i++) {
//
//                String sensorType = sensorResponses.get(i).getSensorType();
//                int index = sensorType.indexOf('.');
//
//                initView(sensorType.substring(index + 1), i);
//            }
//        }

        if (endDate == null) {
            endDate = Calendar.getInstance();
        }

        if (startDate == null) {
            startDate = Calendar.getInstance();
            startDate.add(Calendar.DATE, -1);
        }

        DatePickerDialog.OnDateSetListener endDateDialog = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                endDate.set(Calendar.YEAR, year);
                endDate.set(Calendar.MONTH, monthOfYear);
                endDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mEndDate.setText(sdf.format(endDate.getTime()));
                loadData();
            }
        };

//      mEndDate.setKeyListener(null);
        mEndDate.setText(sdf.format(endDate.getTime()));
        mEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(),
                        endDateDialog,
                        endDate.get(Calendar.YEAR),
                        endDate.get(Calendar.MONTH),
                        endDate.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

        DatePickerDialog.OnDateSetListener startDateDialog = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                startDate.set(Calendar.YEAR, year);
                startDate.set(Calendar.MONTH, monthOfYear);
                startDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                mStartDate.setText(sdf.format(startDate.getTime()));
                loadData();
            }
        };

//      mStartDate.setKeyListener(null);
        mStartDate.setText(sdf.format(startDate.getTime()));
        mStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(getActivity(),
                        startDateDialog,
                        startDate.get(Calendar.YEAR),
                        startDate.get(Calendar.MONTH),
                        startDate.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

        if (filterByDateResponse != null) {
//            String[] items = getResources().getStringArray(R.array.date_filter);

//            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
//            mSpinner.setAdapter(adapter);

//            SpinnerInteractionListener listener = new SpinnerInteractionListener();
//            mSpinner.setOnItemSelectedListener(listener);
//            mSpinner.setOnTouchListener(listener);

            initData();
        } else {
//            start = Calendar.getInstance();
//            start.add(Calendar.DATE, -1);
//            end = Calendar.getInstance();

            loadData();
        }

        super.onViewCreated(view, savedInstanceState);
    }

    private void loadData() {
        if (mNetworkUtils.isNetworkConnected()) {
            showProgress();
            dataHistoryPresenter.getDataChange(sensorNodeId,
                    AppTimeUtils.getDateForDataHistory(startDate.getTimeInMillis()),
                    AppTimeUtils.getDateForDataHistory(endDate.getTimeInMillis()));
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    private void initData() {
        boolean isThereData = false;

        if (filterByDateResponse.getTemperatureList().size() > 0) {
            isThereData = true;
            initGraph(filterByDateResponse.getTemperatureList(), filterByDateResponse.getTemperatureListDate(), getString(R.string.temperature));
        }

        if (filterByDateResponse.getHumidityList().size() > 0) {
            isThereData = true;
            initGraph(filterByDateResponse.getHumidityList(), filterByDateResponse.getHumidityListDate(), getString(R.string.humidity));
        }

        if (filterByDateResponse.getWindSpeedList().size() > 0) {
            isThereData = true;
            initGraph(filterByDateResponse.getWindSpeedList(), filterByDateResponse.getWindSpeedListDate(), getString(R.string.wind_speed));
        }

        if (!isThereData) {
            Toast.makeText(getActivity(), "No data found", Toast.LENGTH_SHORT).show();
        }
    }

//    private class SpinnerInteractionListener implements AdapterView.OnItemSelectedListener, View.OnTouchListener {
//
//        boolean userSelect = false;
//
//        @Override
//        public boolean onTouch(View v, MotionEvent event) {
//            userSelect = true;
//            return false;
//        }
//
//        @Override
//        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//            if (userSelect) {
//                Calendar cal = Calendar.getInstance();
//
//                switch (pos) {
//                    case 0: {
//                        cal.add(Calendar.DATE, -1);
//                        break;
//                    }
//                    case 1: {
//                        cal.add(Calendar.DATE, -3);
//                        break;
//                    }
//                    case 2: {
//                        cal.add(Calendar.DATE, -7);
//                        break;
//                    }
//                    case 3: {
//                        cal.add(Calendar.DATE, -30);
//                        break;
//                    }
//                }
//
//                start = cal;
//                loadData();
//                userSelect = false;
//            }
//        }
//
//        @Override
//        public void onNothingSelected(AdapterView<?> parent) {
//
//        }
//    }

    private LineChart initView(String title) {
        View lineView = getActivity().getLayoutInflater().inflate(R.layout.chart_data_history, null);

        ((TextView) lineView.findViewById(R.id.chart_title)).setText(getString(R.string.data_history, title));

        lineChart = (LineChart) lineView.findViewById(R.id.line_chart);

        chartsContainer.addView(lineView);

        return lineChart;
//        initData(lineChart, sensorResponses.get(0).getSensorValuesList(), title);
    }

    private void initCharts(LineChart lineChart, ArrayList<String> strings) {

        lineChart.setTouchEnabled(false);

        lineChart.getAxisRight().setEnabled(false);
        lineChart.getDescription().setEnabled(false);

        XAxis xAxis = lineChart.getXAxis();
//        xAxis.setLabelCount(7, true);

        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return strings.get((int) value);
            }
        });
    }

    private void initGraph(ArrayList<Float> numbers, ArrayList<String> strings, String title) {

        LineChart lineChart = initView(title);
        initCharts(lineChart, strings);

        ArrayList<Entry> entries = new ArrayList<>();
        int k = 0;

        for (Float cur : numbers) {
            entries.add(new Entry(k, cur));
            k++;
        }

        LineDataSet lineDataSet = new LineDataSet(entries, getString(R.string.data_change, title));

        lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSet.setColor(getResources().getColor(R.color.colorPrimary));
        lineDataSet.setCircleColor(getResources().getColor(R.color.colorPrimaryDark));
        lineDataSet.setLineWidth(2f);
        lineDataSet.setCircleRadius(4f);
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.fade_blue));

        LineData lineData = new LineData(lineDataSet);

        lineChart.setData(lineData);

        if (!isLoaded) {
            lineChart.animateX(1000);
        }

        lineChart.invalidate();
    }

    @Override
    public void onDataChangArrived(FilterByDateResponse filterByDateResponse) {
        this.filterByDateResponse = filterByDateResponse;
        hideProgress();
        if (isResumed() && isVisible()) {
            initData();
        }
    }
}
