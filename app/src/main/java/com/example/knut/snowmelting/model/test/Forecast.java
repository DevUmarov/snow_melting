package com.example.knut.snowmelting.model.test;

import java.util.Date;
import java.util.List;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Forecast {
    final Grid grid;
    final Date timeRelease;
    final List<Integer> lightnings;
    final List<Wind> winds;
    final List<Precipitation> precipitations;
    final List<Sky> skies;
    final List<Float> temperatures;
    final List<Float> humidities;



    public Grid getGrid() {
        return grid;
    }

    public Date getTimeRelease() {
        return timeRelease;
    }

    public List<Integer> getLightnings() {
        return lightnings;
    }

    public List<Wind> getWinds() {
        return winds;
    }

    public List<Precipitation> getPrecipitations() {
        return precipitations;
    }

    public List<Sky> getSkies() {
        return skies;
    }

    public List<Float> getTemperatures() {
        return temperatures;
    }

    public List<Float> getHumidities() {
        return humidities;
    }

    public Forecast(final Grid grid, final Date timeRelease, final List<Integer> lightnings, final List<Wind> winds, final List<Precipitation> precipitations,
                    final List<Sky> skies, final List<Float> temperatures, final List<Float> humidities) {
        this.grid = grid;
        this.timeRelease = timeRelease;
        this.lightnings = lightnings;
        this.winds = winds;
        this.precipitations = precipitations;
        this.skies = skies;
        this.temperatures = temperatures;
        this.humidities = humidities;
    }

    @Override
    public String toString() {
        return new StringBuilder("Forecast{")
                .append("grid=").append(grid)
                .append(", timeRelease=").append(timeRelease)
                .append(", lightnings=").append(lightnings)
                .append(", winds=").append(winds)
                .append(", precipitations=").append(precipitations)
                .append(", skies=").append(skies)
                .append(", temperatures=").append(temperatures)
                .append(", humidities=").append(humidities)
                .append('}').toString();
    }

}
