package com.example.knut.snowmelting.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 6. 16.
 */

public class WeatherDay {

    @SerializedName("result")
    Result result;

    @SerializedName("weather")
    weather weather;

    public class Result {
        @SerializedName("message") String message;
        @SerializedName("code") String code;

        public String getMessage() {return message;}
        public String getCode() {return code;}
    }

    public class weather {

        public List<hourly> hourly = new ArrayList<>();
        public List<hourly> getHourly(){return hourly;}

        public class hourly {
            @SerializedName("sky") Sky sky;
            @SerializedName("precipitation") precipitation precipitation;
            @SerializedName("temperature") temperature temperature;
            @SerializedName("wind") wind wind;
            @SerializedName("grid") grid grid;
            @SerializedName("humidity") String humidity;

            public class grid{

                @SerializedName("city") String city;
                @SerializedName("country") String country;
                @SerializedName("village") String village;

                public String getCity() {return city;}
                public String getCountry() {return country;}
                public String getVillage() {return village;}
            }

            public class Sky{

                @SerializedName("name") String name;
                @SerializedName("code") String code;

                public String getName() {return name;}
                public String getCode() {return code;}
            }

            public class precipitation{
                @SerializedName("sinceOntime") String sinceOntime; //강우
                @SerializedName("type") String type; //0:없음 1:비 2:비/눈 3:눈

                public String getSinceOntime(){return sinceOntime;}
                public String getType() {return type;}
            }

            public class temperature{
                @SerializedName("tc") String tc; //현재 기온
                public String getTc() {return tc;}
            }

            public class wind{
                @SerializedName("wdir") String wdir;
                @SerializedName("wspd") String wspd;

                public String getWdir() {return wdir;}
                public String getWspd() {return wspd;}
            }

            public String getHumidity() {return humidity;}
            public grid getGrid() {return grid;}
            public Sky getSky() {return sky;}
            public precipitation getPrecipitation() {return precipitation;}
            public temperature getTemperature() {return temperature;}
            public wind getWind() {return wind;}

        }
    }

    public Result getResult() {return result;}
    public weather getWeather() {return weather;}
}
