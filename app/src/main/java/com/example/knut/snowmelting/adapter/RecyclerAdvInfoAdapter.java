package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.commons.ExpandableItemStates;
import com.example.knut.snowmelting.databinding.ItemAdvancedInfoBinding;
import com.example.knut.snowmelting.views.dashboard.sensorNodes.SensorNodesView;

/**
 * Created by umarov on 5/10/17.
 */

public class RecyclerAdvInfoAdapter extends RecyclerView.Adapter<RecyclerAdvInfoAdapter.RecyclerViewHolder> {

    Context mContext;
    RecyclerView recyclerView;
    ExpandableItemStates itemStates;
//    List<LocationData> data;
    SensorNodesView sensorNodesView;

    public RecyclerAdvInfoAdapter(Context mContext, SensorNodesView sensorNodesView, RecyclerView recyclerView) {
        this.mContext = mContext;
        this.recyclerView = recyclerView;
        this.sensorNodesView = sensorNodesView;
        itemStates = new ExpandableItemStates(this, true);
//        initData();
    }
//
//    private void initData() {
//        data = new ArrayList<>();
//        SensorNodesData sensorData;
//        for (int i = 1; i <= 4; i++) {LocationData
//            WeatherData obj;
//
//            if (i % 2 == 0) {
//                obj = new WeatherData(i, 88, 12, 234, -17, "Freezing");
//            } else {
//                obj = new WeatherData(i, 68, 21, 134, 10, "Mostly Cloudy");
//            }
//
//            sensorData = new SensorNodesData();
//            sensorData.setSensorId(i);
//            sensorData.setWeatherData(obj);
//            sensorData.setSensorName("Sensor " + i);
//
//            data.add(sensorData);
//        }
//    }

//    public void setData(List<LocationData> data) {
//        this.data = data;
//    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemAdvancedInfoBinding itemBinding = ItemAdvancedInfoBinding.inflate(layoutInflater, parent, false);
        return new RecyclerAdvInfoAdapter.RecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
//        holder.bind(data.get(position), itemStates.isExpanded(position));
//        holder.bind(data.get(0).getSensorDatas().get(position), true);
    }

    @Override
    public int getItemCount() {
//        if (data != null && data.size() > 0) {
//            return data.get(0).getSensorDatas().size();
//        } else {
            return 0;
//        }
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RecyclerViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void onClick(View v) {

        }

        //
//        ItemAdvancedInfoBinding binding;
//
        private RecyclerViewHolder(ItemAdvancedInfoBinding binding) {
            super(binding.getRoot());
//            this.binding = binding;
//            this.binding.setOnClick(this);
        }
//
//        public void bind(SensorNodesData item, boolean isExpanded) {
//            binding.setSensorNode(item);
//            binding.setIsExpanded(isExpanded);
//            binding.executePendingBindings();
//        }
//
//        @Override
//        public void onClick(View v) {
//            itemStates.toggle(getAdapterPosition());
//            sensorNodesView.onSensorNodeClick(getAdapterPosition());
//        }
    }
}
