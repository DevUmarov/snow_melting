package com.example.knut.snowmelting.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.knut.snowmelting.database.dao.LocationDao;
import com.example.knut.snowmelting.database.dao.SensorNodeDao;
import com.example.knut.snowmelting.database.dao.UserDao;
import com.example.knut.snowmelting.database.entities.LocationEntity;
import com.example.knut.snowmelting.database.entities.SensorNodeEntity;
import com.example.knut.snowmelting.database.entities.UserEntity;

/**
 * Created by islom on 17. 6. 2.
 */

@Database(version = 4, entities = {UserEntity.class, LocationEntity.class, SensorNodeEntity.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();

    public abstract LocationDao locationDao();

    public abstract SensorNodeDao sensorNodeDao();
}
