package com.example.knut.snowmelting.views.admin.feedbackRespond;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.arch.persistence.room.util.StringUtil;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.widgets.BottomSheetCoordinatorLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by islom on 17. 5. 25.
 */

public class FeedbackRespondFragment extends BottomSheetDialogFragment {

    EditText editText;
    ActivityInterAction activityInterAction;
    FloatingActionButton fab;
    int feedbackId;

    private static final String FEEDBACK_ID = "com.example.knut.snowmelting.views.admin.feedbackRespond.FEEDBACK_ID";

    public static FeedbackRespondFragment newInstance(int feedbackId) {
        FeedbackRespondFragment feedbackRespondFragment = new FeedbackRespondFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(FEEDBACK_ID, feedbackId);
        feedbackRespondFragment.setArguments(bundle);
        return feedbackRespondFragment;
    }

    public void onAttachToParentFragment(Fragment fragment) {
        try {
            activityInterAction = (ActivityInterAction) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(
                    fragment.toString() + " must implement OnPlayerSelectionSetListener");
        }
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            feedbackId = getArguments().getInt(FEEDBACK_ID);
        }

        onAttachToParentFragment(getParentFragment());
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_bottom_sheet, null);
//        ButterKnife.bind(this, contentView);
        editText = (EditText) contentView.findViewById(R.id.feedback_edit_text);
        fab = (FloatingActionButton) contentView.findViewById(R.id.fab);

        dialog.setContentView(contentView);

        BottomSheetCoordinatorLayout.LayoutParams params = (BottomSheetCoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(false);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        int screenHeight = displaymetrics.heightPixels - getStatusBarHeight();
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        params.height = screenHeight;
        parent.setLayoutParams(params);

        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.feedback_edit_text) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(editText.getText())) {
                    activityInterAction.onResponClicked(feedbackId, editText.getText().toString());
                }
                dismiss();
            }
        });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public interface ActivityInterAction {
        void onResponClicked(int feedbackId, String message);
    }
}
