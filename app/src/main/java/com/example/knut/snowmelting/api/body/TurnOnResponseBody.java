package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by umarov on 8/24/17.
 */

public class TurnOnResponseBody {

    @SerializedName("result")
    @Expose
    private TurnOnResult result;

    public TurnOnResult getResult() {
        return result;
    }

    public void setResult(TurnOnResult result) {
        this.result = result;
    }
}
