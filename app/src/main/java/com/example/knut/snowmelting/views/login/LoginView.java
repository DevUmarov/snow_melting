package com.example.knut.snowmelting.views.login;

import com.example.knut.snowmelting.api.response.SingleUserResponse;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public interface LoginView extends MvpView {

    void onLoginSuccess(UserResponse data);

    void onLoginError();
}
