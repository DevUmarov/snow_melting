package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by islom on 17. 6. 8.
 */

public class OnOffHistoryResponse implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("calculated_time")
    @Expose
    private String calculatedTime;

    @SerializedName("end_time")
    @Expose
    private String endTime;

    @SerializedName("start_time")
    @Expose
    private String startTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCalculatedTime() {
        return calculatedTime;
    }

    public void setCalculatedTime(String calculatedTime) {
        this.calculatedTime = calculatedTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    protected OnOffHistoryResponse(Parcel in) {
        id = in.readInt();
        calculatedTime = in.readString();
        endTime = in.readString();
        startTime = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(calculatedTime);
        dest.writeString(endTime);
        dest.writeString(startTime);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OnOffHistoryResponse> CREATOR = new Parcelable.Creator<OnOffHistoryResponse>() {
        @Override
        public OnOffHistoryResponse createFromParcel(Parcel in) {
            return new OnOffHistoryResponse(in);
        }

        @Override
        public OnOffHistoryResponse[] newArray(int size) {
            return new OnOffHistoryResponse[size];
        }
    };
}