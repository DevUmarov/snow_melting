package com.example.knut.snowmelting.model.custom_deserializre;

import com.example.knut.snowmelting.model.test.Forecast6days;
import com.example.knut.snowmelting.model.test.Grid;
import com.example.knut.snowmelting.model.test.Sky;
import com.example.knut.snowmelting.model.test.SkyAm;
import com.example.knut.snowmelting.model.test.SkyPm;
import com.example.knut.snowmelting.model.test.Temperature;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.parseInt;
import static java.util.regex.Pattern.compile;

/**
 * Created by Nizomjon on 04/03/2017.
 */

public class Forecast6daysDeserializer implements JsonDeserializer<Forecast6days> {
    @Override
    public Forecast6days deserialize(JsonElement json, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        return new Forecast6days(
                context.deserialize(jsonObject.get("grid"), Grid.class),
                context.deserialize(jsonObject.get("timeRelease"), Date.class),
                skiesExtractor.parseList(jsonObject.get("sky").getAsJsonObject()),
                temperaturesExtractor.parseList(jsonObject.get("temperature").getAsJsonObject())
        );
    }

    private static final AbstractExtractor<Sky> skiesExtractor = new AbstractExtractor<Sky>(compile("(?:(pm|am)(Code|Name)(\\d+(\\.\\d*)?|\\.\\d+)day)")) {
        @Override
        protected Sky parse(final int index, final JsonObject jsonObject) {
            String rawAmCode = jsonObject.get("amCode" + index + "day").getAsString();
            String rawAmName = jsonObject.get("amName" + index + "day").getAsString();
            String rawPmCode = jsonObject.get("pmCode" + index + "day").getAsString();
            String rawPmName = jsonObject.get("pmName" + index + "day").getAsString();
            SkyAm skyAm = new SkyAm(rawAmCode, rawAmName);
            SkyPm skPm = new SkyPm(rawPmCode, rawPmName);

//            if (rawCode.isEmpty() && rawName.isEmpty()) {
//                return null;
//            }
            return new Sky(skyAm, skPm);
        }
    };

    private static final AbstractExtractor<Temperature> temperaturesExtractor = new AbstractExtractor<Temperature>(compile("(?:tmax|tmin)(\\d+(\\.\\d*)?|\\.\\d+)day")) {
        @Override
        protected Temperature parse(int index, JsonObject jsonObject) {
            final String tmax = jsonObject.get("tmax" + index + "day").getAsString();
            final String tmin = jsonObject.get("tmin" + index + "day").getAsString();
            return new Temperature(tmax, tmin);
        }
    };

    private abstract static class AbstractExtractor<T> {

        private final Pattern pattern;

        private AbstractExtractor(final Pattern pattern) {
            this.pattern = pattern;
        }

        protected abstract T parse(int index, JsonObject jsonObject);

        private List<T> parseList(final JsonObject jsonObject) {
            final List<T> list = new ArrayList<>();
            for (final Map.Entry<String, JsonElement> e : jsonObject.entrySet()) {
                final String key = e.getKey();
                final Matcher matcher = pattern.matcher(key);
                // Check if the given regular expression matches the key
                if (matcher.matches()) {
                    int index = 0;
                    int groupCount = matcher.groupCount();
                    // If yes, then just extract and parse the index
                    if (groupCount > 3) {
                        index = parseInt(matcher.group(3));
                    } else {
                        index = parseInt(matcher.group(1));
                    }
                    // And check if there is enough room in the result list because the JSON response may contain unordered keys
                    while (index > list.size()) {
                        list.add(null);
                    }
                    // As Java lists are 0-based
                    if (list.get(index - 1) == null) {
                        // Assuming that null marks an object that's probably not parsed yet

                        list.set(index - 1, parse(index, jsonObject));
                    }
                }
            }
            list.removeAll(Collections.singleton(null));
            return list;
        }

    }
}
