package com.example.knut.snowmelting.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.interfaces.OnDialogButtonClickListener;


/**
 * Created by Nizomjon on 11/18/16.
 */

public class DialogsUtil {

    private Context mContext;

    public DialogsUtil(Context context) {
        this.mContext = context;
    }

    public void openAlertDialog(Context context, String message, String positiveBtnText, String negativeBtnText,
                                final OnDialogButtonClickListener listener) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(positiveBtnText, (dialog, which) -> {
            dialog.dismiss();
            listener.onPositiveButtonClicked();

        });

        builder.setNegativeButton(negativeBtnText, (dialog, which) -> {
            dialog.dismiss();
            listener.onNegativeButtonClicked();

        });
        builder.setTitle(context.getResources().getString(R.string.app_name));
        builder.setMessage(message);
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setCancelable(false);
        builder.create().show();
    }


    public void openProgressDialog(Context context, boolean status) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getString(R.string.loading));
        if (status) {
            progressDialog.show();
        } else {
            progressDialog.cancel();
        }
    }

    public static android.app.AlertDialog createInputDialog(Activity activity, String title,
                                                            boolean isCancelable, String positiveButtonText,
                                                            String negativeButtonText, String defaultText,
                                                            DialogInterface.OnClickListener positive,
                                                            DialogInterface.OnClickListener negative){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(activity);
        View view = activity.getLayoutInflater().inflate(R.layout.dialog_input, null);
        EditText inputFileName = (EditText) view.findViewById(R.id.input_file_name);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);

        builder.setPositiveButton(positiveButtonText, positive)
                .setNegativeButton(negativeButtonText, negative)
                .setView(view);

        inputFileName.setText(defaultText);
        inputFileName.setSelection(defaultText.length());

        return builder.create();
    }

}
