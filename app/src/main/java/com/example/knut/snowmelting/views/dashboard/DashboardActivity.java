package com.example.knut.snowmelting.views.dashboard;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.IBinder;
import android.support.v4.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerRealTimeAdapter;
import com.example.knut.snowmelting.api.body.FeedbackBody;
import com.example.knut.snowmelting.api.response.SingleUserResponse;
import com.example.knut.snowmelting.api.response.subModels.LastSeenValues;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.RxBus;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.service.ControllerService;
import com.example.knut.snowmelting.utils.DialogsUtil;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.views.admin.AdminActivity;
import com.example.knut.snowmelting.views.analysis.AnalysisActivity;
import com.example.knut.snowmelting.views.dashboard.onOffHistory.OnOffHistoryFragment;
import com.example.knut.snowmelting.views.dashboard.sensorNodes.SensorNodesFragment;
import com.example.knut.snowmelting.views.dashboard.generalInfo.GeneralInfoFragment;
import com.example.knut.snowmelting.views.login.LoginActivity;
import com.mikepenz.iconics.context.IconicsContextWrapper;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by umarov on 4/19/17.
 */

public class DashboardActivity extends BaseActivity implements DashboardView, GeneralInfoFragment.ActivityInteraction {

    static final String EXTRA_USER_ID = "com.example.knut.snowmelting.extras.EXTRA_USER_ID";
    static final String EXTRA_IS_ADMIN = "com.example.knut.snowmelting.extras.EXTRA_IS_ADMIN";

    private SensorNodesFragment sensorNodesFragment;
    private GeneralInfoFragment generalInfoFragment;
    private OnOffHistoryFragment onOffHistoryFragment;

    private ControllerService mControllerService;
    private int userId;
    private boolean isAdmin;
    private SingleUserResponse mSingleUserResponse;
    private boolean serviceBound;

    @Inject
    Settings settings;

    @BindView(R.id.container_details)
    FrameLayout containerDetails;

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Inject
    DashboardPresenter dashboardPresenter;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public static Intent getStartIntent(Context context, int userId, boolean isAdmin) {
        Intent intent = new Intent(context, DashboardActivity.class);
        intent.putExtra(EXTRA_USER_ID, userId);
        intent.putExtra(EXTRA_IS_ADMIN, isAdmin);
        return intent;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        dashboardPresenter.attachView(this);
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_USER_ID)) {
            userId = getIntent().getIntExtra(EXTRA_USER_ID, -1);
        }

        isAdmin = getIntent().getBooleanExtra(EXTRA_IS_ADMIN, false);

        registerReceiver();
        createFragments();

        showProgress();

        dashboardPresenter.getSingleUserData(userId);
        initBottomNavigation();
    }

    private void registerReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ControllerService.FAIL);
        intentFilter.addAction(ControllerService.SUCCESS);

        LocalBroadcastManager.getInstance(this).registerReceiver(mActionReceiver, intentFilter);

    }

    private BroadcastReceiver mActionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getAction()) {
                case ControllerService.FAIL: {
                    // keep listening
                    if (generalInfoFragment != null) {
                        generalInfoFragment.toggleRadioGroup(false);
                        generalInfoFragment.setStatusText("FAIL");
                        generalInfoFragment.setStatusTextColor(Color.RED);
                    }
                    break;
                }
                case ControllerService.SUCCESS: {

                    if (generalInfoFragment != null ) {
                        generalInfoFragment.setStatusText("SUCCESS");
                        generalInfoFragment.toggleRadioGroup(true);
                        generalInfoFragment.setMacAddress(intent.getStringExtra(ControllerService.SUCCESS_MAC_ADDRESS));
                        generalInfoFragment.setStatusTextColor(Color.parseColor("#5FBA7D"));
                    }

//                    unbindService();
                    break;
                }
            }
        }
    };

    public void bindService() {
        if (!serviceBound) {
            bindService(new Intent(this, ControllerService.class), mServerConn, Context.BIND_AUTO_CREATE);
            serviceBound = true;
        }
    }

    public void unbindService() {
        try {
            if (serviceBound) {
                unbindService(mServerConn);
                serviceBound = false;
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    protected ServiceConnection mServerConn = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ControllerService.LocalBinder binder = (ControllerService.LocalBinder) service;
            mControllerService = binder.getService();
            mControllerService.startListening();
            serviceBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };


    private void createFragments() {
        generalInfoFragment = GeneralInfoFragment.newInstance(isAdmin);
        sensorNodesFragment = SensorNodesFragment.newInstance();
        onOffHistoryFragment = OnOffHistoryFragment.newInstance();
    }

    private void initBottomNavigation() {

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_general: {

//                                if (generalInfoFragment == null) {
//                                    if (mSingleUserResponse != null) {
//                                        generalInfoFragment = GeneralInfoFragment.newInstance(
//                                                isAdmin,
//                                                mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getSensorNodeId(),
//                                                mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getLastSeenValues());
//
//                                    } else {
//                                        generalInfoFragment = GeneralInfoFragment.newInstance(isAdmin);
//                                    }
//                                }

                                replaceFragment(generalInfoFragment);
                                break;
                            }
                            case R.id.action_advanced: {
//                                if (sensorNodesFragment == null) {
//                                    if (mSingleUserResponse != null) {
//                                        sensorNodesFragment = SensorNodesFragment.newInstance(
//                                                new ArrayList<>(mSingleUserResponse.getLocationList().get(0).getSensorNodesList()));
//                                    } else {
//                                        sensorNodesFragment = SensorNodesFragment.newInstance();
//                                    }
//                                }

                                replaceFragment(sensorNodesFragment);
                                break;
                            }
                            case R.id.action_on_off_history: {
//                                if (onOffHistoryFragment == null) {
//                                    if (mSingleUserResponse != null) {
//                                        onOffHistoryFragment = OnOffHistoryFragment.newInstance(
//                                                mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getOnOffHistoryResponsesList());
//                                    } else {
//                                        onOffHistoryFragment = OnOffHistoryFragment.newInstance();
//                                    }
//                                }

                                replaceFragment(onOffHistoryFragment);
                                break;
                            }
                        }
                        return true;
                    }
                });

        bottomNavigationView.setSelectedItemId(R.id.action_general);
    }

    private void initUI() {
        if (generalInfoFragment != null) {
            generalInfoFragment.setData(isAdmin,
                    mSingleUserResponse.getLocationList().get(0).getCameraUrl(),
                    mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getSensorNodeId(),
                    mSingleUserResponse.getLocationList().get(0).getAddress(),
                    mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getLastSeenValues());
        }

        if (sensorNodesFragment != null) {
            sensorNodesFragment.setData(new ArrayList<>(mSingleUserResponse.getLocationList().get(0).getSensorNodesList()));
        }

        if (onOffHistoryFragment != null) {
            onOffHistoryFragment.setData(new ArrayList<>(mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getOnOffHistoryResponsesList()));
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_details, fragment);
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_dashboard, menu);

        if (isAdmin) {
            MenuItem item = menu.findItem(R.id.write_to_admin);
            item.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.write_to_admin: {
                AlertDialog alertDialog = DialogsUtil.createInputDialog(this, getString(R.string.write_to_admin), true, getString(R.string.send), getString(R.string.cancel), "", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText inputFileName = (EditText) ((AlertDialog) dialog).findViewById(R.id.input_file_name);
                        FeedbackBody feedbackBody = new FeedbackBody();

                        feedbackBody.setUser_id(userId);
                        feedbackBody.setLocation_id(mSingleUserResponse.getLocationList().get(0).getId());
                        feedbackBody.setSensor_node_id(mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getSensorNodeId());
                        feedbackBody.setMessage(inputFileName.getText().toString());
                        feedbackBody.setType("user");

                        dashboardPresenter.sendFeedback(feedbackBody);
                    }
                }, null);

                alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                alertDialog.show();
                break;
            }
            case R.id.log_out_action: {
                settings.logOutUser();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.analysis_action: {
                startActivity(AnalysisActivity.getStartIntent(this, mSingleUserResponse.getLocationList().get(0).getSensorNodesList().get(0).getAnalysisResponse()));
                break;
            }

        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void handleBus(Object event) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dashboardPresenter.onDestroy();

        if (serviceBound) {
            unbindService();
        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mActionReceiver);
    }

    @Override
    public void onSingleUserDataArrived(SingleUserResponse singleUserResponse) {
        mSingleUserResponse = singleUserResponse;
        initUI();
        hideProgress();

        if (!isAdmin && !serviceBound) {
            bindService();
        }
    }

    @Override
    public void onFeedbackSend() {
        Toast.makeText(this, R.string.feedback_sent_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFeedbackSendError() {
        Toast.makeText(this, R.string.feedback_sent_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTurnedOff() {
        dashboardPresenter.getSingleUserData(userId);
    }
}
