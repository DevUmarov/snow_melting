package com.example.knut.snowmelting.api.body;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class RegisterBody {
    private String username;
    private String email;
    private String password;

    public RegisterBody(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
