package com.example.knut.snowmelting.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.SensorValuesResponse;

/**
 * Created by islom on 17. 6. 12.
 */

public class SensorDataView extends View {

    private Paint mTextPaint, mArcPaint;
    float titleTextSize;
    float centerTextSize;
    float startEndTextSize;
    String startValue, endValue, title, mCenterValue;
    float mActualValue;
    float screenWidth;
    float padding;
    int titleTextHeight, centerTextHeight, startEndTextHeight, range;
    float stroke;

    public SensorDataView(Context context) {
        super(context);
        init(context);
    }

    public SensorDataView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.SensorDataView);

        try {
            mActualValue = ta.getFloat(R.styleable.SensorDataView_curValue, 0);
            startValue = ta.getString(R.styleable.SensorDataView_startValue);
            endValue = ta.getString(R.styleable.SensorDataView_endValue);
            title = ta.getString(R.styleable.SensorDataView_title);
        } finally {
            ta.recycle();
        }
        init(context);
    }

    private void init(Context context) {

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mArcPaint = new Paint(Paint.FILTER_BITMAP_FLAG | Paint.DITHER_FLAG | Paint.ANTI_ALIAS_FLAG);

        titleTextSize = context.getResources().getDimension(R.dimen.widget_text_large_title);
        centerTextSize = context.getResources().getDimension(R.dimen.widget_text_large_center);
        startEndTextSize = context.getResources().getDimension(R.dimen.widget_text_end_start);
        padding = context.getResources().getDimension(R.dimen.widget_sensor_view_padding);
        stroke = context.getResources().getDimension(R.dimen.widget_sensor_stroke);

        screenWidth = context.getResources().getDisplayMetrics().widthPixels;

        mTextPaint.setTextSize(titleTextSize);
        Paint.FontMetrics fm = mTextPaint.getFontMetrics();
        titleTextHeight = Math.round(fm.descent - fm.ascent);

        mTextPaint.setColor(Color.BLACK);
        mTextPaint.setTextAlign(Paint.Align.CENTER);

        mArcPaint.setStrokeWidth(stroke);
        mArcPaint.setStyle(Paint.Style.STROKE);
    }

    public void setData(float centerValue, float actualValue, String type, int startValue, int endValue) {
        Rect resultBounds = new Rect();
        mActualValue = actualValue;
        mCenterValue = String.format("%.01f", centerValue);
        title = type;
        range = (endValue - startValue);
        this.startValue = Integer.toString(startValue);
        this.endValue = Integer.toString(endValue);

        mTextPaint.setTextSize(centerTextSize);
        mTextPaint.getTextBounds(String.valueOf(mCenterValue), 0, String.valueOf(mCenterValue).length(), resultBounds);
        centerTextHeight = resultBounds.height();

        mTextPaint.setTextSize(startEndTextSize);
        mTextPaint.getTextBounds(this.startValue, 0, this.startValue.length(), resultBounds);
        startEndTextHeight = resultBounds.height();

        invalidate();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = (int) screenWidth / 2;
        int desiredHeight = Math.round(desiredWidth / 2 + 2 * padding + titleTextHeight + startEndTextSize);

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = desiredWidth;
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = desiredHeight;
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float arcWidth = getWidth() - getPaddingLeft() - getPaddingRight();
        float arcHeight = arcWidth;

        //Draw Title
        mTextPaint.setTextSize((int) (titleTextSize));
        canvas.drawText(title, arcWidth / 2 + getPaddingLeft(), titleTextHeight, mTextPaint);

        //Draw Arc
        final RectF recArc = new RectF();

        recArc.set(getPaddingLeft() + (stroke / 2),
                (stroke / 2) + titleTextHeight + padding,
                getPaddingLeft() + arcWidth - (stroke / 2),
                arcHeight - (stroke / 2) + titleTextHeight + padding);

        mArcPaint.setColor(getResources().getColor(R.color.divider));
        canvas.drawArc(recArc, 180, 180, false, mArcPaint);//First draw full arc as background.

        mArcPaint.setColor(getResources().getColor(R.color.colorPrimary));
        canvas.drawArc(recArc, 180, (mActualValue * 180) / range, false, mArcPaint); //Then draw arc progress with actual value.

        //Draw center text value.
        mTextPaint.setTextSize((int) (centerTextSize));
        canvas.drawText(String.valueOf(mCenterValue), arcWidth / 2 + getPaddingLeft(), titleTextHeight + arcHeight / 2, mTextPaint);

        //Draw Start end End values
        mTextPaint.setTextSize((int) (startEndTextSize));

        canvas.drawText(startValue, stroke / 2 + getPaddingLeft(), titleTextHeight + padding + arcHeight / 2 + startEndTextHeight + 10,
                mTextPaint);// Draw Start Value

        canvas.drawText(endValue, arcWidth - (stroke / 2) + getPaddingLeft(), titleTextHeight + padding + arcHeight / 2 + startEndTextHeight + 10,
                mTextPaint); // Draw End Value

    }

}