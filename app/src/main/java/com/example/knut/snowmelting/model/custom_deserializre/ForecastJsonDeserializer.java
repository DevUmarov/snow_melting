package com.example.knut.snowmelting.model.custom_deserializre;

import com.example.knut.snowmelting.model.test.Forecast;
import com.example.knut.snowmelting.model.test.Grid;
import com.example.knut.snowmelting.model.test.Precipitation;
import com.example.knut.snowmelting.model.test.Sky;
import com.example.knut.snowmelting.model.test.Wind;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.util.regex.Pattern.compile;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class ForecastJsonDeserializer
        implements JsonDeserializer<Forecast> {

    private static JsonDeserializer<Forecast> forecastJsonDeserializer = null;
    public static boolean isScince;

    private ForecastJsonDeserializer() {
    }

    public static JsonDeserializer<Forecast> getForecastJsonDeserializer() {
        if (forecastJsonDeserializer == null) {
            forecastJsonDeserializer = new ForecastJsonDeserializer();
        }
        return forecastJsonDeserializer;
    }

    @Override
    public Forecast deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext context)
            throws JsonParseException {
        final JsonObject jsonObject = jsonElement.getAsJsonObject();
        JsonObject prepJson = jsonObject.get("precipitation").getAsJsonObject();
        isScince = prepJson.has("sinceOntime1hour");
        return new Forecast(
                context.deserialize(jsonObject.get("grid"), Grid.class),
                context.deserialize(jsonObject.get("timeRelease"), Date.class),
                lightningsExtractor.parseList(jsonObject),
                windsExtractor.parseList(jsonObject.get("wind").getAsJsonObject()),
                isScince ? precipitationsExtractor.parseList(jsonObject.get("precipitation").getAsJsonObject()) : precipitationsExtractorA.parseList(jsonObject.get("precipitation").getAsJsonObject()),
                skiesExtractor.parseList(jsonObject.get("sky").getAsJsonObject()),
                temperaturesExtractor.parseList(jsonObject.get("temperature").getAsJsonObject()),
                humiditiesExtractor.parseList(jsonObject.get("humidity").getAsJsonObject())
        );
    }

    private static final AbstractExtractor<Integer> lightningsExtractor = new AbstractExtractor<Integer>(compile("lightning(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Integer parse(final int index, final JsonObject jsonObject) {
            final String rawLightning = jsonObject.get("lightning" + index + "hour").getAsString();
            if (rawLightning.isEmpty()) {
                return null;
            }
            return parseInt(rawLightning);
        }
    };

    private static final AbstractExtractor<Wind> windsExtractor = new AbstractExtractor<Wind>(compile("(?:wdir|wspd)(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Wind parse(final int index, final JsonObject jsonObject) {
            String rawSpeed = jsonObject.get("wspd" + index + "hour").getAsString();
            String rawDirection = jsonObject.get("wdir" + index + "hour").getAsString();
            if (rawSpeed.isEmpty() && rawDirection.isEmpty()) {
                return new Wind(parseFloat("0"), parseFloat("0"));
            }
            return new Wind(parseFloat(rawSpeed), parseFloat(rawDirection));
        }
    };

    private static final AbstractExtractor<Precipitation> precipitationsExtractor = new AbstractExtractor<Precipitation>(compile("(?:sinceOntime|type)(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Precipitation parse(final int index, final JsonObject jsonObject) {
            String rawSinceOntime = null;
            String rawType = null;
            rawSinceOntime = jsonObject.get("sinceOntime" + index + "hour").getAsString();
            rawType = jsonObject.get("type" + index + "hour").getAsString();
            if (rawSinceOntime.isEmpty() && rawType.isEmpty()) {
                return new Precipitation(parseFloat("0"), parseInt("0"));
            }
            return new Precipitation(parseFloat(rawSinceOntime), parseInt(rawType));
        }
    };
    private static final AbstractExtractor<Precipitation> precipitationsExtractorA = new AbstractExtractor<Precipitation>(compile("(?:prob|type)(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Precipitation parse(final int index, final JsonObject jsonObject) {
            String rawSinceOntime = null;
            String rawType = null;
            rawSinceOntime = jsonObject.get("prob" + index + "hour").getAsString();
            rawType = jsonObject.get("type" + index + "hour").getAsString();
            if (rawSinceOntime.isEmpty() && rawType.isEmpty()) {
                return new Precipitation(parseFloat("0"), parseInt("0"));
            }
            return new Precipitation(parseFloat(rawSinceOntime), parseInt(rawType));
        }
    };
    private static final AbstractExtractor<Sky> skiesExtractor = new AbstractExtractor<Sky>(compile("(?:code|name)(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Sky parse(final int index, final JsonObject jsonObject) {
            String rawCode = jsonObject.get("code" + index + "hour").getAsString();
            String rawName = jsonObject.get("name" + index + "hour").getAsString();
//            if (rawCode.isEmpty() && rawName.isEmpty()) {
//                return null;
//            }
            return new Sky(rawCode, rawName);
        }
    };

    private static final AbstractExtractor<Float> temperaturesExtractor = new AbstractExtractor<Float>(compile("temp(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Float parse(final int index, final JsonObject jsonObject) {
            final String rawTemperature = jsonObject.get("temp" + index + "hour").getAsString();
            if (rawTemperature.isEmpty()) {
                return null;
            }
            return parseFloat(rawTemperature);
        }
    };

    private static final AbstractExtractor<Float> humiditiesExtractor = new AbstractExtractor<Float>(compile("rh(\\d+(\\.\\d*)?|\\.\\d+)hour")) {
        @Override
        protected Float parse(final int index, final JsonObject jsonObject) {
            final String rawHumidity = jsonObject.get("rh" + index + "hour").getAsString();
            if (rawHumidity.isEmpty()) {
                return null;
            }
            return parseFloat(rawHumidity);
        }
    };

    private abstract static class AbstractExtractor<T> {

        private final Pattern pattern;

        private AbstractExtractor(final Pattern pattern) {
            this.pattern = pattern;
        }

        protected abstract T parse(int index, JsonObject jsonObject);

        private List<T> parseList(final JsonObject jsonObject) {
            final List<T> list = new ArrayList<>();
            for (final Map.Entry<String, JsonElement> e : jsonObject.entrySet()) {
                final String key = e.getKey();
                final Matcher matcher = pattern.matcher(key);
                // Check if the given regular expression matches the key
                if (matcher.matches()) {
                    // If yes, then just extract and parse the index
                    final int index = parseInt(matcher.group(1));
                    // And check if there is enough room in the result list because the JSON response may contain unordered keys
                    while (index > list.size()) {
                        list.add(null);
                    }
                    // As Java lists are 0-based
                    if (list.get(index - 1) == null) {
                        // Assuming that null marks an object that's probably not parsed yet
                        list.set(index - 1, parse(index, jsonObject));
                    }
                }
            }
            list.removeAll(Collections.singleton(null));
            return list;
        }

    }
}
