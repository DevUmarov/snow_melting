package com.example.knut.snowmelting.base;

import android.app.Application;
import android.content.Context;
import android.databinding.DataBindingUtil;

import com.example.knut.snowmelting.BuildConfig;
import com.example.knut.snowmelting.binding.CustomDataBindingComponent;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class WeatherApplication extends Application {

//    private AppComponent mAppComponent;
    private static WeatherApplication instance;
    private Disposable internetStatusObserver;

    @Override
    public void onCreate() {
        super.onCreate();
        final Context context = this;

//        mAppComponent = DaggerAppComponent.builder().utilsModule(new UtilsModule(this)).build();
        instance = this;
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }


        DataBindingUtil.setDefaultComponent(new CustomDataBindingComponent());

          }

    public static WeatherApplication get() {
        return instance;
    }

}
