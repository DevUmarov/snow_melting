package com.example.knut.snowmelting.views.weatherData.history;

import com.example.knut.snowmelting.model.test.Response;
import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by umarov on 5/1/17.
 */

public interface WeatherHistoryView extends MvpView {

    void fillGraph(Response response);

    void fillForecast10days(Response response);

}
