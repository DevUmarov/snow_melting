package com.example.knut.snowmelting.views.xxxmain;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;


import javax.inject.Inject;
import javax.inject.Named;


/**
 * Created by Nizomjon on 10/02/2017.
 */
public class XXXMainPresenter extends BasePresenter<XXXMainView> {

//    @Inject
//    @Named(NetModule.SKPLANET)
//    SkService skApi;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

//    @Inject
//    AppUtils mUtils;
//    @Inject
//    @Named(NetModule.PRODUCTION)
//    Gson gson;

    private Context context;

//    private MqttAndroidClient mqttAndroidClient;
//    private MqttConnectOptions mqttConnectOptions;
//    private String clientId = "UdblabClientId";
//    private static final String main_broker_url = "ws://159.203.160.131:8080/mqtt";
//    private static final String hive_broker_url = "tcp://broker.hivemq.com:1883";
//    private static final String amazon_broker_url = "tcp://m12.cloudmqtt.com:13226";
//    private static final String sub_current_data_topic = "udblab/sensor/+/current_data/";
//    private static final String pub_senor_heating_topic = "udblab/sensor/1112/heating/";

    public XXXMainPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);

        this.context = context;
    }


//    public void getAllUsers(){
//
//        unSubscribeAll();
//        subscribe(restService.getAllUsers(), new SubscriberCallback<>(new ApiCallback<List<UserResponse>>() {
//            @Override
//            public void onSuccess(List<UserResponse> data) {
//                getMvpView().onUserDataArrived(data);
//            }
//
//            @Override
//            public void onFailure(RetrofitException exception) {
//                try {
//                    LoginErrorResponse errorResponse = exception.getErrorBodyAs(LoginErrorResponse.class);
//                    getMvpView().onError(errorResponse.getError());
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onNetworkError() {
//                getMvpView().onError("Network Error");
//            }
//        }));
//    }

//    public void initMqtt(String broker_url, boolean isMain, boolean isHive) {
//
//        clientId = clientId + System.currentTimeMillis();
//
//        mqttAndroidClient = new MqttAndroidClient(context, broker_url, clientId);
//        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI) {
//
//                if (reconnect) {
//                    getMvpView().onReConnected();
//                    subscribeToTopic();
//                } else {
//                    getMvpView().onConnected("");
//                }
//            }
//
//
//            @Override
//            public void connectionLost(Throwable cause) {
//                Toast.makeText(context, ("The Connection was lost."), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                String json = new String(message.getPayload());
//                WeatherPi weatherPi = gson.fromJson(json, WeatherPi.class);
//                getMvpView().onMessageArrived(weatherPi);
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken token) {
//
//            }
//        });
//
//        mqttConnectOptions = new MqttConnectOptions();
//        mqttConnectOptions.setCleanSession(!isMain);
//        mqttConnectOptions.setUserName(isMain ? "udblab" : "hlqbtvzv");
//        mqttConnectOptions.setPassword(isMain ? "12345".toCharArray() : "rGA8NiRaD2KX".toCharArray());
//        mqttConnectOptions.setAutomaticReconnect(true);
//
//        try {
//            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
//                    disconnectedBufferOptions.setBufferEnabled(true);
//                    disconnectedBufferOptions.setBufferSize(100);
//                    disconnectedBufferOptions.setPersistBuffer(false);
//                    disconnectedBufferOptions.setDeleteOldestMessages(false);
//                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
//                    subscribeToTopic();
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    if (isMain && isHive) {
//                        initMqtt(amazon_broker_url, false, false);
//
//                    } else if (!isMain && !isHive) {
//                        initMqtt(hive_broker_url, true, false);
//                    } else {
//                        initMqtt(main_broker_url, true, true);
//                    }
//                    getMvpView().onFailed();
//                }
//            });
//
//
//        } catch (MqttException ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    private void subscribeToTopic() {
//        try {
//            mqttAndroidClient.subscribe(sub_current_data_topic, 0, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    getMvpView().onSubscribed();
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//
//                }
//            });
//        } catch (MqttException ex) {
//            System.err.println("Exception whilst subscribing");
//            ex.printStackTrace();
//        }
//    }
//
//    public void publishMessage(String publishMessage) {
//
//        try {
//            MqttMessage message = new MqttMessage();
//
//            message.setPayload(publishMessage.getBytes());
//            mqttAndroidClient.publish(pub_senor_heating_topic, message);
//            if (!mqttAndroidClient.isConnected()) {
//                Toast.makeText(context, "mqttAndroidClient.getBufferedMessageCount() + \" messages in buffer", Toast.LENGTH_SHORT).show();
//            }
//        } catch (MqttException e) {
//            System.err.println("Error Publishing: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    public void onPauseMode() {
//        try {
//            if (mqttAndroidClient != null && mqttAndroidClient.isConnected()) {
//                mqttAndroidClient.unsubscribe(sub_current_data_topic);
//                mqttAndroidClient.setCallback(null);
//                mqttAndroidClient.disconnect();
//            }
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }
//    }

//    void onRestartMode() {
//        if (mqttAndroidClient!=null && !mqttAndroidClient.isConnected()) {
//            initMqtt();
//        }
//    }

//    public void onDestroyMode() {
//        try {
//            if (mqttAndroidClient!=null && mqttAndroidClient.isConnected()){
//                mqttAndroidClient.unsubscribe(sub_current_data_topic);
//                mqttAndroidClient.disconnect();
//            }
//        } catch (MqttException e) {
//            e.printStackTrace();
//        }
//    }

//    public void syncWeatherData(double latitude, double longitude) {
//        unSubscribeAll();
//        subscribe(skApi.getCurrentWeatherCondition(latitude, longitude), new SubscriberCallback<>(new ApiCallback<SkWeather>() {
//            @Override
//            public void onSuccess(SkWeather model) {
//                model.getResult();
//            }
//
//
//            @Override
//            public void onFailure(RetrofitException exception) {
//                exception.getResponse();
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onNetworkError() {
//
//            }
//        }));
//    }
}
