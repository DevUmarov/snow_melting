package com.example.knut.snowmelting.views.analysis;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.AnalysisResponse;
import com.example.knut.snowmelting.base.BaseActivity;
import com.mikepenz.iconics.context.IconicsContextWrapper;

/**
 * Created by umarov on 8/3/17.
 */

public class  AnalysisActivity extends BaseActivity {

    private static final String ANALYSIS_EXTRA = "com.example.knut.snowmelting.views.analysis.ANALYSIS_EXTRA";

    public static Intent getStartIntent(Context context, AnalysisResponse analysisResponse){
        Intent intent = new Intent(context, AnalysisActivity.class);
        intent.putExtra(ANALYSIS_EXTRA, analysisResponse);
        return intent;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(IconicsContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public int getLayout() {
        return R.layout.activity_analysis;
    }

    @Override
    public void handleBus(Object event) {

    }
}
