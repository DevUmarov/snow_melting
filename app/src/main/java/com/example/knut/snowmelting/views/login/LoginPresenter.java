package com.example.knut.snowmelting.views.login;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.body.LoginBody;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.api.response.LoginResponse;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class LoginPresenter extends BasePresenter<LoginView> {

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    @Inject
    Settings settings;

    public LoginPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);
    }

    public void login(String email, String password) {

        registerAsync(restService.login(new LoginBody(email, password)), new ApiDisposableCallback<LoginResponse>() {
            @Override
            public void onAPIError(@NonNull Exception e) {
                getMvpView().onError(e.getMessage());
            }

            @Override
            public void onNext(@NonNull LoginResponse loginResponse) {
                getMvpView().onLoginSuccess(loginResponse.getUserResponse());
                settings.saveUser(loginResponse.getUserResponse());
            }

            @Override
            public void onComplete() {

            }
        });

    }
}
