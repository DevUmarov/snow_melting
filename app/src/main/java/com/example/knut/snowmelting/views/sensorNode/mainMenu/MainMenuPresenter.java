package com.example.knut.snowmelting.views.sensorNode.mainMenu;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by islom on 17. 6. 12.
 */

public class MainMenuPresenter extends BasePresenter<MainMenuView> {

    Context mContext;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    public MainMenuPresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }

}
