package com.example.knut.snowmelting.views.dashboard;

import com.example.knut.snowmelting.api.response.SingleUserResponse;
import com.example.knut.snowmelting.model.WeatherPi;
import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by umarov on 4/19/17.
 */

public interface DashboardView extends MvpView {

    void onSingleUserDataArrived(SingleUserResponse mSingleUserResponse);
    void onFeedbackSend();
    void onFeedbackSendError();
}
