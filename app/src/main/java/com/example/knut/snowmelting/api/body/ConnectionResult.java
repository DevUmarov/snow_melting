package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by umarov on 9/18/17.
 */

public class ConnectionResult {
    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("mac_address")
    @Expose
    ArrayList<String> macAddress;

    public String getType() {
        return type;
    }

    public ArrayList<String> getMacAddress() {
        return macAddress;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMacAddress(ArrayList<String> macAddress) {
        this.macAddress = macAddress;
    }
}
