package com.example.knut.snowmelting.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by islom on 17. 5. 28.
 */

public class StartUpResponse {

    @SerializedName("chungju_loc_count")
    @Expose
    int chungjuLocCount;

    @SerializedName("seoul_loc_count")
    @Expose
    int seoulLocCount;

    @SerializedName("total_locations_count")
    @Expose
    int totalLocCount;

    @SerializedName("total_users_count")
    @Expose
    int totalUsersCount;

    public int getChungjuLocCount() {
        return chungjuLocCount;
    }

    public void setChungjuLocCount(int chungjuLocCount) {
        this.chungjuLocCount = chungjuLocCount;
    }

    public int getSeoulLocCount() {
        return seoulLocCount;
    }

    public void setSeoulLocCount(int seoulLocCount) {
        this.seoulLocCount = seoulLocCount;
    }

    public int getTotalLocCount() {
        return totalLocCount;
    }

    public void setTotalLocCount(int totalLocCount) {
        this.totalLocCount = totalLocCount;
    }

    public int getTotalUsersCount() {
        return totalUsersCount;
    }

    public void setTotalUsersCount(int totalUsersCount) {
        this.totalUsersCount = totalUsersCount;
    }
}
