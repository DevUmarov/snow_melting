package com.example.knut.snowmelting.views.dashboard.onOffHistory;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by umarov on 5/12/17.
 */

public class OnOffHistoryPresenter extends BasePresenter<OnOffHistoryView> {

    Context mContext;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    public OnOffHistoryPresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }
}
