package com.example.knut.snowmelting.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.example.knut.snowmelting.utils.AppUtils;

/**
 * Created by islom on 17. 5. 23.
 */

public class CircleView extends View {
    private static final int DEFAULT_CIRCLE_COLOR = Color.TRANSPARENT;

    private int circleColor = DEFAULT_CIRCLE_COLOR;
    private Paint paint;
    private final Rect textBounds = new Rect();
    private String centralText = "";

    public CircleView(Context context) {
        super(context);
        init(context, null);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        paint = new Paint();
        paint.setAntiAlias(true);
    }

    public void setData(int circleColor, String text) {
        this.circleColor = circleColor;
        centralText = text;
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = getWidth();
        int h = getHeight();

        int pl = getPaddingLeft();
        int pr = getPaddingRight();
        int pt = getPaddingTop();
        int pb = getPaddingBottom();

        int usableWidth = w - (pl + pr);
        int usableHeight = h - (pt + pb);

        int radius = Math.min(usableWidth, usableHeight) / 2;
        int cx = pl + (usableWidth / 2);
        int cy = pt + (usableHeight / 2);

        paint.setColor(circleColor);
        canvas.drawCircle(cx, cy, radius, paint);

        paint.setColor(Color.WHITE);

        if (!centralText.isEmpty()) {
//            if (centralText.length() <= 4) {
//                paint.setTextSize(determineMaxTextSize(centralText, (2 * radius) / 3));
//            } else {
//                paint.setTextSize(determineMaxTextSize(centralText, radius));
//            }

            paint.setTextSize(AppUtils.dpToPx(55));
            drawTextCentred(canvas, paint, centralText, cx, cy);
        }
    }

    public void drawTextCentred(Canvas canvas, Paint paint, String text, float cx, float cy) {
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, cx - textBounds.exactCenterX(), cy - textBounds.exactCenterY(), paint);
    }

    private int determineMaxTextSize(String str, float maxWidth) {
        int size = 0;
        Paint paint = new Paint();

        do {
            paint.setTextSize(++size);
        } while (paint.measureText(str) < maxWidth);

        return (int) (size);
    }
}