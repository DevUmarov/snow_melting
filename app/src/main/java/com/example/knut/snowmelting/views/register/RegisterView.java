package com.example.knut.snowmelting.views.register;

import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public interface RegisterView extends MvpView{

    void onRegisterSuccess();

    void closeApplication();
}
