package com.example.knut.snowmelting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class CommonSk {
    @SerializedName("alertYn")
    @Expose
    private String alertYn;
    @SerializedName("stormYn")
    @Expose
    private String stormYn;

    public String getAlertYn() {
        return alertYn;
    }

    public void setAlertYn(String alertYn) {
        this.alertYn = alertYn;
    }

    public String getStormYn() {
        return stormYn;
    }

    public void setStormYn(String stormYn) {
        this.stormYn = stormYn;
    }
}
