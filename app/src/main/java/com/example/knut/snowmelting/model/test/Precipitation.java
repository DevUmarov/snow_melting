package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Precipitation {
    float sinceOntime;
    int type;


    public float getSinceOntime() {
        return (int) sinceOntime;
    }

    public int getType() {
        return type;
    }

    public Precipitation(final float sinceOntime, final int type) {
        this.sinceOntime = sinceOntime;
        this.type = type;
    }

    @Override
    public String toString() {
        return new StringBuilder("Precipitation{")
                .append("sinceOntime='").append(sinceOntime).append('\'')
                .append(", type=").append(type)
                .append('}').toString();
    }


}
