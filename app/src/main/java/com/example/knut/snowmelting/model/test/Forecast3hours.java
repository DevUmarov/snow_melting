package com.example.knut.snowmelting.model.test;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Nizomjon on 26/02/2017.
 */

public class Forecast3hours {

    final Grid grid;
    final Date timeRelease;
    @SerializedName("fcst3hour")
    final Forecast fcst3hours;

    public Forecast3hours(Grid grid, Date timeRelease, Forecast fcst3hours) {
        this.grid = grid;
        this.timeRelease = timeRelease;
        this.fcst3hours = fcst3hours;
    }

    public Grid getGrid() {
        return grid;
    }

    public Date getTimeRelease() {
        return timeRelease;
    }

    public Forecast getFcst3hours() {
        return fcst3hours;
    }

    @Override
    public String toString() {
        return new StringBuilder("Forecast{")
                .append("grid=").append(grid)
                .append(", timeRelease=").append(timeRelease)

                .append('}').toString();
    }

}
