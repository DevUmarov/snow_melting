package com.example.knut.snowmelting.views.admin.mainMenu;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.api.response.StartUpResponse;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by islom on 17. 5. 24.
 */

public class MainAdminPagePresenter extends BasePresenter<MainAdminPageView> {

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    @Inject
    NetworkUtils networkUtils;

    @Inject
    Settings settings;

    private Context context;
    private boolean isDataLoadedAtLeastOne;
    private Disposable internetStatusObserver;
    //    private StartUpResponse startUpResponse;
    private List<LocationResponse> locationResponse;

    public MainAdminPagePresenter(Context context) {
        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);

        isDataLoadedAtLeastOne = false;
        this.context = context;
    }

    public void getLocationsByArea() {

        unSubscribeAll();

        registerAsync(restService.getLocationsByArea("all"), new DisposableObserver<List<LocationResponse>>() {
            @Override
            public void onNext(@NonNull List<LocationResponse> data) {
                isDataLoadedAtLeastOne = true;
                locationResponse = data;
                getMvpView().onLocationsDataArrived(data);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                if (!isDataLoadedAtLeastOne) {
                    if (!networkUtils.isNetworkConnected()) {
                        waitForInternetComeBack();
                        getMvpView().onError("Connect to the Internet");
                    } else {
                        getMvpView().onError(e.getMessage());
                    }

                } else {
                    getMvpView().onLocationsDataArrived(locationResponse);
                }
            }

            @Override
            public void onComplete() {
            }
        });

    }

//    public void getStartUpData() {
//
//        Observable<StartUpResponse> responseDB =
//                settings.getStartUpData()
//                        .filter(startUpResponse -> startUpResponse != null)
//                        .doOnNext(data -> {
//                            isDataLoadedAtLeastOne = true;
//                            startUpResponse = data;
//                        })
//                        .subscribeOn(Schedulers.computation());
//
//        Observable<StartUpResponse> responseApi = restService.getStartUpData()
//                .map(startUpResponse -> {
//
//                    Observable.create(disposable -> {
//                        settings.saveStartUpData(startUpResponse, System.currentTimeMillis());
//                        disposable.onComplete();
//                    }).subscribeOn(Schedulers.computation()).subscribe();
//
//                    return startUpResponse;
//                })
//                .subscribeOn(Schedulers.io());
//
//        registerSync(Observable.concat(responseDB, responseApi), new DisposableObserver<StartUpResponse>() {
//            @Override
//            public void onNext(@NonNull StartUpResponse data) {
//                isDataLoadedAtLeastOne = true;
//                startUpResponse = data;
//                getMvpView().onStartUpDataReceived(data);
//            }
//
//            @Override
//            public void onError(@NonNull Throwable e) {
//                if (!isDataLoadedAtLeastOne) {
//                    waitForInternetComeBack();
//                    getMvpView().onError(e.getMessage());
//                } else {
//                    getMvpView().onStartUpDataReceived(startUpResponse);
//                }
//
//            }
//
//            @Override
//            public void onComplete() {
//
//            }
//        });
//    }


    private void waitForInternetComeBack() {
        if (internetStatusObserver == null || internetStatusObserver.isDisposed()) {
            if (!networkUtils.isNetworkConnected()) {
                internetStatusObserver = networkUtils
                        .stream()
                        .filter(internetConnectionStatus -> internetConnectionStatus)
                        .subscribeOn(Schedulers.computation())
                        .observeOn(Schedulers.computation())
                        .subscribe(internetConnectionStatus -> {
//                            getStartUpData();
                            getLocationsByArea();
                            stopWaitForInternetComeBack();
                        });
            }
        }
    }

    private void stopWaitForInternetComeBack() {
        if (internetStatusObserver != null || !internetStatusObserver.isDisposed()) {
            internetStatusObserver.dispose();
        }
    }
}
