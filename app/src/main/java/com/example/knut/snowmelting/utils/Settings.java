package com.example.knut.snowmelting.utils;

import android.content.SharedPreferences;

import com.example.knut.snowmelting.api.response.StartUpResponse;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.model.WeatherDay;
import com.google.gson.Gson;


import java.util.ArrayList;

import javax.inject.Singleton;

import io.reactivex.Observable;

/**
 * Created by Nizomjon on 10/02/2017.
 */

@Singleton
public class Settings {

    private SharedPreferences preferences;
    SharedPreferences.Editor prefsEditor;

    public Settings(SharedPreferences preferences) {
        this.preferences = preferences;
        prefsEditor = preferences.edit();
    }

    public void saveAuthToken(String token) {
        prefsEditor.putString("TOKEN", token).apply();
        prefsEditor.commit();
    }

    public boolean isFirstLunch() {
        if (preferences.getBoolean("FIRST_LUNCH", true)) {
            prefsEditor.putBoolean("FIRST_LUNCH", false).apply();
            prefsEditor.commit();
            return true;
        } else {
            return false;
        }
    }

    public void saveUser(UserResponse data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        prefsEditor.putString("USER", json);

        prefsEditor.commit();
    }

    public ArrayList<Integer> getIntArray(int sensorNodeId) {
        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            list.add(preferences.getInt(sensorNodeId + "_mic_" + i, 0));
        }

        return list;
    }

    public void putIntArray(int sensorNodeId, ArrayList<Integer> list) {

        int size = list.size();

        for (int i = 0; i < size; i++) {
            prefsEditor.putInt(sensorNodeId + "_mic_" + i, list.get(i));
        }

        prefsEditor.commit();
    }

    public void saveTurnOn(int sensorNodeId, long turnedOn) {
        prefsEditor.putLong("turn_on_time_" + sensorNodeId, turnedOn);
        prefsEditor.putBoolean("turn_on_" + sensorNodeId, true);
        prefsEditor.commit();
    }

    public long getTurnedOnTime(int sensorNodeId) {
        if (preferences.contains("turn_on_time_" + sensorNodeId)) {
            return preferences.getLong("turn_on_time_" + sensorNodeId, 0);
        } else {
            return 0;
        }
    }

    public boolean isTurnedOn(int sensorNodeId) {
        if (preferences.contains("turn_on_" + sensorNodeId)) {
            return preferences.getBoolean("turn_on_" + sensorNodeId, false);
        } else {
            return false;
        }
    }

    public void turnOff(int sensorNodeId) {
        prefsEditor.remove("turn_on_time_" + sensorNodeId);
        prefsEditor.remove("turn_on_" + sensorNodeId);
        prefsEditor.commit();
    }

    public void saveWeather(WeatherDay data, long time) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        prefsEditor.putString("WEATHER", json);
        prefsEditor.putLong("WeatherLastUpdatedTime", time);

        prefsEditor.commit();
    }

    public Observable<WeatherDay> getWeather() {
        Gson gson = new Gson();
        String json = preferences.getString("WEATHER", "");
        WeatherDay response = gson.fromJson(json, WeatherDay.class);

        if (response == null) {
            return Observable.empty();
        } else {
            return Observable.just(response);
        }

    }

    public String getAuthToken() {
        return preferences.getString("TOKEN", null);
    }

    public UserResponse getUser() {
        Gson gson = new Gson();
        String json = preferences.getString("USER", "");
        return gson.fromJson(json, UserResponse.class);
    }

    public boolean isAuthorized() {
        return getUser() != null;
    }

    public void logOutUser() {

        prefsEditor.remove("TOKEN");
        prefsEditor.remove("USER");
        prefsEditor.remove("FIRST_LUNCH");

        prefsEditor.commit();
    }

    public void saveStartUpData(StartUpResponse data, long time) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        prefsEditor.putString("StartUp", json);
        prefsEditor.putLong("StartUpLastUpdatedTime", time);

        prefsEditor.commit();
    }

    public Observable<StartUpResponse> getStartUpData() {
        Gson gson = new Gson();
        String json = preferences.getString("StartUp", "");
        StartUpResponse response = gson.fromJson(json, StartUpResponse.class);

        if (response == null) {
            return Observable.empty();
        } else {
            return Observable.just(response);
        }
    }

    public long getStartUpLastUpdatedTime() {
        return preferences.getLong("StartUpLastUpdatedTime", 0);
    }

    public long getWeatherLastUpdatedTime() {
        return preferences.getLong("WeatherLastUpdatedTime", 0);
    }


}
