package com.example.knut.snowmelting.views.dashboard.generalInfo;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.knut.snowmelting.BuildConfig;
import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.SkService;
import com.example.knut.snowmelting.api.body.TurnOffBody;
import com.example.knut.snowmelting.api.body.TurnOffResponseBody;
import com.example.knut.snowmelting.api.body.TurnOnBody;
import com.example.knut.snowmelting.api.body.TurnOnResponseBody;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.WeatherDay;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;

/**
 * Created by umarov on 5/10/17.
 */

public class GeneralInfoPresenter extends BasePresenter<GeneralInfoView> {

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skService;

    @Inject
    Settings settings;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    public GeneralInfoPresenter(Context mContext) {
        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }

    @Override
    public void detachView() {
        unSubscribeAll();
        super.detachView();
    }

    public void checkMicStatus(int sensorNodeId) {

        registerAsync(restService.checkMicStatus(), new ApiDisposableCallback<ResponseBody>() {

            @Override
            public void onAPIError(Exception e) {
                Log.d("TAG", e.getMessage());

            }

            @Override
            public void onNext(ResponseBody responseBody) {
                try {

//                    String result = responseBody.to
                    JSONArray jsonArray = new JSONObject(responseBody.string()).getJSONArray("result");

                    ArrayList<Integer> integers = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        switch (i) {
                            case 0: {
                                integers.add(jsonObject.getInt("mic_1"));
                                break;
                            }
                            case 1: {
                                integers.add(jsonObject.getInt("mic_2"));
                                break;
                            }
                            case 2: {
                                integers.add(jsonObject.getInt("mic_3"));
                                break;
                            }
                        }
                    }

                    saveMicList(sensorNodeId, integers);

                    if (isViewAttached()) {
                        getMvpView().onMicStatusArrived(integers);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void turnOn(int sensorNodeId) {

        unSubscribeAll();

        TurnOnBody turnOnBody = new TurnOnBody();
        turnOnBody.setType("on");
        turnOnBody.setSensor_node_id(sensorNodeId);

        registerAsync(restService.turnOn(turnOnBody), new ApiDisposableCallback<TurnOnResponseBody>() {
            @Override
            public void onAPIError(@NonNull Exception e) {
                if (isViewAttached()) {
                    getMvpView().turnOnFail();
                }
            }

            @Override
            public void onNext(@NonNull TurnOnResponseBody turnOnResponseBody) {

                if (turnOnResponseBody.getResult().getType().equals("success")) {
                    settings.saveTurnOn(sensorNodeId, System.currentTimeMillis());
//                    settings.putIntArray(sensorNodeId, turnOnResponseBody.getResult().getMcStatusListOn());

                    if (isViewAttached()) {
                        getMvpView().turnOnSuccess(turnOnResponseBody);
                    }
                } else {
                    if (isViewAttached()) {
                        getMvpView().turnOnFail();
                    }
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void saveMicList(int sensorNodeId, ArrayList<Integer> integers) {
        settings.putIntArray(0, integers); //TODO: change it when it is based on id
    }

    public ArrayList<Integer> getMicList(int sensorNodeId) {
        return settings.getIntArray(0); //TODO: change it when it is based on id
    }

    public long getTurnedTime(int sensorNodeId) {
        return settings.getTurnedOnTime(sensorNodeId);
    }

    public boolean isTurnedOn(int sensorNodeId) {
        return settings.isTurnedOn(sensorNodeId);
    }

    public void turnOff(int sensorNodeId) {
        unSubscribeAll();

        long turnedOn = getTurnedTime(sensorNodeId);

        if (turnedOn != 0) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long currentTime = System.currentTimeMillis();

            Date dateD = new Date();
            dateD.setTime(turnedOn);

            TurnOffBody turnOffBody = new TurnOffBody();
            turnOffBody.setSensorNodeId(sensorNodeId);
            turnOffBody.setType("off");
            turnOffBody.setStartTime(format.format(dateD));

            dateD.setTime(currentTime);

            turnOffBody.setEndTime(format.format(dateD));

            turnOffBody.setCalculatedTime((currentTime - turnedOn) / 1000);

            registerAsync(restService.turnOff(turnOffBody), new ApiDisposableCallback<TurnOffResponseBody>() {
                @Override
                public void onAPIError(@NonNull Exception e) {
                    if (isViewAttached()) {
                        getMvpView().turnOffFail();
                    }
                }

                @Override
                public void onNext(@NonNull TurnOffResponseBody turnOnResponseBody) {

                    if (turnOnResponseBody.getResult().getType().equals("success")) {
                        if (isViewAttached()) {
                            getMvpView().turnOffSuccess();
                        }

                        settings.turnOff(sensorNodeId);
                    } else {
                        if (isViewAttached()) {
                            getMvpView().turnOffFail();
                        }
                    }
                }

                @Override
                public void onComplete() {

                }
            });

        } else {

            if (isViewAttached()) {
                getMvpView().notTurnedOn();
            }
        }
    }

}
