package com.example.knut.snowmelting.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresPermission;

import com.example.knut.snowmelting.network.OnSubscribeBroadcastRegister;

import io.reactivex.Observable;


/**
 * Created by islom on 17. 5. 30.
 */

public class NetworkUtils {

    private Context mContext;
    private boolean isMonitoring;

    public NetworkUtils(Context context) {
        this.mContext = context;
    }

    @RequiresPermission(Manifest.permission.ACCESS_NETWORK_STATE)
    public Observable<Boolean> stream(){
        isMonitoring = true;
        final Context appContext = mContext.getApplicationContext();
        final IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        return Observable.create(new OnSubscribeBroadcastRegister(appContext, filter, null, null))
                .startWith(new Intent())
                .map(intent -> isNetworkConnected())
                .distinctUntilChanged();
    }

    public Boolean isNetworkConnected() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public boolean isMonitoring() {
        return isMonitoring;
    }
}
