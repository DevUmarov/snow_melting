package com.example.knut.snowmelting.model;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class TemperatureSk {

    private String tc;
    private String tmax;
    private String tmin;


    public String getTc() {
        return tc;
    }

    public String getTmax() {
        return tmax;
    }

    public String getTmin() {
        return tmin;
    }


}
