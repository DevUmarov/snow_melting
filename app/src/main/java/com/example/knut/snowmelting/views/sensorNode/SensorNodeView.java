package com.example.knut.snowmelting.views.sensorNode;

import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.api.response.SensorNodeResponse;
import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by islom on 17. 6. 1.
 */

public interface SensorNodeView extends MvpView {
        void onFullSensorNodeDataArrived(SensorNodeResponse sensorNodeResponse);
}
