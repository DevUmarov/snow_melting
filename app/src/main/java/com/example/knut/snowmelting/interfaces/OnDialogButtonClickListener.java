package com.example.knut.snowmelting.interfaces;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public interface OnDialogButtonClickListener {
    void onPositiveButtonClicked();

    void onNegativeButtonClicked();
}
