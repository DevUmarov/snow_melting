package com.example.knut.snowmelting.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.MvpView;

import butterknife.ButterKnife;

/**
 * Created by Nizomjon on 22/02/2017.
 */

public abstract class BaseFragment extends Fragment implements MvpView {

    private ProgressDialog progressDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getBaseActivity()))
                .presenterModule(new PresenterModule(getBaseActivity()))
                .build()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @LayoutRes
    protected abstract int getLayout();

    @Override
    public void onDestroy() {
        hideProgress();
        super.onDestroy();
    }

    public void createProgress(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
    }

    public void showProgress() {

        createProgress();

        if(!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    public void hideProgress() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.hide();
        }
    }

    @Override
    public void onError(String error) {
        hideProgress();
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }


    public void showNoConnection(){
        Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
    }



}
