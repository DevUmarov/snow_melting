package com.example.knut.snowmelting.views.weatherData.history;

import android.content.Context;

import com.example.knut.snowmelting.api.SkService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by umarov on 5/1/17.
 */

public class WeatherHistoryPresenter extends BasePresenter<WeatherHistoryView> {
    Context mContext;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;

    public WeatherHistoryPresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }

//    void forecast3hours(double latitude, double longitude) {
//        subscribe(skApi.getForeCast3days(latitude, longitude), new SubscriberCallback<>(new ApiCallback<Response>() {
//            @Override
//            public void onSuccess(Response model) {
//                getMvpView().fillGraph(model);
//            }
//
//            @Override
//            public void onFailure(RetrofitException exception) {
//                exception.getResponse();
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onNetworkError() {
//
//            }
//        }));
//    }

//    void forecast6days(double latitude, double longitude) {
//        subscribe(skApi.getForecast6days(latitude, longitude), new SubscriberCallback<>(new ApiCallback<Response>() {
//            @Override
//            public void onSuccess(Response model) {
//                getMvpView().fillForecast10days(model);
//            }
//
//            @Override
//            public void onFailure(RetrofitException exception) {
//                exception.getResponse();
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onNetworkError() {
//
//            }
//        }));
//    }

}
