package com.example.knut.snowmelting.views.sensorNode;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.api.response.SensorNodeResponse;
import com.example.knut.snowmelting.api.response.subModels.LastSeenValues;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.views.sensorNode.dataHistory.DataHistoryFragment;
import com.example.knut.snowmelting.views.sensorNode.mainMenu.MainMenuFragment;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 6. 1.
 */

public class SensorNodeActivity extends BaseActivity implements SensorNodeView {

    static final String EXTRA_SENSOR_ID = "com.example.knut.snowmelting.extras.sensor.id";
    static final String LAST_SEEN_VALUES = "com.example.knut.snowmelting.extras.sensor.LAST_SEEN_VALUES";

    @Inject
    Settings settings;

    @BindView(R.id.container_sensor_node)
    FrameLayout containerDetails;

    @Inject
    SensorNodePresenter sensorNodePresenter;

    @BindView(R.id.bottom_navigation_sensor_node)
    BottomNavigationView bottomNavigationView;


    private int sensorNodeId;
    private LastSeenValues lastSeenValues;
    //    private SensorNodeResponse sensorNodeResponse;
    private MainMenuFragment mainMenuFragment;
    private DataHistoryFragment dataHistoryFragment;
//    private OnOffHistoryFragment onOffHistoryFragment;

    public static Intent getStartIntent(Context context, int sensorNodeId, LastSeenValues lastSeenValues) {
        Intent intent = new Intent(context, SensorNodeActivity.class);
        intent.putExtra(EXTRA_SENSOR_ID, sensorNodeId);
        intent.putExtra(LAST_SEEN_VALUES, lastSeenValues);
        return intent;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorNodePresenter.detachView();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        if (getIntent() != null) {
            sensorNodeId = getIntent().getIntExtra(EXTRA_SENSOR_ID, -1);
            lastSeenValues = getIntent().getParcelableExtra(LAST_SEEN_VALUES);
        }

        sensorNodePresenter.attachView(this);

        initializeToolbar();

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.action_main_menu: {
                                replaceFragment(getMainMenuFragment());
                                break;
                            }
                            case R.id.action_data_history: {
//                                    if(filterByDateResponse != null) {
                                replaceFragment(getDataHistoryFragment());
//                                    }
                                break;
                            }
                        }
                        return true;
                    }
                });

        bottomNavigationView.setSelectedItemId(R.id.action_main_menu);
//        sensorNodePresenter.getFullSensorData(String.valueOf(sensorNodeId));
//        showProgress();
    }

    private MainMenuFragment getMainMenuFragment() {
        if (mainMenuFragment == null) {
            mainMenuFragment = MainMenuFragment.newInstance(lastSeenValues);
        }

        return mainMenuFragment;
    }

    private DataHistoryFragment getDataHistoryFragment() {

        if (dataHistoryFragment == null) {
            dataHistoryFragment = DataHistoryFragment.newInstance(sensorNodeId);
        }

        return dataHistoryFragment;
    }

//    private OnOffHistoryFragment getOnOffHistoryFragment(){
////        if(onOffHistoryFragment == null){
////            onOffHistoryFragment = OnOffHistoryFragment.newInstance(sensorNodeResponse.getOffHistoryResponseList());
////        }
//
//        return onOffHistoryFragment;
//    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_sensor_node, fragment);
        transaction.commit();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_sensor_node;
    }

    @Override
    public void handleBus(Object event) {

    }

    private void initializeToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.sensor_node) + " " + sensorNodeId);
        }
    }

    @Override
    public void onFullSensorNodeDataArrived(SensorNodeResponse sensorNodeResponse) {
        hideProgress();
//        this.sensorNodeResponse = sensorNodeResponse;
//        bottomNavigationView.setSelectedItemId(R.id.action_main_menu);
    }

}
