package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 06/03/2017.
 */

public class SkyPm {
    private String code;
    private String name;

    public SkyPm(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
