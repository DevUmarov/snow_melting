package com.example.knut.snowmelting.api.response;

import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by umarov on 5/17/17.
 */

public class SingleUserResponse {

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("isAdmin")
    @Expose
    private boolean isAdmin;

    @SerializedName("username")
    @Expose
    private String userName;

    @SerializedName("locations")
    @Expose
    private List<LocationResponse> locationList;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<LocationResponse> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<LocationResponse> locationList) {
        this.locationList = locationList;
    }

}
