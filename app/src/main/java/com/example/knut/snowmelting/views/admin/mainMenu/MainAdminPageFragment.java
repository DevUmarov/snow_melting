package com.example.knut.snowmelting.views.admin.mainMenu;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.TimeUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerLocListAdapter;
import com.example.knut.snowmelting.api.response.StartUpResponse;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.AppTimeUtils;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.views.dashboard.DashboardActivity;
import com.example.knut.snowmelting.views.locationList.LocationListActivity;
import com.example.knut.snowmelting.views.usersList.UsersListActivity;
import com.example.knut.snowmelting.widgets.AdminCircles;
import com.example.knut.snowmelting.widgets.MainCircleView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 5. 24.
 */

public class MainAdminPageFragment extends BaseFragment implements MainAdminPageView, View.OnClickListener {

    static final String EXTRA_ADMIN_DATA = "com.example.knut.snowmelting.extras.ADMIN_DATA";

    @Inject
    MainAdminPagePresenter mPresenter;

    @Inject
    Settings settings;
    private RecyclerLocListAdapter recyclerLocListAdapter;

//    @Inject
//    NetworkUtils networkUtils;

//    @BindView(R.id.admin_circles)
//    AdminCircles adminCircles;
//
//    @BindView(R.id.last_updated_time)
//    TextView lastUpdatedTime;

    @BindView(R.id.recycler_view_location_list)
    RecyclerView locationList;

    private List<LocationResponse> data;

//    private MainCircleView[] views;
//    private UserResponse data;
//    private StartUpResponse startUpResponse;

    public static MainAdminPageFragment newInstance(UserResponse data) {
        MainAdminPageFragment fragment = new MainAdminPageFragment();

        Bundle bundle = new Bundle();
        bundle.putParcelable(EXTRA_ADMIN_DATA, data);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);

//        data = getArguments().getParcelable(EXTRA_ADMIN_DATA);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.admin_main_menu);

        mPresenter.attachView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        initializeViews();

        initializeRecyclerView();

        if (data == null) {
//            if (networkUtils.isNetworkConnected()) {
                mPresenter.getLocationsByArea();
                showProgress();
//            } else {
//                Toast.makeText(getActivity(), R.string.no_internet, Toast.LENGTH_SHORT).show();
//            }
        } else {
            initData();
        }

//        if (startUpResponse == null) {
//            showProgress();
//            mPresenter.getStartUpData();
//        } else {
//            setData(startUpResponse);
//        }

    }

//    private void initializeViews() {
//        views = new MainCircleView[3];
//
//        for (int i = 0; i < views.length; i++) {
//            views[i] = new MainCircleView(getActivity(), this);
//            views[i].setOnClickListener(this);
//            adminCircles.addView(views[i]);
//        }
//
//        views[0].setId(R.id.locations_list);
//        views[1].setId(R.id.seoul_users);
//        views[2].setId(R.id.chungju_users);
//    }
//
//    private void setData(StartUpResponse startUpResponse) {
//        views[0].setData(Color.parseColor("#26A69A"), startUpResponse.getTotalLocCount(), getString(R.string.all_locations));
//        views[1].setData(Color.parseColor("#FF7043"), startUpResponse.getSeoulLocCount(), getString(R.string.seoul_locations));
//        views[2].setData(Color.parseColor("#D4E157"), startUpResponse.getChungjuLocCount(), getString(R.string.chungju_locations));
//    }
//
//    public int randomColor(int alpha) {
//
//        int r = (int) (0xff * Math.random());
//        int g = (int) (0xff * Math.random());
//        int b = (int) (0xff * Math.random());
//
//        return Color.argb(alpha, r, g, b);
//    }
//
//    private void initializeToolbar(String title, boolean isHomeEnabled) {
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setTitle(title);
//            if (isHomeEnabled) {
//                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                getSupportActionBar().setDisplayShowCustomEnabled(true);
//                getSupportActionBar().setDisplayShowHomeEnabled(true);
//            }
//        }
//    }

    private void initializeRecyclerView() {
        recyclerLocListAdapter = new RecyclerLocListAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        locationList.setLayoutManager(linearLayoutManager);
        locationList.setAdapter(recyclerLocListAdapter);
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_main_admin_page;
    }

    @Override
    public void onClick(View v) {

//        switch (v.getId()) {
//            case R.id.locations_list: {
//                startActivity(LocationListActivity.getStartIntent(getActivity(), "all"));
//                break;
//            }
//            case R.id.seoul_users: {
//                startActivity(LocationListActivity.getStartIntent(getActivity(), "seoul"));
//                break;
//            }
//            case R.id.chungju_users: {
//                startActivity(LocationListActivity.getStartIntent(getActivity(), "chungju"));
//                break;
//            }
//        }
    }

    private void initData() {
        recyclerLocListAdapter.setData(data);
        recyclerLocListAdapter.notifyDataSetChanged();
    }

    /**
     * MVP Implementation
     **/

//    @Override
//    public void onStartUpDataReceived(StartUpResponse startUpResponse) {
//        hideProgress();
//        this.startUpResponse = startUpResponse;
//        setData(startUpResponse);
//        lastUpdatedTime.setText(getString(R.string.last_update, AppTimeUtils.getDate(settings.getStartUpLastUpdatedTime())));
//    }
    @Override
    public void onLocationsDataArrived(List<LocationResponse> data) {
        hideProgress();
        this.data = data;
        initData();

    }

    @Override
    public void onItemClicked(int clickedPst, LocationResponse location) {
//        startActivity(DashboardActivity.getStartIntent(this, location));
        startActivity(DashboardActivity.getStartIntent(getActivity(), location.getUserId(), true));
    }
}
