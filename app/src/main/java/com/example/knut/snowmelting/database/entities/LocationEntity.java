package com.example.knut.snowmelting.database.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by islom on 17. 6. 8.
 */

@Entity(tableName = "location")
public class LocationEntity {

//    public LocationEntity(int id, int areaCode, int userId, String address) {
//        this.id = id;
//        this.areaCode = areaCode;
//        this.address = address;
//        this.userId = userId;
//    }
//
//    public LocationEntity(int id, int userId, String address) {
//        this.id = id;
//        this.address = address;
//        this.userId = userId;
//    }


    @PrimaryKey
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "user_id")
    public int userId;

    @ColumnInfo(name = "area_code")
    public int areaCode;

    @ColumnInfo(name = "address")
    public String address;
}
