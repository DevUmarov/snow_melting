package com.example.knut.snowmelting.api.body;

/**
 * Created by umarov on 8/24/17.
 */

public class TurnOnBody {
    private String type;
    private int sensor_node_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSensor_node_id() {
        return sensor_node_id;
    }

    public void setSensor_node_id(int sensor_node_id) {
        this.sensor_node_id = sensor_node_id;
    }
}
