package com.example.knut.snowmelting.utils;

import android.os.Build;

/**
 * Created by umarov on 4/27/17.
 */

public class ApiLevelHelper {

    private ApiLevelHelper() {
        //no instance
    }

    public static boolean isAtLeast(int apiLevel) {
        return Build.VERSION.SDK_INT >= apiLevel;
    }

    public static boolean isLowerThan(int apiLevel) {
        return Build.VERSION.SDK_INT < apiLevel;
    }
}
