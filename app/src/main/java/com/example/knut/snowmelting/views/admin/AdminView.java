package com.example.knut.snowmelting.views.admin;

import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by umarov on 5/22/17.
 */

public interface AdminView extends MvpView {
    void onNoConnection();
}
