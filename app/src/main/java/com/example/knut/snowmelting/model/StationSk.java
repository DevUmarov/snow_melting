package com.example.knut.snowmelting.model;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class StationSk {
    private String city;
    private String county;
    private String village;
    private String latitude;
    private String longitude;
    private String name;

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public String getCounty() {
        return county;
    }

    public String getVillage() {
        return village;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
