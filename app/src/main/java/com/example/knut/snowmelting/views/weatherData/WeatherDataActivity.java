package com.example.knut.snowmelting.views.weatherData;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.ViewPagerAdapter;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.views.weatherData.forecast.WeatherForecastFragment;
import com.example.knut.snowmelting.views.weatherData.history.WeatherHistoryFragment;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by umarov on 4/21/17.
 */

public class WeatherDataActivity extends BaseActivity implements WeatherDataView {

    @Inject
    WeatherDataPresenter weatherDataPresenter;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.view_pager)
    public ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        initializeToolbar();


        ViewTreeObserver viewTreeObserver = tabLayout.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    setupViewPager();
                    tabLayout.setupWithViewPager(viewPager);
                }
            });
        }

    }

    private void initializeToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("Forecast and Data History");
        }
    }


    private void setupViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPagerAdapter.addFragment(WeatherHistoryFragment.newInstance(), "Weather History");
//        viewPagerAdapter.addFragment(WeatherForecastFragment.newInstance(), "Weather Forecast");

//        viewPagerAdapter.addFragment(WeatherFragment.newInstance(12), "Today");
//        viewPagerAdapter.addFragment(new PiFragment(), "Real Time");

        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onError(String error) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_weather_data_new;
    }

    @Override
    public void handleBus(Object event) {

    }


}
