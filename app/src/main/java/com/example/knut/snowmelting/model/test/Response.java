package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Response {

    final Weather weather = null;
    final Common common = null;
    final Result result = null;

    public Weather getWeather() {
        return weather;
    }

    public Common getCommon() {
        return common;
    }

    public Result getResult() {
        return result;
    }

    @Override
    public String toString() {
        return new StringBuilder("Response{")
                .append("weather=").append(weather)
                .append(", common=").append(common)
                .append(", result=").append(result)
                .append('}').toString();
    }

}
