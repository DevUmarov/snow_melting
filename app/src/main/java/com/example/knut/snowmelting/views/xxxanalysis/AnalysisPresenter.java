package com.example.knut.snowmelting.views.xxxanalysis;

import android.content.Context;

import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;

/**
 * Created by umarov on 4/20/17.
 */

public class AnalysisPresenter extends BasePresenter<AnalysisView> {

    Context mContext;

    public AnalysisPresenter(Context context) {
        mContext = context;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .build()
                .inject(this);

    }

}
