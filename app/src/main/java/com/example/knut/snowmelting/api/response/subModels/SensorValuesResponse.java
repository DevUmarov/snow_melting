package com.example.knut.snowmelting.api.response.subModels;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by islom on 17. 6. 8.
 */

public class SensorValuesResponse implements Parcelable {

    @SerializedName("id")
    @Expose
    int id;

    @SerializedName("sensor_id")
    @Expose
    int sensorId;

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("updated_at")
    @Expose
    String updatedAt;

    @SerializedName("value")
    @Expose
    int value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    protected SensorValuesResponse(Parcel in) {
        id = in.readInt();
        sensorId = in.readInt();
        type = in.readString();
        updatedAt = in.readString();
        value = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(sensorId);
        dest.writeString(type);
        dest.writeString(updatedAt);
        dest.writeInt(value);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SensorValuesResponse> CREATOR = new Parcelable.Creator<SensorValuesResponse>() {
        @Override
        public SensorValuesResponse createFromParcel(Parcel in) {
            return new SensorValuesResponse(in);
        }

        @Override
        public SensorValuesResponse[] newArray(int size) {
            return new SensorValuesResponse[size];
        }
    };
}