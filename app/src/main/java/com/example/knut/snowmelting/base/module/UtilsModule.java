package com.example.knut.snowmelting.base.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.knut.snowmelting.utils.AppUtils;
import com.example.knut.snowmelting.utils.DialogsUtil;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Module
public class  UtilsModule {

    private Context context;

    public UtilsModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    AppUtils getAppUtils() {
        return new AppUtils(context);
    }

    @Provides
    @Singleton
    NetworkUtils getNetworkUtils() {
        return new NetworkUtils(context);
    }

    @Provides
    @Singleton
    DialogsUtil getDialogUtils() {
        return new DialogsUtil(context);
    }

    @Provides
    @Singleton
    @Named("new_thread")
    Scheduler getNewThread() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @Named("main_thread")
    Scheduler getMainThread() {
        return AndroidSchedulers.mainThread();
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @Singleton
    Settings provideSettings() {
        return new Settings(provideSharedPreferences());
    }
}
