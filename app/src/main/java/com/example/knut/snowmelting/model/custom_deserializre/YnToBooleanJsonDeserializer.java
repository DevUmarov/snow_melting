package com.example.knut.snowmelting.model.custom_deserializre;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class YnToBooleanJsonDeserializer implements JsonDeserializer<Boolean> {

    @Override
    public Boolean deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final String rawFlag = json.getAsString();
        switch ( rawFlag ) {
            case "N":
                return false;
            case "Y":
                return true;
            default:
                throw new JsonParseException("Can't parse: " + rawFlag);
        }
    }
}
