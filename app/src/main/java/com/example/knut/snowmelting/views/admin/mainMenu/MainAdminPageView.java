package com.example.knut.snowmelting.views.admin.mainMenu;

import com.example.knut.snowmelting.api.response.StartUpResponse;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.mvp.MvpView;

import java.util.List;

/**
 * Created by islom on 17. 5. 24.
 */

public interface MainAdminPageView extends MvpView {
//        public void onStartUpDataReceived(StartUpResponse startUpResponse);

        void onLocationsDataArrived(List<LocationResponse> data);

        void onItemClicked(int clickedPst, LocationResponse location);
}
