package com.example.knut.snowmelting.views.usersList;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.database.AppDatabase;
import com.example.knut.snowmelting.database.entities.UserEntity;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by islom on 17. 5. 24.
 */

public class UsersListPresenter extends BasePresenter<UsersListView> {

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    @Inject
    NetworkUtils networkUtils;

    @Inject
    Settings settings;

    @Inject
    AppDatabase appDatabase;

    private boolean needsData;
    private Context context;
    private boolean isDataLoadedAtLeastOne;
    private Disposable internetStatusObserver;
    private List<UserResponse> userResponses;

    public UsersListPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);
        isDataLoadedAtLeastOne = false;

        this.context = context;
    }

    private Observable<List<UserEntity>> getAllUsersFromDB(){
        return Observable.defer(new Callable<ObservableSource<? extends List<UserEntity>>>() {
            @Override
            public ObservableSource<? extends List<UserEntity>> call() throws Exception {
                return Observable.just(appDatabase.userDao().getAllUsers());
            }
        });
    }

    public void getAllUsers() {

        Observable<List<UserResponse>> responseDB =
                        getAllUsersFromDB()
                        .filter(response -> response.size() > 0)
                        .doOnNext(data -> {
                            isDataLoadedAtLeastOne = true;
                        })
                        .map(users -> {
                            List<UserResponse> dataFromDB = new ArrayList<UserResponse>();

                            for(UserEntity user: users){
                                dataFromDB.add(new UserResponse(user.id, user.email, user.name, user.isAdmin));
                            }

                            userResponses = dataFromDB;

                            return userResponses;
                        })
                        .subscribeOn(Schedulers.computation());

        Observable<List<UserResponse>> responseApi = restService.getAllUsers()
                .map(userList -> {

                    Observable.create(disposable -> {

                        for(UserResponse data : userList){
                            appDatabase.userDao().insertUser(new UserEntity(data.getId(),
                                    data.getEmail(),
                                    data.getUserName(),
                                    data.isAdmin()));
                        }

                        disposable.onComplete();
                    }).subscribeOn(Schedulers.computation()).subscribe();

                    return userList;
                })
                .subscribeOn(Schedulers.io());

        registerSync(Observable.concat(responseDB, responseApi), new DisposableObserver<List<UserResponse>>() {
            @Override
            public void onNext(@NonNull List<UserResponse> data) {
                isDataLoadedAtLeastOne = true;
                userResponses = data;
                getMvpView().onUserDataArrived(data);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                if (!isDataLoadedAtLeastOne) {
                    waitForInternetComeBack();
                    getMvpView().onError(e.getMessage());
                } else {
                    getMvpView().onUserDataArrived(userResponses);
                }
            }

            @Override
            public void onComplete() {

            }
        });

    }

    private void waitForInternetComeBack() {
        if (internetStatusObserver == null || internetStatusObserver.isDisposed()) {
            if (!networkUtils.isNetworkConnected()) {
                internetStatusObserver = networkUtils
                        .stream()
                        .filter(internetConnectionStatus -> internetConnectionStatus )
                        .subscribeOn(Schedulers.computation())
                        .observeOn(Schedulers.computation())
                        .subscribe(internetConnectionStatus -> {
                            getAllUsers();
                            stopWaitForInternetComeBack();
                        });
            }
        }
    }

    private void stopWaitForInternetComeBack(){
        if (internetStatusObserver != null || !internetStatusObserver.isDisposed()) {
            internetStatusObserver.dispose();
        }
    }


}
