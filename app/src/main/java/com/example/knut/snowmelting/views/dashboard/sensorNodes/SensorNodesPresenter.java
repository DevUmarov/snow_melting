package com.example.knut.snowmelting.views.dashboard.sensorNodes;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.api.response.SingleUserResponse;
import com.example.knut.snowmelting.mvp.BasePresenter;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.annotations.NonNull;


/**
 * Created by umarov on 5/10/17.
 */

public class SensorNodesPresenter extends BasePresenter<SensorNodesView> {

    Context mContext;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    public SensorNodesPresenter(Context mContext) {
        this.mContext = mContext;

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(mContext))
                .databaseModule(new DatabaseModule(mContext))
                .build()
                .inject(this);
    }

//    public void getAdvancedInfo(int userId){
//        unSubscribeAll();
//
//        getMvpView().showProgress();
//
//        registerAsync(restService.getSensorData(userId), new ApiDisposableCallback<SingleUserResponse>(){
//            @Override
//            public void onAPIError(@NonNull Exception e) {
//                getMvpView().onError(e.getMessage());
//            }
//
//            @Override
//            public void onNext(@NonNull SingleUserResponse data) {
//                getMvpView().showAdvancedInfo(data);
//            }
//
//            @Override
//            public void onComplete() {
//                getMvpView().hideProgress();
//            }
//        });

//        subscribe(restService.getSensorData(userId), new SubscriberCallback<>(new ApiCallback<SingleUserResponse>() {
//            @Override
//            public void onSuccess(SingleUserResponse data) {
//                getMvpView().showAdvancedInfo(data);
//            }
//
//            @Override
//            public void onFailure(RetrofitException exception) {
//                exception.getResponse();
//            }
//
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onNetworkError() {
//
//            }
//        }));


//    }

}
