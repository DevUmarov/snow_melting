package com.example.knut.snowmelting.views.dashboard.onOffHistory;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerStatusHistoryAdapter;
import com.example.knut.snowmelting.api.response.subModels.OnOffHistoryResponse;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by umarov on 5/12/17.
 */

public class OnOffHistoryFragment extends BaseFragment implements OnOffHistoryView {
    static final String EXTRA_LIST = "com.example.knut.snowmelting.extras.EXTRA_LIST";
    RecyclerStatusHistoryAdapter recyclerStatusHistoryAdapter;

    @Inject
    OnOffHistoryPresenter onOffHistoryPresenter;

    @BindView(R.id.recycler_view_history)
    RecyclerView recyclerView;

    private ArrayList<OnOffHistoryResponse> mOnOffHistoryResponses;
    private LinearLayoutManager linearLayoutManager;

//    public static OnOffHistoryFragment newInstance(List<OnOffHistoryResponse> onOffHistoryResponses) {
//
//        OnOffHistoryFragment fragment = new OnOffHistoryFragment();
//        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList(EXTRA_LIST, new ArrayList<>(onOffHistoryResponses));
//        fragment.setArguments(bundle);
//
//        return fragment;
//    }

    public static OnOffHistoryFragment newInstance() {
        OnOffHistoryFragment fragment = new OnOffHistoryFragment();
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_on_off_history;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);

        onOffHistoryPresenter.attachView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

//        if (getArguments() != null) {
//            if (getArguments().containsKey(EXTRA_LIST)) {
//                mOnOffHistoryResponses = getArguments().getParcelableArrayList(EXTRA_LIST);
//            }
//        }

        recyclerStatusHistoryAdapter = new RecyclerStatusHistoryAdapter(getActivity());
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), linearLayoutManager.getOrientation()));
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerStatusHistoryAdapter);

        if(mOnOffHistoryResponses != null){
            recyclerStatusHistoryAdapter.setData(mOnOffHistoryResponses);
            recyclerStatusHistoryAdapter.notifyDataSetChanged();
        }

        super.onViewCreated(view, savedInstanceState);
    }

    public void setData(ArrayList<OnOffHistoryResponse> onOffHistoryResponses) {
        mOnOffHistoryResponses = onOffHistoryResponses;

        if (isAdded() && isVisible()) {
            recyclerStatusHistoryAdapter.setData(mOnOffHistoryResponses);
            recyclerStatusHistoryAdapter.notifyDataSetChanged();
        }
    }
}
