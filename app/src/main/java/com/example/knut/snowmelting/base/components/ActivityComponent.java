package com.example.knut.snowmelting.base.components;

import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.service.ControllerService;
import com.example.knut.snowmelting.views.admin.AdminActivity;
import com.example.knut.snowmelting.views.admin.feedback.FeedbackFragment;
import com.example.knut.snowmelting.views.admin.mainMenu.MainAdminPageFragment;
import com.example.knut.snowmelting.views.dashboard.generalInfo.GeneralInfoFragment;
import com.example.knut.snowmelting.views.dashboard.onOffHistory.OnOffHistoryFragment;
import com.example.knut.snowmelting.views.main.MainActivity;
import com.example.knut.snowmelting.views.sensorNode.SensorNodeActivity;
import com.example.knut.snowmelting.views.sensorNode.dataHistory.DataHistoryFragment;
import com.example.knut.snowmelting.views.sensorNode.mainMenu.MainMenuFragment;
import com.example.knut.snowmelting.views.xxxanalysis.AnalysisActivity;
import com.example.knut.snowmelting.views.dashboard.DashboardActivity;
import com.example.knut.snowmelting.views.dashboard.sensorNodes.SensorNodesFragment;
import com.example.knut.snowmelting.views.locationList.LocationListActivity;
import com.example.knut.snowmelting.views.login.LoginActivity;
import com.example.knut.snowmelting.views.register.RegisterActivity;
import com.example.knut.snowmelting.views.usersList.UsersListActivity;
import com.example.knut.snowmelting.views.weatherData.WeatherDataActivity;
import com.example.knut.snowmelting.views.weatherData.forecast.WeatherForecastFragment;
import com.example.knut.snowmelting.views.weatherData.history.WeatherHistoryFragment;
import com.example.knut.snowmelting.views.xxxmain.XXXMainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Singleton
@Component(modules = {PresenterModule.class, UtilsModule.class})
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);

    void inject(LoginActivity loginActivity);

    void inject(RegisterActivity registerActivity);

    void inject(DashboardActivity mainActivity);

    void inject(AnalysisActivity analysisActivity);

    void inject(AdminActivity adminActivity);

    void inject(WeatherDataActivity weatherDataActivity);

    void inject(BaseFragment baseFragment);

    void inject(GeneralInfoFragment generalInfoFragment);

    void inject(WeatherHistoryFragment weatherHistoryPresenter);

    void inject(WeatherForecastFragment weatherForecastPresenter);

    void inject(SensorNodesFragment sensorNodesFragment);

    void inject(UsersListActivity usersListActivity);

    void inject(MainAdminPageFragment mainAdminPageFragment);

    void inject(FeedbackFragment feedbackActivity);

    void inject(LocationListActivity locationListActivity);

    void inject(SensorNodeActivity sensorNodeActivity);

    void inject(MainMenuFragment mainMenuFragment);

    void inject(OnOffHistoryFragment onOffHistoryFragment);

    void inject(DataHistoryFragment dataHistoryFragment);

    void inject(MainActivity mainActivity);

    void inject(XXXMainActivity xxxMainActivity);


}
