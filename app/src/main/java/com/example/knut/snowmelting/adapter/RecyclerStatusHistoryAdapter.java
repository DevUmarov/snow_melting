package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.OnOffHistoryResponse;
import com.example.knut.snowmelting.databinding.ItemFeedbackBinding;
import com.example.knut.snowmelting.databinding.ItemStatusHistoryBinding;
import com.example.knut.snowmelting.model.FeedbackData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.ButterKnife;

/**
 * Created by umarov on 4/25/17.
 */

public class RecyclerStatusHistoryAdapter extends RecyclerView.Adapter<RecyclerStatusHistoryAdapter.RecyclerViewHolder> {

    Context mContext;
    ArrayList<OnOffHistoryResponse> onOffHistoryResponses;
    SimpleDateFormat input;
    SimpleDateFormat output;

    public RecyclerStatusHistoryAdapter(Context context) {
        input = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        output = new SimpleDateFormat("dd MMM, HH:mm:ss", Locale.ENGLISH);
//        output.setTimeZone(TimeZone.getTimeZone("UTC"));
        mContext = context;
        onOffHistoryResponses = new ArrayList<>();
    }

    public void setData(ArrayList<OnOffHistoryResponse> onOffHistoryResponses){
        this.onOffHistoryResponses = onOffHistoryResponses;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemStatusHistoryBinding itemBinding = ItemStatusHistoryBinding.inflate(layoutInflater, parent, false);
        return new RecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        OnOffHistoryResponse cur = onOffHistoryResponses.get(position);

        try {
            cur.setCalculatedTime(getHourMinutes(cur.getCalculatedTime()));
            input.setLenient( true );

            cur.setStartTime(output.format(input.parse(cur.getStartTime())));
            cur.setEndTime(output.format(input.parse(cur.getEndTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NumberFormatException e){
            e.printStackTrace();
        }

        holder.bind(cur);
    }

    @Override
    public int getItemCount() {
        return onOffHistoryResponses.size();
    }

    private String getHourMinutes(String sek) throws NumberFormatException{
        float f = Float.valueOf(sek);
        int second = (int) f;

        StringBuilder res = new StringBuilder();

        int hour = second / 3600;

        if (hour > 0) {
            res.append(hour);
            res.append(" h");
            res.append(", ");
        }

        int minute = second / 60 - hour * 3600;

        if (hour > 0 || minute > 0) {
            res.append(minute);
            res.append(" m");
            res.append(", ");
        }

        second = second - minute * 60;
        res.append(second);
        res.append(" s");

        return res.toString();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemStatusHistoryBinding binding;

        public RecyclerViewHolder(ItemStatusHistoryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setOnClick(this);
        }

        public void bind(OnOffHistoryResponse item) {
            binding.setHistoryResponse(item);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {

        }
    }
}
