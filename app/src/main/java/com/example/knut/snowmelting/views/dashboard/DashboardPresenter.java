package com.example.knut.snowmelting.views.dashboard;

import android.content.Context;
import android.widget.Toast;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.body.FeedbackBody;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.api.response.ResultResponse;
import com.example.knut.snowmelting.api.response.SingleUserResponse;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.WeatherPi;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.google.gson.Gson;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by umarov on 4/19/17.
 */

public class DashboardPresenter extends BasePresenter<DashboardView> {


    @Inject
    @Named(NetModule.PRODUCTION)
    Gson gson;

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    @Inject
    NetworkUtils networkUtils;

    private Disposable internetStatusObserver;

    private Context context;
    private int userId;
    private MqttAndroidClient mqttAndroidClient;
    private String clientId = "UdblabClientId";
    private static final String main_broker_url = "ws://159.203.160.131:8080/mqtt";
    private static final String hive_broker_url = "tcp://broker.hivemq.com:1883";
    private static final String amazon_broker_url = "tcp://m12.cloudmqtt.com:13226";
    private static final String sub_current_data_topic = "udblab/sensor/+/current_data/";
    private static final String pub_senor_heating_topic = "udblab/sensor/1112/heating/";

    public DashboardPresenter(Context context) {
        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);

        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopWaitForInternetComeBack();
    }

    public void sendFeedback(FeedbackBody feedbackBody) {

        unSubscribeAll();

        registerAsync(restService.sendFeedback(feedbackBody), new ApiDisposableCallback<ResultResponse>() {
            @Override
            public void onAPIError(@NonNull Exception e) {
                if (isViewAttached()) {
                    getMvpView().onFeedbackSendError();
                }
            }

            @Override
            public void onNext(@NonNull ResultResponse resultResponse) {
                if (isViewAttached()) {
                    getMvpView().onFeedbackSend();
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void getSingleUserData(int userId) {
        this.userId = userId;

        if (networkUtils.isNetworkConnected()) {
            unSubscribeAll();

//            Observable<SingleUserResponse> responseApi =
//                    restService.getSingelUserData(userId)
//                            .subscribeOn(Schedulers.io());

//        Observable.concat(responseDB, responseApi)

            registerAsync(restService.getSingelUserData(userId), new DisposableObserver<SingleUserResponse>() {
                @Override
                public void onNext(@NonNull SingleUserResponse data) {
                    if (isViewAttached()) {
                        getMvpView().onSingleUserDataArrived(data);
                    }
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    if (isViewAttached()) {
                        getMvpView().onError(e.getMessage());
                    }
                }

                @Override
                public void onComplete() {

                }
            });
        } else {
            getMvpView().onError("Connect to the Internet");
            monitorInternetConnection();
        }
    }

    public void monitorInternetConnection() {
        if (internetStatusObserver == null || internetStatusObserver.isDisposed()) {
            internetStatusObserver = networkUtils
                    .stream()
                    .subscribeOn(Schedulers.computation())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(internetConnectionStatus -> {

                        if (internetConnectionStatus) {
                            getSingleUserData(userId);
                            stopWaitForInternetComeBack();
                        }
//                        if (RxBus.getInstance().hasObservers() && internetConnectionStatus) {
//                            RxBus.getInstance().post(true);
//                        }
//
//                        if (!internetConnectionStatus) {
//                            Toast.makeText(DashboardActivity.this, "Offline", Toast.LENGTH_SHORT).show();
//                        }
                    });
        }
    }

    private void stopWaitForInternetComeBack() {
        if (internetStatusObserver == null) {
            return;
        }

        if (!internetStatusObserver.isDisposed()) {
            internetStatusObserver.dispose();
        }
    }


//    public void initMqtt(String broker_url, boolean isMain, boolean isHive) {
//
//        clientId = clientId + System.currentTimeMillis();
//
//        mqttAndroidClient = new MqttAndroidClient(context, broker_url, clientId);
//        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
//            @Override
//            public void connectComplete(boolean reconnect, String serverURI) {
//
//                if (reconnect) {
//                    getMvpView().onReConnected();
//                    subscribeToTopic();
//                } else {
//                    getMvpView().onConnected("");
//                }
//            }
//
//
//            @Override
//            public void connectionLost(Throwable cause) {
//                Toast.makeText(context, ("The Connection was lost."), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void messageArrived(String topic, MqttMessage message) throws Exception {
//                String json = new String(message.getPayload());
//                WeatherPi weatherPi = gson.fromJson(json, WeatherPi.class);
//                getMvpView().onMessageArrived(weatherPi);
//            }
//
//            @Override
//            public void deliveryComplete(IMqttDeliveryToken token) {
//
//            }
//        });
//
//        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
//        mqttConnectOptions.setCleanSession(!isMain);
//        mqttConnectOptions.setUserName(isMain ? "udblab" : "hlqbtvzv");
//        mqttConnectOptions.setPassword(isMain ? "12345".toCharArray() : "rGA8NiRaD2KX".toCharArray());
//        mqttConnectOptions.setAutomaticReconnect(true);
//
//        try {
//            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
//                    disconnectedBufferOptions.setBufferEnabled(true);
//                    disconnectedBufferOptions.setBufferSize(100);
//                    disconnectedBufferOptions.setPersistBuffer(false);
//                    disconnectedBufferOptions.setDeleteOldestMessages(false);
//                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
//                    subscribeToTopic();
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    if (isMain && isHive) {
//                        initMqtt(amazon_broker_url, false, false);
//
//                    } else if (!isMain && !isHive) {
//                        initMqtt(hive_broker_url, true, false);
//                    } else {
//                        initMqtt(main_broker_url, true, true);
//                    }
//                    getMvpView().onFailed();
//                }
//            });
//
//
//        } catch (MqttException ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    private void subscribeToTopic() {
//        try {
//            mqttAndroidClient.subscribe(sub_current_data_topic, 0, null, new IMqttActionListener() {
//                @Override
//                public void onSuccess(IMqttToken asyncActionToken) {
//                    getMvpView().onSubscribed();
//                }
//
//                @Override
//                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//
//                }
//            });
//        } catch (MqttException ex) {
//            System.err.println("Exception whilst subscribing");
//            ex.printStackTrace();
//        }
//    }
//
//    void publishMessage(String publishMessage) {
//
//        try {
//            MqttMessage message = new MqttMessage();
//
//            message.setPayload(publishMessage.getBytes());
//            if (mqttAndroidClient != null) {
//                mqttAndroidClient.publish(pub_senor_heating_topic, message);
//                if (!mqttAndroidClient.isConnected()) {
//                    Toast.makeText(context, "mqttAndroidClient.getBufferedMessageCount() + \" messages in buffer", Toast.LENGTH_SHORT).show();
//                }
//            }
//        } catch (MqttException e) {
//            System.err.println("Error Publishing: " + e.getMessage());
//            e.printStackTrace();
//        }
//    }
//
//    public void onPauseMode() {
//    }
//
//
//    public void onDestroyMode() {
//    }

}
