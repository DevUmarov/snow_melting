package com.example.knut.snowmelting.views.admin.feedback;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerFeedbackAdapter;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.FeedbackData;
import com.example.knut.snowmelting.views.admin.feedbackRespond.FeedbackRespondFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 5. 25.
 */

public class FeedbackFragment extends BaseFragment implements FeedbackView, FeedbackRespondFragment.ActivityInterAction {

    @BindView(R.id.list_feedback)
    RecyclerView recyclerView;

    @Inject
    FeedbackPresenter feedbackPresenter;

    private RecyclerFeedbackAdapter recyclerFeedbackAdapter;
    private List<FeedbackData> mFeedbackDatas;

    public static FeedbackFragment newInstance(UserResponse data) {
        FeedbackFragment fragment = new FeedbackFragment();
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViews();

        if (mFeedbackDatas == null) {
            showProgress();
            feedbackPresenter.getFeedbacks();
        } else {
            initData(mFeedbackDatas);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.feedback);

        feedbackPresenter.attachView(this);
    }

    private void initializeViews() {
        recyclerFeedbackAdapter = new RecyclerFeedbackAdapter(getActivity(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(recyclerFeedbackAdapter);
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_feedback;
    }

    @Override
    public void onResponClicked(int feedbackId, String message) {
        feedbackPresenter.respondToUser(feedbackId, message);
    }

    private void initData(List<FeedbackData> feedbackDataList) {
        recyclerFeedbackAdapter.setData(feedbackDataList);
        recyclerFeedbackAdapter.notifyDataSetChanged();
    }

    /**
     * MVP implementation
     **/
    @Override
    public void onResonseSuccess() {
        Toast.makeText(getActivity(), R.string.feedback_response_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponseFail() {
        Toast.makeText(getActivity(), R.string.feedback_response_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(View v, FeedbackData feedbackData) {

    }

    @Override
    public void onRespondClick(FeedbackData feedbackData) {
        BottomSheetDialogFragment bottomSheetDialogFragment = FeedbackRespondFragment.newInstance(feedbackData.getId());
        bottomSheetDialogFragment.show(getChildFragmentManager(), bottomSheetDialogFragment.getTag());
    }

    @Override
    public void onFeedbacksArrived(List<FeedbackData> feedbackDataList) {
        mFeedbackDatas = feedbackDataList;
        hideProgress();
        initData(feedbackDataList);
    }

    @Override
    public void onFeedbacksError() {
        hideProgress();
        Toast.makeText(getActivity(), R.string.feedback_receive_fail, Toast.LENGTH_SHORT).show();
    }
}
