package com.example.knut.snowmelting.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.knut.snowmelting.database.entities.SensorNodeEntity;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by islom on 17. 6. 8.
 */

@Dao
public interface SensorNodeDao {

    @Insert(onConflict = REPLACE)
    void insertSensorNode(SensorNodeEntity sensorNodeEntity);

    @Query("SELECT * FROM sensor_node")
    List<SensorNodeEntity> getAllSensorNodes();

    @Query("SELECT * FROM sensor_node WHERE location_id = :locationId")
    List<SensorNodeEntity> getSensorNodesByLocationId(int locationId);
}
