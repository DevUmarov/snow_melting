package com.example.knut.snowmelting.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class WeatherPi {

    @Expose
    @SerializedName("temp")
    private String temperature;

    @Expose
    @SerializedName("hum")
    private String humidity;

    @Expose
    @SerializedName("date")
    private String current_time;

    @Expose
    @SerializedName("wind_spped")
    private String wind_speed;

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public String getWind_speed() {
        return wind_speed;
    }
}
