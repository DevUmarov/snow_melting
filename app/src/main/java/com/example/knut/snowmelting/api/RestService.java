package com.example.knut.snowmelting.api;


import com.example.knut.snowmelting.api.body.ConnectionBody;
import com.example.knut.snowmelting.api.body.FeedbackBody;
import com.example.knut.snowmelting.api.body.FilterByDateBody;
import com.example.knut.snowmelting.api.body.LoginBody;
import com.example.knut.snowmelting.api.body.RegisterBody;
import com.example.knut.snowmelting.api.body.ResponseFeedbackBody;
import com.example.knut.snowmelting.api.body.TurnOffBody;
import com.example.knut.snowmelting.api.body.TurnOffResponseBody;
import com.example.knut.snowmelting.api.body.TurnOnBody;
import com.example.knut.snowmelting.api.body.TurnOnResponseBody;
import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.api.response.ResultResponse;
import com.example.knut.snowmelting.api.response.SensorNodeResponse;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.api.response.LoginResponse;
import com.example.knut.snowmelting.api.response.SingleUserResponse;
import com.example.knut.snowmelting.api.response.StartUpResponse;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.model.FeedbackData;
import com.example.knut.snowmelting.model.SkWeather;
import com.example.knut.snowmelting.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import io.reactivex.Observable;

/**
 * Created by Nizomjon on 09/02/2017.
 */
public interface RestService {

    @POST("/api/v1/registerAsync/")
    Observable<User> register(@Body RegisterBody registerBody);

    @POST("/api/v1/login/")
    Observable<LoginResponse> login(@Body LoginBody loginBody);

    @GET
    @Headers({
            "Accept: application/json",
            "Content-Language: ko,ko-kr"
    })

    Observable<SkWeather> current_weather(@Url String url);

    @GET("/api/v1/users/{id}")
    Observable<SingleUserResponse> getSingelUserData(@Path("id") int userId);

    @GET("/api/v1/users")
    Observable<List<UserResponse>> getAllUsers();

    @GET("/api/v1/location")
    Observable<List<LocationResponse>> getLocationsByArea(@Query("q") String location);

    @GET("/api/v1/startup")
    Observable<StartUpResponse> getStartUpData();

    @GET("/api/v1/node_sensor")
    Observable<SensorNodeResponse> getFullSensorNodeData(@Query("id") String sensorNodeId);

    @POST("/api/v1/feedback")
    Observable<ResultResponse> sendFeedback(@Body FeedbackBody feedbackBody);

    @POST("/api/v1/feedback")
    Observable<ResponseFeedbackBody> respondToFeedback(@Body ResponseFeedbackBody responseFeedbackBody);

    @GET("/api/v1/feedbacks/")
    Observable<List<FeedbackData>> getFeedbacks();

    @POST("/api/v1/filter_by_date/")
    Observable<FilterByDateResponse> getDataChange(@Body FilterByDateBody filterByDateBody);

    @POST("/api/v1/heating/")
    Observable<TurnOnResponseBody> turnOn(@Body TurnOnBody turnOnBody);

    @POST("/api/v1/heating/")
    Observable<TurnOffResponseBody> turnOff(@Body TurnOffBody turnOffBody);

    @GET("/api/v1/connected_client")
    Call<ConnectionBody> checkConnection();

    @GET("/api/v1/check_mic_status")
    Observable<ResponseBody> checkMicStatus();
}
