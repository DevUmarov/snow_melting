package com.example.knut.snowmelting.views.admin;

import android.content.Context;
import android.support.annotation.MainThread;
import android.widget.Toast;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.base.RxBus;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.views.main.MainActivity;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by umarov on 5/22/17.
 */

public class AdminPresenter extends BasePresenter<AdminView> {

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    private Context context;

    public AdminPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);

        this.context = context;
    }

}
