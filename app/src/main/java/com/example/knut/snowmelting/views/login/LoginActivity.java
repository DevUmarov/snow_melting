package com.example.knut.snowmelting.views.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.AppUtils;
import com.example.knut.snowmelting.utils.DialogsUtil;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.utils.Settings;
import com.example.knut.snowmelting.views.admin.AdminActivity;
import com.example.knut.snowmelting.views.dashboard.DashboardActivity;
import com.example.knut.snowmelting.views.locationList.LocationListActivity;
import com.example.knut.snowmelting.views.main.MainActivity;
import com.example.knut.snowmelting.views.intro.IntroActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 09/02/2017.
 */

public class LoginActivity extends BaseActivity implements LoginView {

    public static final int REQUEST_CODE_REGISTER = 1001;
    public static final int REQUEST_CODE_INTRODUCTION = 1002;

    private UserResponse user;

    @Inject
    LoginPresenter loginPresenter;
    @Inject
    Settings settings;

    @Inject
    DialogsUtil dialogsUtil;

    @Inject
    AppUtils appUtils;

    @Inject
    NetworkUtils newNetworkUtils;

    @BindView(R.id.input_email)
    EditText emailT;

    @BindView(R.id.input_password)
    EditText passwordT;

//    @BindView(R.id.input_layout_email)
//    TextInputLayout inputEmailLayout;
//
//    @BindView(R.id.input_layout_password)
//    TextInputLayout inputPassLayout;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

        loginPresenter.attachView(this);
        user = settings.getUser();

        if (settings.isFirstLunch()) {
            startActivityForResult(new Intent(this, IntroActivity.class), REQUEST_CODE_INTRODUCTION);
        } else {
            checkUser();
        }

        setTitle(getString(R.string.login));
        emailT.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
        passwordT.getBackground().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
    }

    @OnClick(R.id.btn_login)
    void login() {

        if(!newNetworkUtils.isNetworkConnected()){
            Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
            return;
        }

        String email = emailT.getText().toString();
        String password = passwordT.getText().toString();

        if (email.isEmpty()) {
            Toast.makeText(this, R.string.input_email_error, Toast.LENGTH_SHORT).show();
        } else if (password.isEmpty()) {
            Toast.makeText(this, R.string.input_password_error, Toast.LENGTH_SHORT).show();
        } else {
            appUtils.hideSoftKeyboard(this);
            showProgress();
            loginPresenter.login(email, password);
        }
    }

//    @OnClick(R.id.link_signup)
//    void register_link() {
//        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
//        startActivityForResult(intent, REQUEST_CODE_REGISTER);
//    }

    @Override
    public int getLayout() {
        return R.layout.activity_login_new;
    }

    @Override
    public void handleBus(Object event) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_REGISTER) {
            if (resultCode == RESULT_OK) {
                String username = data.getStringExtra("username");
                emailT.setText(username);
            }
        }

        if (requestCode == REQUEST_CODE_INTRODUCTION) {
            checkUser();
        }
    }

    private void checkUser() {
        if (user != null) {

            if (user.isAdmin()) {
                startActivity(AdminActivity.getStartIntent(this, user));
            } else {
//                startActivity(LocationListActivity.getStartIntent(this, user));
//                startActivity(DashboardActivity.getStartIntent(this, user.getId()));
                startActivity(DashboardActivity.getStartIntent(this, 3, false));
            }

            finish();
        }
    }

    /***** MVP View methods implementation *****/

    @Override
    public void onLoginError() {
        hideProgress();
        Toast.makeText(this, R.string.login_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginSuccess(UserResponse userResponse) {

        hideProgress();

        if (userResponse.isAdmin()) {
            startActivity(AdminActivity.getStartIntent(this, userResponse));
        } else {
//            startActivity(LocationListActivity.getStartIntent(this, userResponse));
            startActivity(DashboardActivity.getStartIntent(this, userResponse.getId(), false));
        }

        Toast.makeText(this, R.string.login_success, Toast.LENGTH_SHORT).show();

        finish();
    }

}
