package com.example.knut.snowmelting.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.body.ConnectionBody;
import com.example.knut.snowmelting.api.body.ConnectionResult;
import com.example.knut.snowmelting.base.module.NetModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by umarov on 9/18/17.
 */

public class ControllerService extends Service {

    public static final String FAIL = "com.example.knut.snowmelting.service.FAIL";
    public static final String SUCCESS = "com.example.knut.snowmelting.service.SUCCESS";
    public static final String SUCCESS_MAC_ADDRESS = "com.example.knut.snowmelting.service.SUCCESS_MAC_ADDRESS";

    public static final String ERROR = "error";

    private final IBinder iBinder = new LocalBinder();
    private LocalBroadcastManager localBroadcastManager;
    private final static int INTERVAL = 10000;
    Handler mHandler = new Handler();
    Call<ConnectionBody> call;
    Retrofit retrofit;
    RestService mRestService;

    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            listenConnection();
            mHandler.postDelayed(mHandlerTask, INTERVAL);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopListening();
        return super.onUnbind(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl(NetModule.PRODUCTION_API)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .build();

        mRestService = retrofit.create(RestService.class);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    Gson provideGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    public class LocalBinder extends Binder {
        public ControllerService getService() {
            return ControllerService.this;
        }
    }

    private void listenConnection() {

        call = mRestService.checkConnection();

        call.enqueue(new Callback<ConnectionBody>() {
            @Override
            public void onResponse(Call<ConnectionBody> call, Response<ConnectionBody> response) {
                if (response.isSuccessful()) {

                    try {
                        ConnectionResult result = response.body().getConnectionResult();

                        if (result.getType().equals(ERROR)) {
                            localBroadcastManager.sendBroadcast(new Intent(FAIL));
                        } else {

                            StringBuilder macAddress = new StringBuilder();
                            ArrayList<String> strings = result.getMacAddress();

                            for (int i = 0; i < strings.size(); i++) {
                                macAddress.append(strings.get(i).toUpperCase());

                                if (i != strings.size() - 1) {
                                    macAddress.append("-");
                                }
                            }

                            localBroadcastManager.sendBroadcast(new Intent(SUCCESS).putExtra(SUCCESS_MAC_ADDRESS, macAddress.toString()));
                            stopListening();
                        }
                    } catch (NullPointerException e) {
                        localBroadcastManager.sendBroadcast(new Intent(FAIL));
                    }

                } else {
                    localBroadcastManager.sendBroadcast(new Intent(FAIL));
                }
            }

            @Override
            public void onFailure(Call<ConnectionBody> call, Throwable t) {
                localBroadcastManager.sendBroadcast(new Intent(FAIL));
            }
        });
    }

    public void startListening() {
        mHandlerTask.run();
    }

    private void stopListening() {
        mHandler.removeCallbacks(mHandlerTask);
    }
}
