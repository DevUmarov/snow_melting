package com.example.knut.snowmelting.views.admin.feedback;

import android.view.View;

import com.example.knut.snowmelting.model.FeedbackData;
import com.example.knut.snowmelting.mvp.MvpView;

import java.util.List;

/**
 * Created by islom on 17. 5. 25.
 */

public interface FeedbackView extends MvpView {

    void onItemClick(View v, FeedbackData feedbackData);

    void onRespondClick(FeedbackData feedbackData);

    void onFeedbacksArrived(List<FeedbackData> feedbackDataList);

    void onFeedbacksError();

    void onResonseSuccess();

    void onResponseFail();

}
