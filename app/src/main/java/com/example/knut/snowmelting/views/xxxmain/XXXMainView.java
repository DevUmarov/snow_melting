package com.example.knut.snowmelting.views.xxxmain;

import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.mvp.MvpView;

import java.util.List;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public interface XXXMainView extends MvpView {

//    void onMessageArrived(WeatherPi weatherPi);
//
//    void onConnected(String url);
//
//    void onFailed();
//
//    void onReConnected();
//
//    void onSubscribed();

    void onUserDataArrived(List<UserResponse> data);
}
