package com.example.knut.snowmelting.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nizomjon on 16/02/2017.
 */

public class WeatherSk {
    @SerializedName("minutely")
    private
    List<WeatherConditionSk> hourlSk;

    @SerializedName("forecast3days")
    private List<WeatherConditionSk> forecast3days;

    public List<WeatherConditionSk> getForecast3days() {
        return forecast3days;
    }

    public List<WeatherConditionSk> getHourlSk() {
        return hourlSk;
    }

}
