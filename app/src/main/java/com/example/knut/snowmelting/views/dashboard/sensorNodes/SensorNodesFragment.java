package com.example.knut.snowmelting.views.dashboard.sensorNodes;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.SensorNodeResponse;
import com.example.knut.snowmelting.base.BaseFragment;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.CustomMarker;
import com.example.knut.snowmelting.views.sensorNode.SensorNodeActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Inject;

/**
 * Created by umarov on 5/10/17.
 */

public class SensorNodesFragment extends BaseFragment implements SensorNodesView,
        OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    static final String EXTRA_SENSOR_NODES = "com.example.knut.snowmelting.extras.SENSOR_NODES";

    @Inject
    SensorNodesPresenter mPresenter;


    // Google Map
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap googleMap;
    private HashMap markersHashMap;
    private Iterator<Map.Entry> iter;
    private SupportMapFragment mapFragment;
    private ArrayList<SensorNodeResponse> sensorNodeList;
    private CameraPosition mCameraPosition;

//    public static SensorNodesFragment newInstance(ArrayList<SensorNodeResponse> sensorNodeList) {
//
//        SensorNodesFragment fragment = new SensorNodesFragment();
//        Bundle bundle = new Bundle();
//        bundle.putParcelableArrayList(EXTRA_SENSOR_NODES, sensorNodeList);
//        fragment.setArguments(bundle);
//
//        return fragment;
//    }

    public static SensorNodesFragment newInstance() {
        SensorNodesFragment fragment = new SensorNodesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(getActivity()))
                .presenterModule(new PresenterModule(getActivity()))
                .build()
                .inject(this);

        mPresenter.attachView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        if (getArguments() != null) {
//            if (getArguments().containsKey(EXTRA_SENSOR_NODES)) {
//                sensorNodeList = getArguments().getParcelableArrayList(EXTRA_SENSOR_NODES);
//            }
//        }

//        initializeRecyclerView();

//        if (data == null) {
//            mPresenter.getAdvancedInfo(userId);
//        } else {
//            showAdvancedInfo(data);
//        }

        if (sensorNodeList != null) {
            configureUI();
        }
    }

    public void setData(ArrayList<SensorNodeResponse> sensorNodeList) {
        this.sensorNodeList = sensorNodeList;

        if (isAdded() && isVisible()) {
            configureUI();
        }
    }

    public void configureUI() {
//        if (googleMap == null) {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
//        } else {
//            initData();
//        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

//        SupportMapFragment f = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//
//        if (f != null) {
//            getFragmentManager().beginTransaction().remove(f).commit();
//        }
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_sensor_nodes;
    }

//    private void initializeRecyclerView() {
//        adapter = new RecyclerAdvInfoAdapter(getActivity(), this, recyclerView);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
//        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setAdapter(adapter);
//    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (isResumed() && isAdded()) {
            initializeUiSettings();
            initializeMapLocationSettings();
            initializeMapTraffic();
            initializeMapType();
            initializeMapViewSettings();
            initData();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void initializeUiSettings() {

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                iter = markersHashMap.entrySet().iterator();
                CustomMarker customMarker = null;

                while (iter.hasNext()) {
                    Map.Entry mEntry = (Map.Entry) iter.next();
                    customMarker = (CustomMarker) mEntry.getKey();
                    Marker curMarker = (Marker) mEntry.getValue();

                    if (curMarker.equals(marker)) {
                        break;
                    }
                }

                if (customMarker != null) {
                    startActivity(SensorNodeActivity.getStartIntent(getActivity(),
                            customMarker.getCustomMarkerId(), sensorNodeList.get(0).getLastSeenValues()));
                }

                return false;
            }
        });
        googleMap.getUiSettings().setCompassEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    public void initializeMapLocationSettings() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                googleMap.setMyLocationEnabled(true);
            }
        } else {
            googleMap.setMyLocationEnabled(true);
        }
    }

    public void initializeMapTraffic() {
        googleMap.setTrafficEnabled(true);
    }

    public void initializeMapType() {
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public void initializeMapViewSettings() {
        googleMap.setIndoorEnabled(true);
        googleMap.setBuildingsEnabled(false);
    }

    private void initData() {
        removeAllMarkers();
        setMarkers();
    }

    void setMarkers() {
        for (int i = 0; i < sensorNodeList.size(); i++) {
            CustomMarker customMarker =
                    new CustomMarker(sensorNodeList.get(i).getSensorNodeId(),
                            sensorNodeList.get(i).getLatitude(),
                            sensorNodeList.get(i).getLongitude());

            float markerColor;

            if (i % 2 == 0) {
                markerColor = BitmapDescriptorFactory.HUE_RED;
            } else {
                markerColor = BitmapDescriptorFactory.HUE_BLUE;
            }

            addMarker(customMarker, markerColor, getString(R.string.marker) + " " + i);
        }

        if (markersHashMap != null) {
            zoomAnimateLevelToFitMarkers(120);
        }
    }


    public void removeAllMarkers() {
        if (markersHashMap != null) {
            iter = markersHashMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry mEntry = (Map.Entry) iter.next();
                Marker marker = (Marker) mEntry.getValue();
                marker.remove();
            }

            markersHashMap.clear();
        }
    }

    public void addMarkerToHashMap(CustomMarker customMarker, Marker marker) {

        if (markersHashMap == null) {
            markersHashMap = new HashMap();
        }

        markersHashMap.put(customMarker, marker);
    }

    public Marker findMarker(CustomMarker customMarker) {
        iter = markersHashMap.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry mEntry = (Map.Entry) iter.next();
            CustomMarker key = (CustomMarker) mEntry.getKey();

            if (customMarker.getCustomMarkerId() == (key.getCustomMarkerId())) {
                Marker value = (Marker) mEntry.getValue();
                return value;
            }
        }
        return null;
    }

    public void addMarker(CustomMarker customMarker, float markerColor, String markerTitle) {

        MarkerOptions markerOption = new MarkerOptions()
                .position(new LatLng(customMarker.getCustomMarkerLatitude(), customMarker.getCustomMarkerLongitude()))
                .icon(BitmapDescriptorFactory.defaultMarker(markerColor))
                .title(markerTitle);

        Marker newMark = googleMap.addMarker(markerOption);
        newMark.showInfoWindow();

        addMarkerToHashMap(customMarker, newMark);
    }

    public void removeMarker(CustomMarker customMarker) {
        if (markersHashMap != null) {
            if (findMarker(customMarker) != null) {
                findMarker(customMarker).remove();
                markersHashMap.remove(customMarker);
            }
        }
    }

    public void zoomAnimateLevelToFitMarkers(int padding) {
        CameraUpdate cu;

        if (mCameraPosition == null) {
            LatLngBounds.Builder b = new LatLngBounds.Builder();
            iter = markersHashMap.entrySet().iterator();

            while (iter.hasNext()) {
                Map.Entry mEntry = (Map.Entry) iter.next();
                CustomMarker key = (CustomMarker) mEntry.getKey();
                LatLng ll = new LatLng(key.getCustomMarkerLatitude(), key.getCustomMarkerLongitude());
                b.include(ll);
            }

            LatLngBounds bounds = b.build();

            cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);

            googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    googleMap.moveCamera(cu);
                    mCameraPosition = googleMap.getCameraPosition();
                }
            });
        } else {
            cu = CameraUpdateFactory.newCameraPosition(mCameraPosition);
            googleMap.moveCamera(cu);
        }
    }

    /***** MVP View methods implementation *****/
    @Override
    public void onSensorNodeClick(int position) {
//        startActivity(SensorNodeActivity.getStartIntent(getActivity(), data.getLocationDataList().get(0).getSensorDatas().get(position).getSensorNodeId()));
    }
}
