package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.media.Image;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.model.WeatherPi;
import com.example.knut.snowmelting.model.test.Weather;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by umarov on 4/20/17.
 */

public class RecyclerRealTimeAdapter extends RecyclerView.Adapter<RecyclerRealTimeAdapter.RecyclerViewHolder> {

    private Context context;
    private ArrayList<DataModel> data;

    public RecyclerRealTimeAdapter(Context context) {
        this.context = context;
        initData(new WeatherPi());
    }

    public void setData(WeatherPi weatherPi) {
        initData(weatherPi);
        notifyDataSetChanged();
    }

    private void initData(WeatherPi weatherPi) {
        data = new ArrayList<>();
        data.add(new DataModel("Humidity", weatherPi.getHumidity(), R.drawable.humidity_icon));
        data.add(new DataModel("Temperature", weatherPi.getTemperature(), R.drawable.temperature_icon));
        data.add(new DataModel("Wind speed", weatherPi.getWind_speed(), R.drawable.wind_speed_icon));
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_real_time, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        if (data.size() > position) {
            holder.type.setText(data.get(position).getTitle());
            holder.value.setText(data.get(position).getValue());
            holder.icon.setBackground(context.getResources().getDrawable(data.get(position).getResId()));
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_real_time)
        ImageView icon;
        @BindView(R.id.text_sensor_type)
        TextView type;
        @BindView(R.id.text_sensor_value)
        TextView value;


        private RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private class DataModel {
        private String title;
        private String value;
        private int resId;

        DataModel(String title, String value, int resId) {
            this.title = title;
            this.value = value;
            this.resId = resId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public int getResId() {
            return resId;
        }

        public void setResId(int resId) {
            this.resId = resId;
        }
    }
}
