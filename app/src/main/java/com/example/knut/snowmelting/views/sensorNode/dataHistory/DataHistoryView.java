package com.example.knut.snowmelting.views.sensorNode.dataHistory;

import com.example.knut.snowmelting.api.response.FilterByDateResponse;
import com.example.knut.snowmelting.mvp.MvpView;

/**
 * Created by islom on 17. 6. 12.
 */

public interface DataHistoryView extends MvpView {
    void onDataChangArrived(FilterByDateResponse filterByDateResponse);
}
