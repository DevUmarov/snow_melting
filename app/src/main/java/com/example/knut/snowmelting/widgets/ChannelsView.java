package com.example.knut.snowmelting.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.knut.snowmelting.R;

/**
 * Created by umarov on 9/18/17.
 */

public class ChannelsView extends View {

    private Paint p, mTextPaint;
    int parentWidth;
    int parentHeight;
    int radius;
    int coords[][] = new int[4][2];
    int bottomCirclesY;
    int shortLine;
    int longLine;
    int mainChannelColor;
    int micOneColor;
    int micTwoColor;
    int micThreeColor;
    private final Rect textBounds = new Rect();

    public ChannelsView(Context context) {
        super(context);
        init(context);
    }

    public ChannelsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ChannelsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        p = new Paint();
        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mTextPaint.setTextSize(context.getResources().getDimension(R.dimen.text_size_mics));

        this.mainChannelColor = Color.RED;
        this.micOneColor = Color.RED;
        this.micTwoColor = Color.RED;
        this.micThreeColor = Color.RED;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {

        int widthCenter = parentWidth / 2;
        shortLine = parentHeight / 6;
        longLine = parentHeight / 2;
        radius = shortLine / 2;

        coords[0][0] = widthCenter;
        coords[0][1] = shortLine;

        bottomCirclesY = coords[0][1] + 2 * radius + 2 * shortLine;

        coords[1][0] = widthCenter;
        coords[1][1] = bottomCirclesY;

        coords[2][0] = widthCenter - longLine;
        coords[2][1] = bottomCirclesY;

        coords[3][0] = widthCenter + longLine;
        coords[3][1] = bottomCirclesY;

        super.onLayout(changed, left, top, right, bottom);
    }

    public void setMainChannelColors(int mainChannelColor, int micOneColor, int micTwoColor, int micThreeColor) {
        this.mainChannelColor = mainChannelColor;
        this.micOneColor = micOneColor;
        this.micTwoColor = micTwoColor;
        this.micThreeColor = micThreeColor;

        invalidate();
    }

    public void drawTextCentred(Canvas canvas, Paint paint, String text, float cx, float cy){
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, cx - textBounds.exactCenterX(), cy - textBounds.exactCenterY(), paint);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        p.setColor(mainChannelColor);
        canvas.drawCircle(coords[0][0], coords[0][1], radius, p);
        drawTextCentred(canvas, mTextPaint, "C_P", coords[0][0] + 3 * radius, coords[0][1]);

        p.setColor(micTwoColor);
        canvas.drawCircle(coords[1][0], coords[1][1], radius, p);
        drawTextCentred(canvas, mTextPaint, "MC_2", coords[1][0], coords[1][1] + 2 * radius);

        p.setColor(micOneColor);
        canvas.drawCircle(coords[2][0], coords[2][1], radius, p);
        drawTextCentred(canvas, mTextPaint, "MC_1", coords[2][0], coords[2][1] + 2 * radius);

        p.setColor(micThreeColor);
        canvas.drawCircle(coords[3][0], coords[3][1], radius, p);
        drawTextCentred(canvas, mTextPaint, "MC_3", coords[3][0], coords[3][1] + 2 * radius);

        p.setStrokeWidth(7);

        canvas.drawLine(parentWidth / 2, coords[0][1] + radius, parentWidth / 2, coords[1][1] - radius, p);
        canvas.drawLine(coords[2][0], bottomCirclesY - radius - shortLine, coords[3][0], bottomCirclesY - radius - shortLine, p);

        canvas.drawLine(coords[2][0], coords[2][1] - radius - shortLine, coords[2][0], coords[2][1] - radius, p);
        canvas.drawLine(coords[3][0], coords[3][1] - radius - shortLine, coords[3][0], coords[3][1] - radius, p);

        super.onDraw(canvas);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth, parentHeight);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
