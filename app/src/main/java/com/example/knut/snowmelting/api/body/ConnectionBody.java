package com.example.knut.snowmelting.api.body;

import com.google.android.gms.common.*;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by umarov on 9/18/17.
 */

public class ConnectionBody {

    @SerializedName("result")
    @Expose
    ConnectionResult connectionResult;

    public ConnectionResult getConnectionResult() {
        return connectionResult;
    }

    public void setConnectionResult(ConnectionResult connectionResult) {
        this.connectionResult = connectionResult;
    }
}
