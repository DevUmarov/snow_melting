package com.example.knut.snowmelting.api.body;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class LoginBody {
    private String email;
    private String password;

    public LoginBody(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
