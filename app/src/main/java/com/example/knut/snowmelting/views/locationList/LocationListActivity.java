package com.example.knut.snowmelting.views.locationList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.adapter.RecyclerLocListAdapter;
import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.NetworkUtils;
import com.example.knut.snowmelting.views.dashboard.DashboardActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by islom on 17. 5. 30.
 */

public class LocationListActivity extends BaseActivity implements LocationListView {

    private static String LOC_ID_EXTRA = "com.example.knut.snowmelting.views.locationList.LOCATION_ID";
//    private static String USER_EXTRA = "com.example.knut.snowmelting.views.locationList.USER";

    @Inject
    LocationListPresenter locationListPresenter;

    @Inject
    NetworkUtils networkUtils;

    @BindView(R.id.recycler_view_location_list)
    RecyclerView locationList;

    private List<LocationResponse> data;
//    private UserResponse userResponse;
    private RecyclerLocListAdapter recyclerLocListAdapter;
    private String location;

    public static Intent getStartIntent(Context context, String location) {
        // Admin
        Intent intent = new Intent(context, LocationListActivity.class);
        intent.putExtra(LOC_ID_EXTRA, location);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);

//        initializeRecyclerView();

        locationListPresenter.attachView(this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
//          showProgress(this);
            if (extras.containsKey(LOC_ID_EXTRA)) {
                location = getIntent().getStringExtra(LOC_ID_EXTRA);
                initializeToolbar(location.substring(0, 1).toUpperCase() + location.substring(1) + " " + getString(R.string.locations), true);

                if (networkUtils.isNetworkConnected()) {
                    locationListPresenter.getLocationsByArea(location);
                    showProgress();
                } else {
                    Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
                }
            }

//            else if (extras.containsKey(USER_EXTRA)) {
//                userResponse = getIntent().getParcelableExtra(USER_EXTRA);
//                locationListPresenter.getLocationsByUserId(userResponse.getId());
//                initializeToolbar(userResponse.getUserName() + " locations", false);
//            }

        }
    }

    private void initializeToolbar(String title, boolean isHomeEnabled) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            if (isHomeEnabled) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowCustomEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }
    }

//    private void initializeRecyclerView() {
//        recyclerLocListAdapter = new RecyclerLocListAdapter(this);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        locationList.setLayoutManager(linearLayoutManager);
//        locationList.setAdapter(recyclerLocListAdapter);
//    }

    @Override
    public int getLayout() {
        return R.layout.activity_location_list;
    }

    @Override
    public void handleBus(Object event) {

    }

    /*** MVP Implementation ***/

    @Override
    public void onLocationsDataArrived(List<LocationResponse> data) {
        hideProgress();
        this.data = data;
        recyclerLocListAdapter.setData(data);
        recyclerLocListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(int clickedPst, LocationResponse location) {
//        startActivity(DashboardActivity.getStartIntent(this, location));
        startActivity(DashboardActivity.getStartIntent(this, location.getUserId(), true));
    }
}
