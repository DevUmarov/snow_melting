package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 04/03/2017.
 */

public class Temperature {

    private String tMax;
    private String tMin;

    public Temperature(String tMax, String tMin) {
        this.tMax = tMax;
        this.tMin = tMin;
    }

    public String gettMax() {
        return tMax;
    }

    public String gettMin() {
        return tMin;
    }
}
