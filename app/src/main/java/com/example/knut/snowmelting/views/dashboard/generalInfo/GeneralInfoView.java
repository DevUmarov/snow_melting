package com.example.knut.snowmelting.views.dashboard.generalInfo;

import com.example.knut.snowmelting.api.body.TurnOnResponseBody;
import com.example.knut.snowmelting.model.WeatherDay;
import com.example.knut.snowmelting.mvp.MvpView;

import java.util.ArrayList;

/**
 * Created by umarov on 5/10/17.
 */

public interface GeneralInfoView extends MvpView {

    void onHourlyWeatherDataArrived(WeatherDay weatherDay);

    void turnOnFail();
    void turnOnSuccess(TurnOnResponseBody turnOnResponseBody);

    void turnOffFail();
    void turnOffSuccess();

    void onMicStatusArrived(ArrayList<Integer> integers);

    void notTurnedOn();
}
