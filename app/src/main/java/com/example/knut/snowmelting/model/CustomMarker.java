package com.example.knut.snowmelting.model;

/**
 * Created by umarov on 4/18/17.
 */

public class CustomMarker {

    private int id;
    private Double latitude;
    private Double longitude;

    public CustomMarker(int id, Double latitude, Double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getCustomMarkerId() {
        return id;
    }

    public void setCustomMarkerId(int id) {
        this.id = id;
    }

    public Double getCustomMarkerLatitude() {
        return latitude;
    }

    public void setCustomMarkerLatitude(Double mLatitude) {
        this.latitude = mLatitude;
    }

    public Double getCustomMarkerLongitude() {
        return longitude;
    }

    public void setCustomMarkerLongitude(Double mLongitude) {
        this.longitude = mLongitude;
    }
}