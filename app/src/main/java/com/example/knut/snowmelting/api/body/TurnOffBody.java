package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 8/24/17.
 */

public class TurnOffBody {

    @SerializedName("start_time")
    @Expose
    String startTime;

    @SerializedName("end_time")
    @Expose
    String endTime;

    @SerializedName("sensor_node_id")
    @Expose
    int sensorNodeId;

    @SerializedName("type")
    @Expose
    String type;

    @SerializedName("calculated_time")
    @Expose
    long calculatedTime;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public int getSensorNodeId() {
        return sensorNodeId;
    }

    public void setSensorNodeId(int sensorNodeId) {
        this.sensorNodeId = sensorNodeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getCalculatedTime() {
        return calculatedTime;
    }

    public void setCalculatedTime(long calculatedTime) {
        this.calculatedTime = calculatedTime;
    }
}
