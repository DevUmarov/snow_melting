package com.example.knut.snowmelting.views.register;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.Toast;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.base.BaseActivity;
import com.example.knut.snowmelting.base.components.DaggerActivityComponent;
import com.example.knut.snowmelting.base.module.PresenterModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.utils.DialogsUtil;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by Nizomjon on 10/02/2017.
 */

public class RegisterActivity extends BaseActivity implements RegisterView {

    @Inject
    RegisterPresenter registerPresenter;

    @Inject
    DialogsUtil dialogsUtil;

    @BindView(R.id.input_name)
    EditText userNameT;
    @BindView(R.id.input_email)
    EditText emailT;
    @BindView(R.id.input_password)
    EditText passwordT;
    ProgressDialog progressDialog;
    private boolean isSuccess;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(getString(R.string.loading));
        setHomeAsUp();
        setTitle("Register");

        DaggerActivityComponent.builder()
                .utilsModule(new UtilsModule(this))
                .presenterModule(new PresenterModule(this))
                .build()
                .inject(this);
        registerPresenter.attachView(this);
    }

    @OnClick(R.id.btn_signup)
    void register() {
        String username = userNameT.getText().toString();
        String email = emailT.getText().toString();
        String password = passwordT.getText().toString();

        if (!username.isEmpty() && !password.isEmpty() && !email.isEmpty()) {
//            registerPresenter.registerAsync(username, email, password);
        } else {
            Toast.makeText(this, "Please fill the fields", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.link_login)
    void login(){
        finish();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_register_new;
    }

    @Override
    public void handleBus(Object event) {

    }

    @Override
    public void onRegisterSuccess() {
        isSuccess = true;
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void closeApplication() {
        if (isSuccess){
            Intent intent = new Intent();
            intent.putExtra("username", userNameT.getText().toString());
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
}
