package com.example.knut.snowmelting.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by islom on 17. 5. 25.
 */

public class FeedbackData implements Parcelable {

    int id;

    @SerializedName("location_id")
    @Expose
    int locationId;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("sensor_node_id")
    @Expose
    int sensorNodeId;

    @SerializedName("user_id")
    @Expose
    int userId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getSensorNodeId() {
        return sensorNodeId;
    }

    public void setSensorNodeId(int sensorNodeId) {
        this.sensorNodeId = sensorNodeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    protected FeedbackData(Parcel in) {
        id = in.readInt();
        locationId = in.readInt();
        message = in.readString();
        sensorNodeId = in.readInt();
        userId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(locationId);
        dest.writeString(message);
        dest.writeInt(sensorNodeId);
        dest.writeInt(userId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<FeedbackData> CREATOR = new Parcelable.Creator<FeedbackData>() {
        @Override
        public FeedbackData createFromParcel(Parcel in) {
            return new FeedbackData(in);
        }

        @Override
        public FeedbackData[] newArray(int size) {
            return new FeedbackData[size];
        }
    };
}