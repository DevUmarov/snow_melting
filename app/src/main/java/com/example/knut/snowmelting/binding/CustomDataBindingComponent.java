package com.example.knut.snowmelting.binding;

/**
 * Created by umarov on 4/28/17.
 */

public class CustomDataBindingComponent implements android.databinding.DataBindingComponent{

    @Override
    public BindingAdapters getBindingAdapters() {
        return new CustomBindingAdapter();
    }
}
