package com.example.knut.snowmelting.model.test;


/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Sky {

    private String code;
    private String name;
    private SkyAm skyAm;
    private SkyPm skPm;

    public Sky(SkyAm skyAm, SkyPm skPm) {
        this.skyAm = skyAm;
        this.skPm = skPm;
    }

    public Sky(final String code, final String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public SkyAm getSkyAm() {
        return skyAm;
    }

    public SkyPm getSkPm() {
        return skPm;
    }
}
