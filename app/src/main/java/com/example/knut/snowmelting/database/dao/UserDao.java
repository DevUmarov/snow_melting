package com.example.knut.snowmelting.database.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.knut.snowmelting.database.entities.UserEntity;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by islom on 17. 6. 7.
 */

@Dao
public interface UserDao {

    @Insert(onConflict = REPLACE)
    void insertUser(UserEntity userEntity);

    @Query("SELECT * FROM user")
    List<UserEntity> getAllUsers();

}
