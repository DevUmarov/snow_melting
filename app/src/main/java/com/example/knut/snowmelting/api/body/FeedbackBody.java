package com.example.knut.snowmelting.api.body;

/**
 * Created by umarov on 8/15/17.
 */

public class FeedbackBody {

    int user_id;

    String type;

    int location_id;

    int sensor_node_id;

    String message;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getSensor_node_id() {
        return sensor_node_id;
    }

    public void setSensor_node_id(int sensor_node_id) {
        this.sensor_node_id = sensor_node_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
