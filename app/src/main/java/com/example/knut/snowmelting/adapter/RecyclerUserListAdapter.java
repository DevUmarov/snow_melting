package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.api.response.subModels.UserResponse;
import com.example.knut.snowmelting.databinding.ItemUserListBinding;
import com.example.knut.snowmelting.views.usersList.UsersListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 5. 24.
 */

public class RecyclerUserListAdapter extends RecyclerView.Adapter<RecyclerUserListAdapter.RecyclerViewHolder> {

    Context mContext;
    List<UserResponse> data;
    UsersListView usersListView;

    public RecyclerUserListAdapter(Context mContext) {
        this.mContext = mContext;
        this.usersListView = (UsersListView) mContext;
        data = new ArrayList<>();
    }

    public void setData(List<UserResponse> data) {
        this.data = data;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemUserListBinding itemBinding = ItemUserListBinding.inflate(layoutInflater, parent, false);
        return new RecyclerUserListAdapter.RecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemUserListBinding binding;

        public RecyclerViewHolder(ItemUserListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setOnClick(this);
        }

        public void bind(UserResponse item) {
            binding.setUserData(item);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_edit_user: {
                    usersListView.onEditButtonClick(binding.getUserData());
                    break;
                }
                case R.id.button_delete_user: {
                    usersListView.onDeleteButtonClick(binding.getUserData());
                    break;
                }
                case R.id.button_view_user: {
                    usersListView.onViewButtonClick(binding.getUserData());
                    break;
                }
                default: {
                    usersListView.onItemClick(v, binding.getUserData());
                }
            }

        }
    }
}
