package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.api.response.subModels.LocationResponse;
import com.example.knut.snowmelting.databinding.ItemLocListBinding;
import com.example.knut.snowmelting.views.admin.mainMenu.MainAdminPageView;
import com.example.knut.snowmelting.views.locationList.LocationListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 5. 30.
 */

public class RecyclerLocListAdapter extends RecyclerView.Adapter<RecyclerLocListAdapter.RecyclerViewHolder> {

    List<LocationResponse> data;
    MainAdminPageView locationListView;

    public RecyclerLocListAdapter(MainAdminPageView mainAdminPageView) {
        this.locationListView = mainAdminPageView;
        data = new ArrayList<>();
    }

    public void setData(List<LocationResponse> data) {
        this.data = data;
    }

    @Override
    public RecyclerLocListAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemLocListBinding itemBinding = ItemLocListBinding.inflate(layoutInflater, parent, false);
        return new RecyclerLocListAdapter.RecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerLocListAdapter.RecyclerViewHolder holder, int position) {
        if(data.size() > 0) {
            holder.bind(data.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemLocListBinding binding;

        public RecyclerViewHolder(ItemLocListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setOnClick(this);
        }

        public void bind(LocationResponse item) {
            binding.setLocationData(item);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            locationListView.onItemClicked(getAdapterPosition(), data.get(getAdapterPosition()));
        }
    }
}
