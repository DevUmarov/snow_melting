package com.example.knut.snowmelting.mvp;


/**
 * Created by Nizomjon on 11/18/16.
 */

public interface MvpView {

    void onError(String error);

    void showNoConnection();
}
