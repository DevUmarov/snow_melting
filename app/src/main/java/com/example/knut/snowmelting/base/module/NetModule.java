package com.example.knut.snowmelting.base.module;

import android.util.Log;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.SkService;
import com.example.knut.snowmelting.model.test.Forecast;
import com.example.knut.snowmelting.model.custom_deserializre.ForecastJsonDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nizomjon on 09/02/2017.
 */
@Module
public class NetModule {

    public static final String PRODUCTION_API = "http://138.197.126.15/";
    private final static String BASE_URL_SK = "http://apis.skplanetx.com/";
    public static final String BETA_API = "http://192.168.1.11:5000/";
    private static final String SK_API = "";
    public static final String PRODUCTION = "production";
    public static final String SKPLANET = "skplanet";

    @Singleton
    @Provides
    @Named(PRODUCTION)
    RestService provideService() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(PRODUCTION_API)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .client(provideClient())
                .build();

        return retrofit.create(RestService.class);
    }

    @Singleton
    @Provides
    @Named(SKPLANET)
    SkService provideServiceSk() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_SK)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(provideClient())
                .build();

        return retrofit.create(SkService.class);
    }

    @Provides
    @Singleton
    @Named(PRODUCTION)
    Gson provideGson() {
        return new GsonBuilder()
                .setLenient()
                .create();
    }

    @Provides
    @Singleton
    @Named(SKPLANET)
    Gson provideGsonSk() {
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd hh:mm:ss")
                .registerTypeAdapter(Forecast.class, ForecastJsonDeserializer.getForecastJsonDeserializer())
                .create();
    }

    @Singleton
    @Provides
    OkHttpClient provideClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json;charset=utf-8")
                        .addHeader("Accept", "application/json")
                        .build();


                long t1 = System.nanoTime();
                Log.i("TAG", String.format("Sending request %s on %s%n%s",
                        request.url(), chain.connection(), request.headers()));

                Response response = chain.proceed(request);

                long t2 = System.nanoTime();
                Log.i("TAG", String.format("Received response for %s in %.1fms%n%s",
                        response.request().url(), (t2 - t1) / 1e6d, response.headers()));


                return chain.proceed(request);
            }
        });
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.connectTimeout(10, TimeUnit.SECONDS);
        return builder.build();
    }

}
