package com.example.knut.snowmelting.api.callback;

import java.net.ConnectException;
import java.net.UnknownHostException;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import retrofit2.adapter.rxjava2.HttpException;

/**
 * Created by islom on 17. 6. 2.
 */

public abstract class ApiDisposableCallback<T> extends DisposableObserver<T> {

    public abstract void onAPIError(@NonNull Exception e);

    @Override
    public void onError(Throwable e) {

        if (e instanceof HttpException) {
            HttpException exception = (HttpException) e;
            onAPIError(new Exception(getMessage(exception.code(), "Unknown error.")));
        } else if (e instanceof UnknownHostException) {
            onAPIError(new Exception("Server not available."));
        } else if (e instanceof ConnectException) {
            onAPIError(new Exception("Failed to connect to server"));
        } else {
            onAPIError(new Exception(e.getMessage()));
        }

        onComplete();
    }

    @NonNull
    private String getMessage(int errorCode, @NonNull String defaultMessage) {
        switch (errorCode) {
            case 400:
                return "Bad Request.";
            case 401:
                return "Unauthorized";
            case 403:
                return "Forbidden";
            case 404:
                return "Not Found";
            case 408:
                return "Request Time Out";
            default:
                return defaultMessage;
        }
    }
}
