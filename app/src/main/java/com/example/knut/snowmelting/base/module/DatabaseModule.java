package com.example.knut.snowmelting.base.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.example.knut.snowmelting.database.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by islom on 17. 6. 2.
 */

@Module
public class DatabaseModule {

    private Context context;

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    AppDatabase provideAppDataBase(){
        return Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "app_db").build();
    }
}
