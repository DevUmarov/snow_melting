package com.example.knut.snowmelting.api.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by umarov on 9/25/17.
 */

public class TurnOnResponseContent {

    @SerializedName("mic_1")
    @Expose
    int micOne;

    @SerializedName("mic_2")
    @Expose
    int micTwo;

    @SerializedName("mic_3")
    @Expose
    int micThree;

    public int getMicOne() {
        return micOne;
    }

    public void setMicOne(int micOne) {
        this.micOne = micOne;
    }

    public int getMicTwo() {
        return micTwo;
    }

    public void setMicTwo(int micTwo) {
        this.micTwo = micTwo;
    }

    public int getMicThree() {
        return micThree;
    }

    public void setMicThree(int micThree) {
        this.micThree = micThree;
    }
}
