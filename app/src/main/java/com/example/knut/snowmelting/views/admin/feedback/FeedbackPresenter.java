package com.example.knut.snowmelting.views.admin.feedback;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.body.FeedbackBody;
import com.example.knut.snowmelting.api.body.ResponseFeedbackBody;
import com.example.knut.snowmelting.api.callback.ApiDisposableCallback;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.model.FeedbackData;
import com.example.knut.snowmelting.mvp.BasePresenter;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.annotations.NonNull;


/**
 * Created by islom on 17. 5. 25.
 */

public class FeedbackPresenter extends BasePresenter<FeedbackView> {

    @Inject
    @Named(NetModule.PRODUCTION)
    RestService restService;

    private Context context;

    public FeedbackPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);

        this.context = context;
    }

    public void getFeedbacks() {

        unSubscribeAll();

        registerAsync(restService.getFeedbacks(), new ApiDisposableCallback<List<FeedbackData>>() {
            @Override
            public void onAPIError(@NonNull Exception e) {
                if (isViewAttached()) {
                    getMvpView().onFeedbacksError();
                }
            }

            @Override
            public void onNext(@NonNull List<FeedbackData> feedbackDatas) {
                if (isViewAttached()) {
                    getMvpView().onFeedbacksArrived(feedbackDatas);
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void respondToUser(int feedbackId, String message) {

        unSubscribeAll();
        ResponseFeedbackBody responseFeedbackBody = new ResponseFeedbackBody();
        responseFeedbackBody.setType("admin");
        responseFeedbackBody.setFeedbackId(feedbackId);
        responseFeedbackBody.setResponse(message);

        registerAsync(restService.respondToFeedback(responseFeedbackBody), new ApiDisposableCallback<ResponseFeedbackBody>() {
            @Override
            public void onAPIError(@NonNull Exception e) {
                if (isViewAttached()) {
                    getMvpView().onResponseFail();
                }
            }

            @Override
            public void onNext(@NonNull ResponseFeedbackBody responseFeedbackBody) {
                if (isViewAttached()) {
                    getMvpView().onResonseSuccess();
                }
            }

            @Override
            public void onComplete() {

            }
        });

    }
}
