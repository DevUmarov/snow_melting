package com.example.knut.snowmelting.views.register;

import android.content.Context;

import com.example.knut.snowmelting.api.RestService;
import com.example.knut.snowmelting.api.SkService;
import com.example.knut.snowmelting.base.components.DaggerPresenterComponent;
import com.example.knut.snowmelting.base.module.DatabaseModule;
import com.example.knut.snowmelting.base.module.NetModule;
import com.example.knut.snowmelting.base.module.UtilsModule;
import com.example.knut.snowmelting.mvp.BasePresenter;
import com.example.knut.snowmelting.utils.AppUtils;


import javax.inject.Inject;
import javax.inject.Named;


/**
 * Created by Nizomjon on 10/02/2017.
 */

public class RegisterPresenter extends BasePresenter<RegisterView> {


    @Inject
    @Named(NetModule.PRODUCTION)
    RestService api;

    @Inject
    @Named(NetModule.SKPLANET)
    SkService skApi;

    @Inject
    AppUtils mUtils;

    public RegisterPresenter(Context context) {

        DaggerPresenterComponent.builder()
                .netModule(new NetModule())
                .utilsModule(new UtilsModule(context))
                .databaseModule(new DatabaseModule(context))
                .build()
                .inject(this);
    }

//    public void registerAsync(String username, String email, String password) {
//        getMvpView().showProgress();
//        unSubscribeAll();
//        RegisterBody body = new RegisterBody(username, email, password);
//        subscribe(api.registerAsync(body), new SubscriberCallback<>(new ApiCallback<UserEntity>() {
//            @Override
//            public void onSuccess(UserEntity model) {
//                getMvpView().onRegisterSuccess();
//            }
//
//            @Override
//            public void onFailure(RetrofitException exception) {
//                try {
//                    LoginErrorResponse errorResponse = exception.getErrorBodyAs(LoginErrorResponse.class);
//                    getMvpView().onError(errorResponse.getError());
//                } catch (IOException e1) {
//                    e1.printStackTrace();
//                }
//            }
//
//
//            @Override
//            public void onCompleted() {
//                getMvpView().hideProgress();
//                getMvpView().closeApplication();
//            }
//
//            @Override
//            public void onNetworkError() {
//
//            }
//        }));
//    }
}
