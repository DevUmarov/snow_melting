package com.example.knut.snowmelting.model.test;

/**
 * Created by Nizomjon on 25/02/2017.
 */

public class Wind {
    final float speed;
    final float direction;

    public Wind(final float speed, final float direction) {
        this.speed = speed;
        this.direction = direction;
    }

    @Override
    public String toString() {
        return new StringBuilder("Wind{")
                .append("speed=").append(speed)
                .append(", direction=").append(direction)
                .append('}').toString();
    }

}
