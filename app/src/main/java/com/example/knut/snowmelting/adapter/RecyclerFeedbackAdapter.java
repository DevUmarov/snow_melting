package com.example.knut.snowmelting.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.knut.snowmelting.R;
import com.example.knut.snowmelting.commons.ExpandableItemStates;
import com.example.knut.snowmelting.databinding.ItemFeedbackBinding;
import com.example.knut.snowmelting.model.FeedbackData;
import com.example.knut.snowmelting.model.WeatherData;
import com.example.knut.snowmelting.views.admin.feedback.FeedbackView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by islom on 17. 5. 25.
 */

public class RecyclerFeedbackAdapter extends RecyclerView.Adapter<RecyclerFeedbackAdapter.RecyclerViewHolder> {

    Context mContext;
    List<FeedbackData> data;
    FeedbackView feedbackView;
    ExpandableItemStates itemStates;

    public RecyclerFeedbackAdapter(Context mContext, FeedbackView feedbackView) {
        this.mContext = mContext;
        this.feedbackView = feedbackView;
        itemStates = new ExpandableItemStates(this);
        data = new ArrayList<>();
    }

    public void setData(List<FeedbackData> data) {
        this.data = data;
    }

    @Override
    public RecyclerFeedbackAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemFeedbackBinding itemBinding = ItemFeedbackBinding.inflate(layoutInflater, parent, false);
        return new RecyclerFeedbackAdapter.RecyclerViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerFeedbackAdapter.RecyclerViewHolder holder, int position) {
        holder.bind(data.get(position), itemStates.isExpanded(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemFeedbackBinding binding;

        public RecyclerViewHolder(ItemFeedbackBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.setOnClick(this);
        }

        public void bind(FeedbackData item,  boolean isExpanded) {
            binding.setUserData(item);
            binding.setIsExpanded(isExpanded);
            binding.executePendingBindings();
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_respond_feedback: {
                    feedbackView.onRespondClick(data.get(getAdapterPosition()));
                    break;
                }
                default: {
                    itemStates.toggle(getAdapterPosition());
//                    feedbackView.onItemClick(v, data.get(getAdapterPosition()));
                }
            }
        }
    }
}