package com.example.knut.snowmelting.api.response;

import com.example.knut.snowmelting.api.response.subModels.SensorResponse;
import com.example.knut.snowmelting.api.response.subModels.SensorValuesResponse;
import com.example.knut.snowmelting.api.response.subModels.OnOffHistoryResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by islom on 17. 6. 8.
 */

public class SensorNodeResponse {

    @SerializedName("id")
    @Expose
    private int sensorNodeId;

    @SerializedName("location_id")
    @Expose
    private int locationId;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("status")
    @Expose
    private boolean status;

//    @SerializedName("history")
//    @Expose
//    private List<OnOffHistoryResponse> offHistoryResponseList;
//
//    @SerializedName("last_seen_values")
//    @Expose
//    private List<SensorValuesResponse> sensorLastSeenValuesList;

    @SerializedName("sensors")
    @Expose
    private List<SensorResponse> sensorResponseList;

    public int getSensorNodeId() {
        return sensorNodeId;
    }

    public void setSensorNodeId(int sensorNodeId) {
        this.sensorNodeId = sensorNodeId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


//    public List<OnOffHistoryResponse> getOffHistoryResponseList() {
//        return offHistoryResponseList;
//    }
//
//    public void setOffHistoryResponseList(List<OnOffHistoryResponse> offHistoryResponseList) {
//        this.offHistoryResponseList = offHistoryResponseList;
//    }
//
//    public List<SensorValuesResponse> getSensorLastSeenValuesList() {
//        return sensorLastSeenValuesList;
//    }
//
//    public void setSensorLastSeenValuesList(List<SensorValuesResponse> sensorLastSeenValuesList) {
//        this.sensorLastSeenValuesList = sensorLastSeenValuesList;
//    }

    public List<SensorResponse> getSensorResponseList() {
        return sensorResponseList;
    }

    public void setSensorResponseList(List<SensorResponse> sensorResponseList) {
        this.sensorResponseList = sensorResponseList;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
}
